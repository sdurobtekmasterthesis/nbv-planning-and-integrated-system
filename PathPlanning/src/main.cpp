#include <string>
#include <exception>

#include <caros/common.h>
#include <caros/common_robwork.h>
#include <rw/loaders/WorkCellLoader.hpp>
#include <ros/ros.h>


#include "PathPlanning.hpp"

int main(int argc, char *argv[])
{

    try{
    ros::init(argc, argv, "pathplanner");
    PathPlanning pathplan;

    pathplan.run();

    }
    catch(const rw::common::Exception &e)
    {
        std::cerr << "Error exception catched!!!: "<< std::endl << e << std::endl;

    }

  return 0;
}
