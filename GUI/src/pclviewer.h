#ifndef PCLVIEWER_H
#define PCLVIEWER_H

#include <iostream>

// Qt
#include <QMainWindow>

// Point Cloud Library
#include <pcl/point_cloud.h>
#include <pcl/point_types.h>
#include <pcl/visualization/pcl_visualizer.h>

//eigen
#include "eigen3/Eigen/Core"
#include "eigen3/Eigen/Geometry"

// Visualization Toolkit (VTK)
#include <vtkRenderWindow.h>

#include "rosserver.h"
#include "geometry_msgs/Transform.h"

#include <QObject>
#include <QString>
#include <QComboBox>
#include <QWidget>
#include <QMetaType>
#include <QThread>

#include <QTreeView>
#include <QStandardItemModel>
#include <QItemSelectionModel>


//typedef pcl::PointXYZRGBA PointT;
//typedef pcl::PointCloud<PointT> PointCloudT;

namespace Ui
{
  class PCLViewer;
}

class PCLViewer : public QMainWindow
{
  Q_OBJECT

public:
  explicit PCLViewer (QWidget *parent = 0);
  ~PCLViewer ();


public slots:
  //void RGBsliderReleased ();
  void viewSliderValueChanged (int value);
  void startButton ();
  void resetButton ();
  void saveButton ();
  void colorButton ();
  void sideBox (int index);
  //void redSliderValueChanged (int value);
  //void greenSliderValueChanged (int value);
  //void blueSliderValueChanged (int value);

  void updatePointCloud ();
  void updateIteration (double value);
  void updatePose (geometry_msgs::Transform pose);
  void done(int state);

signals:
  void send_goal();
  void send_reset();


protected:
  boost::shared_ptr<pcl::visualization::PCLVisualizer> viewer;
  //PointCloudT::Ptr cloud;


private:
  void updateCloudViewer(int view);

  Ui::PCLViewer *ui;
  std::vector<pcl::PointCloud<pcl::PointXYZRGB>::Ptr> clouds;
  std::vector<Eigen::Affine3f> view_points;

  int iteration_nr;
  int midway;
  bool pause;
  bool finished;

  QThread *ros_server_thread;
  RosServer *server;

};

#endif // PCLVIEWER_H
