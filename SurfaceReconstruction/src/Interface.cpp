#include <iostream>
#include "boost/date_time/posix_time/posix_time.hpp"
#include "Reconstruction.h"

typedef boost::posix_time::ptime Time;
typedef boost::posix_time::time_duration TimeDuration;

#include "ros/ros.h"

//message
#include <simsensorplugin/pointCloud.h>
#include <sensor_msgs/PointCloud2.h>
#include "geometry_msgs/Transform.h"
#include "../../NBV_algorithms/Shared/libaryTranslation.h"
#include "../../NBV_algorithms/Shared/Quaternion.h"


#include "pcl/PolygonMesh.h"
#include <pcl/io/pcd_io.h>
#include <pcl/io/ply_io.h>
#include <pcl/point_types.h>
#include "pcl/point_cloud.h"
#include <pcl/search/kdtree.h>
#include <pcl/features/normal_3d_omp.h>
#include <pcl/surface/marching_cubes_rbf.h>
#include <pcl/surface/marching_cubes_hoppe.h>

#include <std_msgs/Bool.h>
#include <std_msgs/String.h>
//#include "view_pick/nbv_plannerAction.h"

//viewer stuff
#include <pcl_conversions/pcl_conversions.h>
#include <pcl/visualization/cloud_viewer.h>
#include <pcl/visualization/pcl_visualizer.h>

ros::Publisher publishPointCloud;
Reconstruction *method;

bool log_data;
int iteration;

void doneCallback(const std_msgs::BoolConstPtr done)
{
    if(done->data)
    {
        method->saveCloud();
    }
}

void registrationCallback(const geometry_msgs::TransformConstPtr turn)
{
    Eigen::Matrix4d transform = Libary::toEigenFromROS(*turn);
    method->combineCloud(transform);
}

void filenameCallback(const std_msgs::StringConstPtr file)
{
    method->setFilename(file->data);
}

void pointCloudCallback(const simsensorplugin::pointCloudPtr pointCloud)
{
    pcl::PointCloud<pcl::PointXYZ>::Ptr cloud(new pcl::PointCloud<pcl::PointXYZ>());
    pcl::fromROSMsg(pointCloud->pointcloud, *cloud);

    Eigen::Vector3d direction(-pointCloud->transform.translation.x, -pointCloud->transform.translation.y, -pointCloud->transform.translation.z);

    method->addCloudConsistentNormals(cloud, direction);
    
    /*pcl::PolygonMesh::Ptr triangles;
    if(iteration > 2)
    {
        //pcl::PolygonMesh::Ptr triangles = method.marchingCubes();
        triangles = method->greedyTriangulation();
    }*/
    iteration++;

    //std::cout << triangles->polygons.size() << " triangles created" << "In time: " << msec/1000.0 << std::endl;
    /*boost::shared_ptr<pcl::visualization::PCLVisualizer> viewer (new pcl::visualization::PCLVisualizer ("3D mesh Viewer"));
    viewer->setBackgroundColor (0, 0, 0);
    viewer->addPointCloud(cloud);
    viewer->addPolygonMesh(*triangles,"meshes",0);
    viewer->addCoordinateSystem (1.0);
    viewer->initCameraParameters ();
    while (!viewer->wasStopped() && ros::ok() ){
        viewer->spinOnce (100);
        boost::this_thread::sleep (boost::posix_time::microseconds (100000));
    }*/
}

void resetCallback(const std_msgs::BoolConstPtr reset)
{
    if(reset->data)
    {
        delete method;
        method = new Reconstruction(log_data);
    }
}

int main(int argc, char **argv)
{
    ros::init(argc, argv, "surface_reconstruction_node");
    ros::NodeHandle nodeHandler("surface_reconstruction");

    //local handle for parameters
    ros::NodeHandle n("~");
    //load parameters
    n.param<bool>("log_data", log_data, false);
    std::cout << "surface reconstruction log data: " << log_data << std::endl;

    method = new Reconstruction(log_data);

    //connect with the topic
    ros::Subscriber sub = nodeHandler.subscribe("/registration/PointCloudMsg_Part", 1, pointCloudCallback);
    //publishPointCloud = nodeHandler.advertise<simsensorplugin::pointCloud>("PointCloudMsg", 1);
    ros::Subscriber sub_reset = nodeHandler.subscribe("/view_pick_node/reset", 4, resetCallback);
    ros::Subscriber sub_file = nodeHandler.subscribe("/GUI/filename", 4, filenameCallback);
    ros::Subscriber sub_done = nodeHandler.subscribe("/planetarian/finished", 4, doneCallback);
    //ros::Subscriber sub_done = nodeHandler.subscribe("/surfacebased/finished", 4, doneCallback);
    ros::Subscriber sub_registration = nodeHandler.subscribe("/registration/Global_Transform_Delay", 4, registrationCallback);

    iteration = 0;

    ros::spin();

    return 0;
}
