#include "Random.h"
#include "ros/ros.h"
#include "geometry_msgs/Transform.h"
#include "simsensorplugin/getPointCloud.h"

#include <sensor_msgs/PointCloud2.h>
#include <pcl_conversions/pcl_conversions.h>
#include <pcl/visualization/cloud_viewer.h>
#include <pcl/visualization/pcl_visualizer.h>

int main(int argc, char **argv)
{
    ros::init(argc, argv, "NBV_Random_node");
    ros::NodeHandle nodeHandler;

    ros::ServiceClient client = nodeHandler.serviceClient<simsensorplugin::getPointCloud>("/getPointCloudService");
    simsensorplugin::getPointCloud::Request  req;
    simsensorplugin::getPointCloud::Response res;


    NBV_Random method;
    bool done = false;

    pcl::PointCloud<pcl::PointXYZRGB> cloud;
     pcl::PointCloud<pcl::PointXYZ> cloud2;

     int count = 0;

    while( count < 6 )
    {
        std::cout << "count " << count << std::endl;
        transform pose = method.next(cloud2, done);

        geometry_msgs::Transform msg;
        msg.translation.x = pose.translation(0);
        msg.translation.y = pose.translation(1);
        msg.translation.z = pose.translation(2);
        msg.rotation.w = pose.rotation.w();
        msg.rotation.x = pose.rotation.x();
        msg.rotation.y = pose.rotation.y();
        msg.rotation.z = pose.rotation.z();
        req.transf = msg;

    client.call(req, res);
    sensor_msgs::PointCloud2 data = res.pointcloud;

    pcl::fromROSMsg(data, cloud);
    pcl::copyPointCloud(cloud, cloud2);

//        boost::shared_ptr<pcl::visualization::PCLVisualizer> viewer3 (new pcl::visualization::PCLVisualizer ("3D Viewer ROS"));
//        viewer3->addPointCloud(cloud.makeShared());

//        while(!viewer3->wasStopped())
//        {
//            viewer3->spinOnce(100);
//            boost::this_thread::sleep (boost::posix_time::microseconds (1000));
//        }

        count++;
    }

    method.getInfo();

    return 0;
}
