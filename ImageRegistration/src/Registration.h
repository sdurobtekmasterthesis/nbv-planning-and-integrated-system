#ifndef REGISTRATION_H
#define REGISTRATION_H

#include "pcl/point_cloud.h"
#include "pcl/point_types.h"
#include "eigen3/Eigen/Core"
#include "eigen3/Eigen/Geometry"

#include <fstream>
#include <iostream>

#include <pcl/registration/icp.h>
#include <pcl/registration/correspondence_rejection_trimmed.h>
#include <pcl/recognition/auxiliary.h> //needed for trimmed_icp
#include <pcl/recognition/ransac_based/trimmed_icp.h>

#define OVERLAP_RATIO 0.45

class Registration
{
    public:
        Registration(double ground_filter = 0.02, double plane_filter_max_dist = 0.01, bool log_data = false, std::string filename = "data_default");
        /*
         * Add a point cloud to the model and makes local registration(TrICP) on the input cloud
         * */
        Eigen::Matrix4d addPointCloud(pcl::PointCloud<pcl::PointXYZRGB> &cloud, double overlap = OVERLAP_RATIO);
        /*
         * Applies Fast Point Feature Histogram and Sample Consensus Initial Alignment to find a global transformation for the problem
         * */
        Eigen::Matrix4d findGlobalTransform(pcl::PointCloud<pcl::PointXYZRGB> &cloud);
        /*
         * Tries to evaluate if the global transform found is correctly placed and tries to fix symmetry issues
         * */
        Eigen::Matrix4d symmetryCheck(pcl::PointCloud<pcl::PointXYZRGB> &cloud, Eigen::Matrix4d initial_transform);
        /*
         * Set the filename for the folder logged data should be stored in, Example:("data_default")
         * */
        void set_filename(std::string filename);
        /*
         * Applies the given transform on the input cloud and gives it in the output cloud
         * */
        void transformOrigin(const pcl::PointCloud<pcl::PointXYZRGB> &inputCloud, pcl::PointCloud<pcl::PointXYZRGB> &outputCloud, Eigen::Matrix4d transform);
        /*
         * Filters for z<ground_filter, which is given in the constructor
         * */
        void filterGround(const pcl::PointCloud<pcl::PointXYZRGB> &inputCloud, pcl::PointCloud<pcl::PointXYZRGB> &filteredCloud);
        /*
         * Filter for the ground, using a plane filter
         * */
        void filterGroundPlane(const pcl::PointCloud<pcl::PointXYZRGB> &inputCloud, pcl::PointCloud<pcl::PointXYZRGB> &filteredCloud);

        /*
         * !!!NOT TESTED YET!!!, filters point far away based on the 3d datas covariance
         * */
        void filterStatisticalDistance(const pcl::PointCloud<pcl::PointXYZRGB> &inputCloud, pcl::PointCloud<pcl::PointXYZRGB> &filteredCloud, double dist);
        void testFilterStatisticalDistance(Eigen::Vector3d mean, Eigen::MatrixXd cov, double dist);


        pcl::PointCloud<pcl::PointXYZRGB>::Ptr model_cloud;
        pcl::PointCloud<pcl::PointXYZRGB>::Ptr model_cloud_2;
        int view_count;

    private:
        pcl::IterativeClosestPoint<pcl::PointXYZRGB, pcl::PointXYZRGB> trimmed_icp;
        pcl::registration::CorrespondenceRejectorTrimmed::Ptr trimmed_rejector;
        bool log_data;
        double ground_filter;
        double plane_filter_max_dist;
        std::string filename;
        std::ofstream data_file;
        int save_count;

};

#endif // REGISTRATION_H
