#ifndef NBV_SURFACEBASED_H
#define NBV_SURFACEBASED_H

#include <Eigen/Core>
#include <vector>

//#include "../../Shared/EAA.h"
//#include "../../Shared/Quaternion.h"
#include "eigen3/Eigen/Geometry"

#include <pcl/point_types.h>
#include <pcl/point_cloud.h>
#include <pcl/octree/octree.h>
#include <pcl/kdtree/kdtree.h>
#include <pcl/kdtree/kdtree_flann.h>
#include <pcl/features/normal_3d.h>
#include <pcl/features/normal_3d_omp.h>

#include <fstream>
#include <iostream>

#include <pcl/features/boundary.h>

struct transform
{
    Eigen::Vector3d translation;
    Eigen::Quaterniond rotation;
};

class NBV_surfacebased
{

public:
    NBV_surfacebased(bool log_data = false, int max_iterations = 30, double object_distance = 0.3, double filter_distance = 0.1, std::string filename = "data_default");
    void rotate_scene(transform object_movement);
    std::vector<transform> next(const pcl::PointCloud<pcl::PointXYZ> &pointCloud, bool &done);
    transform selectView(std::vector<transform>, bool &done);
    void set_filename(std::string filename);


private: 
    pcl::PointCloud<pcl::PointXYZ>::Ptr cloudDownSampling(pcl::PointCloud<pcl::PointXYZ>::Ptr);
    pcl::PointCloud<pcl::PointXYZ>::Ptr boundaryEstimation(pcl::PointCloud<pcl::PointXYZ>::Ptr);
    std::vector<transform> viewGeneration(pcl::PointCloud<pcl::PointXYZ>::Ptr cloud);
    //High level logic for clustering
    std::vector<pcl::PointCloud<pcl::PointXYZ>::Ptr> cluster(pcl::PointCloud<pcl::PointXYZ>::Ptr cloud);
    //Filters views away if they are too simular
    bool viewFilter(transform view);
    //generates a viewpoint to look at some data
    transform viewFromPCA(pcl::PointCloud<pcl::PointXYZ>::Ptr cluster);
    //Makes a guess on where to start the radius to find the most significant clusters
    float estimateSearchRadius(pcl::PointCloud<pcl::PointXYZ>::Ptr cloud, std::vector<int> &cluster_options);
    //function to connect cluster from a point based on the radius
    std::vector<int> estimateCluster(pcl::PointCloud<pcl::PointXYZ>::Ptr cloud, int start_index, float radius);
    bool *found;
    //saves a cloud(warning, folder is not realtive now)
    void save(pcl::PointCloud<pcl::PointXYZ>::Ptr cloud);
    int save_counter;
    std::vector<transform> picked_views;
    std::vector<transform> current_options;

    pcl::PointCloud<pcl::PointXYZ>::Ptr pointcloud;
    pcl::PointCloud<pcl::Normal>::Ptr  pointnormals;

    Eigen::Vector3d global_translation;
    Eigen::Matrix3d global_rotation;
    Eigen::Matrix4d global_transformation;
    bool log_data;
    bool turned;
    std::string filename;
    std::ofstream data_file;
    int iteration_counter;
    int iteration_max;
    double filter_distance, object_distance;
};

#endif // NBV_SURFACEBASED_H
