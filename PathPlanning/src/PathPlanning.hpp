#ifndef PATHPLANNIG_H
#define PATHPLANNIG_H

#include <mutex>
#include <exception>

#include <opencv2/opencv.hpp>
#include <iostream>

#include <caros/serial_device_si_proxy.h>
#include <caros/common_robwork.h>

#include <rw/common.hpp>
#include <rw/kinematics.hpp>
#include <rw/math.hpp>
#include <rw/models.hpp>
#include <rw/models/WorkCell.hpp>
#include <rw/loaders/WorkCellFactory.hpp>
#include <rw/loaders/WorkCellLoader.hpp>
#include <rw/trajectory/LinearInterpolator.hpp>
#include <rw/pathplanning/PlannerConstraint.hpp>
#include <rw/pathplanning/QToQPlanner.hpp>
#include <rw/invkin.hpp>
#include <rw/geometry.hpp>
#include <rwlibs/proximitystrategies/ProximityStrategyYaobi.hpp>
#include <rwlibs/pathplanners/rrt/RRTPlanner.hpp>
#include <rwlibs/pathplanners/rrt/RRTQToQPlanner.hpp>
#include <rwlibs/proximitystrategies/ProximityStrategyFactory.hpp>
#include <rwlibs/proximitystrategies/ProximityStrategyFactory.hpp>
#include <rwlibs/pathplanners/rrt/RRTPlanner.hpp>
#include <rwlibs/pathoptimization/pathlength/PathLengthOptimizer.hpp>

#include <pcl/io/io.h>
#include <pcl/io/pcd_io.h>
#include <pcl/point_types.h>
#include <pcl/point_cloud.h>
#include <pcl_ros/point_cloud.h>

#include <ros/ros.h>
#include <ros/callback_queue.h>
#include <cv_bridge/cv_bridge.h>
#include <image_transport/image_transport.h>
#include <sensor_msgs/image_encodings.h>

#include <sensor_msgs/PointCloud2.h>
#include <sensor_msgs/point_cloud2_iterator.h>
#include <sensor_msgs/Image.h>

#include <pathplanner/setCamPose.h>
#include <pathplanner/setBoundingBox.h>
#include <pathplanner/getPointCloud.h>
#include <pathplanner/isPoseValid.h>
#include <pathplanner/pointCloud.h>
#include <pathplanner/saveQtoFile.h>

#include <string>
#include <stdexcept>

#define MAX_X -0.25
#define MIN_X -0.7
#define MAX_Y 0.7
#define MIN_Y 0.05

/* WARNING: USE AT YOUR OWN RISK!
 * None of this is code is guaranteed to not make the robot crash into anything or do unexpected behaviour!
 * It is highly recommended that you understand what the code does before you run it!
 *
 * To minimise risk, please use (very) small q_change values!
 */

/* This class is supposed to make it a bit easier to reuse the test code.
 * To use another planner or collision detector, simply add a member function that the user can invoke and override the
 * defaults (before invoking the test functions)
 */
class PathPlanning
{
 public:
  PathPlanning(unsigned int rotation_steps = 4) : nodehandle_("~"), sdsip_(nodehandle_, "caros_universalrobot", true)
  {
    ROS_INFO_STREAM("Starting init");
    this->rotation_steps = rotation_steps;

    initWorkCell();
    initDevice();
    initPathPlannerWithCollisionDetector();

//    std::vector<rw::kinematics::Frame*> frames = workcell_->getFrames();
//    int framecount = frames.size();

//    for(unsigned int i = 0; i < frames.size(); ++i)
//    {
//        std::cout << "Frame index: " << i << " name: " << (*frames[i]).getName() << std::endl;
//    }

    objectFrame_ = dynamic_cast<rw::kinematics::MovableFrame*>(workcell_->findFrame("objectFrame"));
    camFrame_ = workcell_->findFrame("camFrame");
    boundingFrame_ = dynamic_cast<rw::kinematics::MovableFrame*>(workcell_->findFrame("boundingFrame"));

    if(objectFrame_ == NULL)
    {
        ROS_ERROR_STREAM("Object frame: " << objectFrame_->getName() << " not found");
        RW_THROW("Camera frame: " << objectFrame_->getName() << " not found!");
    }

    if(camFrame_ == NULL)
    {
        ROS_ERROR_STREAM("Camera frame: " << camFrame_->getName() << " not found");
        RW_THROW("Camera frame: " << camFrame_->getName() << " not found!");
    }

    if(boundingFrame_ == NULL)
    {
        ROS_ERROR_STREAM("Bounding frame: " << boundingFrame_->getName() << " not found");
        RW_THROW("Bounding frame: " << boundingFrame_->getName() << " not found!");
    }

    rw::geometry::GeometryData::Ptr box = rw::common::ownedPtr(new rw::geometry::Box(0.01,0.01,0.01));
    geoPtr_ = new rw::geometry::Geometry(box, 1.0);
    detector_->addGeometry(boundingFrame_,geoPtr_);
    setboundingBox(rw::math::Transform3D<>(rw::math::Vector3D<>(0,0,0.20),rw::math::RPY<>(0,0,0)),0.19, 0.19,0.19);
    numimage = 0;

    worldTObjectRef = rw::kinematics::Kinematics::worldTframe(objectFrame_, state_);
    worldTRobotBase = rw::kinematics::Kinematics::worldTframe(device_->getBase(), state_);
    endTcam = rw::kinematics::Kinematics::frameTframe(device_->getEnd(),camFrame_, state_);

    std::cout << "worldTObjectRef" << worldTObjectRef.P() << " " << worldTObjectRef.R() << std::endl;
    std::cout << "worldTRobotBase" << worldTRobotBase.P() << " " << worldTRobotBase.R() << std::endl;

    sub_pointcloud_ = nodehandle_.subscribe("/camera/depth_registered/points", 1, &PathPlanning::callbackGetpointCloud, this);
    //sub_image = nodehandle_.subscribe("/camera/rgb/image_rect_color", 1, &PathPlanning::callbackGetImage, this);
    setCamPose_service_ = nodehandle_.advertiseService((std::string) "/getPointCloudService" , &PathPlanning::callbackMoveCamToPose, this);
    IKcheckPose_ = nodehandle_.advertiseService((std::string) "isPoseValid", &PathPlanning::callbackIKcheckPose, this);
    setBoundingBox_srv = nodehandle_.advertiseService((std::string) "setBoundingBox", &PathPlanning::callbacksetBoundingBox, this);
    publishPointCloud_ = nodehandle_.advertise<pathplanner::pointCloud>( (std::string) "/PointCloudMsg", 1);
    saveQtoFile_ = nodehandle_.advertiseService((std::string) "saveQtoFile", &PathPlanning::CallbackSaveQtoFile,this);

    ROS_INFO_STREAM("Finished init");
  }

  virtual ~PathPlanning()
  {
    /* Empty */
  }

  void run()
  {
      ROS_INFO_STREAM("Path planning node running");

      ros::MultiThreadedSpinner spinner(5); // Use 5 threads
      spinner.spin(); // spin() will not return until the node has been shutdown
  }

  bool CallbackSaveQtoFile(pathplanner::saveQtoFile::Request &req, pathplanner::saveQtoFile::Response &res)
  {
	ROS_INFO_STREAM("saveQ called");
        // Get current Q and sace to file
        rw::math::Q tempQ = getCurrentJointConfiguration();

        std::ofstream out;
        out.open("calib_data_q.txt", std::ios::app);
        out.precision(16);
        out << tempQ << std::endl;
	ROS_INFO_STREAM("saveQ end!!");
    return true;
  }

  void callbackGetpointCloud(const sensor_msgs::PointCloud2Ptr& pointcloud_msg)
  {
    mutex_pointcloud.lock();
    pointCloud_ = *pointcloud_msg;
    mutex_pointcloud.unlock();
  }

//  void callbackGetImage(const sensor_msgs::Image::ConstPtr& image_mgs)
//  {
//      mutex_image.lock();
//      image_ = cv_bridge::toCvCopy(image_mgs, "bgr8")->image;
//      mutex_image.unlock();

//  }

  bool callbackIKcheckPose(pathplanner::isPoseValid::Request &req, pathplanner::isPoseValid::Response &res)
  {    
    rw::math::Transform3D<double> temp_trans(rw::math::Vector3D<double>(req.transf.translation.x,req.transf.translation.y, req.transf.translation.z ),
                                        rw::math::Quaternion<double>(req.transf.rotation.x,req.transf.rotation.y, req.transf.rotation.z, req.transf.rotation.w).toRotation3D());

    temp_trans.R() = temp_trans.R().inverse();

    rw::math::Transform3D<double> actual_transform;
    bool isvalid = false;

    if(!((temp_trans.P()[0] > MIN_X && temp_trans.P()[0] < MAX_X && temp_trans.P()[1] > MIN_Y && temp_trans.P()[1] < MAX_Y) || temp_trans.P()[2] < 0.20))
    {
        isvalid = moveCamToPose(temp_trans, actual_transform, false);
        std::cout << temp_trans.P() << std::endl;
    }

    std::cout << "isPoseVaild check:" << isvalid << std::endl;
    res.isValid = isvalid;

    return true;
  }

  bool callbackMoveCamToPose(pathplanner::getPointCloud::Request &req, pathplanner::getPointCloud::Response &res)
  {
      ROS_INFO_STREAM("callback CamToPose called");

      rw::math::Transform3D<double> temp_trans(rw::math::Vector3D<double>(req.transf.translation.x,req.transf.translation.y, req.transf.translation.z ),
                                         rw::math::Quaternion<double>(req.transf.rotation.x,req.transf.rotation.y, req.transf.rotation.z, req.transf.rotation.w).toRotation3D());

      temp_trans.R() = temp_trans.R().inverse();

      rw::math::Transform3D<double> actual_transform;
      bool isPoseReached = false;

      if(!((temp_trans.P()[0] > MIN_X && temp_trans.P()[0] < MAX_X && temp_trans.P()[1] > MIN_Y && temp_trans.P()[1] < MAX_Y) || temp_trans.P()[2] < 0.2))
      {

          std::cout << temp_trans.P() << std::endl;

          rw::math::Q qCurrent = getCurrentJointConfiguration();
          device_->setQ(qCurrent, state_);

          rw::math::Rotation3D<> temp2;

          isPoseReached = moveCamToPose(temp_trans, actual_transform, true);
      }

      if(isPoseReached)
      {
          rw::math::Quaternion<> quater(rw::math::inverse(actual_transform.R()));

          res.isPoseReached = isPoseReached;

          //publish the point cloud
          pathplanner::pointCloud msg;
          mutex_pointcloud.lock();
          msg.pointcloud = pointCloud_;
          msg.header.stamp = ros::Time::now();
          mutex_pointcloud.unlock();

          msg.transform.rotation.x = quater.getQx();
          msg.transform.rotation.y = quater.getQy();
          msg.transform.rotation.z = quater.getQz();
          msg.transform.rotation.w = quater.getQw();
          msg.transform.translation.x = actual_transform.P()[0];
          msg.transform.translation.y = actual_transform.P()[1];
          msg.transform.translation.z = actual_transform.P()[2];
          publishPointCloud_.publish(msg);

          // save q to calibration
          rw::math::Q tempQ = getCurrentJointConfiguration();
          std::ofstream out;
          out.open("calib_data_q_new.txt", std::ios::app);
          out.precision(10);
          out << tempQ << std::endl;
          out.close();

//          std::stringstream s;
//          s << std::setfill('0') << std::setw(2) << numimage;
//          s <<".jpeg";
//          cv::imwrite(s.str().c_str(), image_);
//          numimage++;
      }

      ROS_INFO_STREAM("callback CamToPose finished");

      return true;
  }

  bool callbacksetBoundingBox(pathplanner::setBoundingBox::Request &req, pathplanner::setBoundingBox::Response &res)
  {

      rw::math::Transform3D<double> temp_trans(rw::math::Vector3D<double>(req.transf.translation.x,req.transf.translation.y, req.transf.translation.z ),
                                         rw::math::RPY<double>(req.transf.rotation.x,req.transf.rotation.y, req.transf.rotation.z));
      setboundingBox(temp_trans, req.sizeXYZ.x, req.sizeXYZ.y, req.sizeXYZ.y);

      return true;
  }

  void setboundingBox(rw::math::Transform3D<> trans, double x, double y, double z)
  {
    std::cout << "setboundingBox called" << std::endl;
      boundingFrame_->setTransform(trans, state_);
      std::cout << "setboundingBox 1" << std::endl;
      detector_->removeGeometry(boundingFrame_, geoPtr_);

    std::cout << "setboundingBox between" << std::endl;
      rw::geometry::GeometryData::Ptr box = rw::common::ownedPtr(new rw::geometry::Box(x,y,z));
      geoPtr_ = new rw::geometry::Geometry(box, 1.0);
      detector_->addGeometry(boundingFrame_, geoPtr_);
    std::cout << "setboundingBox end" << std::endl;
  }

  bool moveCamToPose(rw::math::Transform3D<double> transform, rw::math::Transform3D<>& transform_out, bool moveRobot=true )
  {
      ROS_INFO_STREAM("moveCamToPose called");
      bool return_status = true;
      bool solution_found = false;
      rw::math::Q qDesired;
      rw::math::Transform3D<double> robotposeEnd;

       std::cout << "input pose \n" << transform.P() << " \n" << rw::math::RPY<>(transform.R()) << std::endl;

      for(unsigned int i = 0; i < rotation_steps && !solution_found; ++i)
      {
        // Transform the Pose into robot end pose
        rw::math::Transform3D<> baseTcam = rw::math::inverse(worldTRobotBase) * worldTObjectRef *transform;
        //rw::math::Transform3D<> baseTend = baseTcam * rw::math::inverse(endTcam);

        transform_out = transform * rw::math::Transform3D<>(rw::math::Vector3D<>(0,0,0),rw::math::RPY<>((2.0*rw::math::Pi)/rotation_steps * i, 0,0));

        rw::math::Transform3D<> baseTcam_temp = baseTcam * rw::math::Transform3D<>(rw::math::Vector3D<>(0,0,0),rw::math::RPY<>((2.0*rw::math::Pi)/rotation_steps * i, 0,0));
        rw::math::Transform3D<> baseTend_rot = baseTcam_temp * rw::math::inverse(endTcam);

//        std::cout << "baseTcam: " << std::endl;
//        std::cout << baseTcam.P() << "\n" << rw::math::RPY<>(baseTcam.R()) << std::endl;
//        std::cout << baseTcam_temp.P() << "\n" << rw::math::RPY<>(baseTcam_temp.R()) << std::endl;

//        std::cout << "baseTend: " << std::endl;
//        std::cout << baseTend.P() << "\n" << rw::math::RPY<>(baseTend.R()) << std::endl;
//        std::cout << baseTend_rot.P() << "\n " << rw::math::RPY<>(baseTend_rot.R()) << std::endl;

//        std::cout << "Transform out \n" << transform_out.P() << "\n" << rw::math::RPY<>(transform_out.R()) << std::endl;

          solution_found = inverseKinematics(baseTend_rot, qDesired);

      }

      if(solution_found)
      {
        if(moveRobot)
        {
            // Move the robot
              rw::trajectory::QPath path = getQPath(qDesired);
              if(path.size() > 0)
              {
                  for (const rw::math::Q& p : path)
                  {
                        // should use the path interface instead!!!
                        //ROS_INFO_STREAM("Ask to movePtp to '" << p << "'.");
                        bool ret = false;
                        ret = sdsip_.movePtp(p, 30);
                        if (!ret)
                        {
                          return_status = false;
                          ROS_ERROR_STREAM("The serial device didn't acknowledge the movePtp command.");
                          break;
                        }
                  }

                // Block until the robot is at the right place
                  double dist = 100.0;
                  rw::math::Q diff_q, qCurrent;
                  while(dist > 0.01)
                  {
                      qCurrent = getCurrentJointConfiguration();
                      diff_q = (qDesired-qCurrent),
                      dist = diff_q.norm1();
                      /*
                      std::cout << "qDisred: "<< qDesired << std::endl;
                      std::cout << "qCurrent: " << qCurrent << std::endl;
                      std::cout << "qDisred-getCurrentJointConfig: " << diff_q << std::endl;
                      std::cout << "norm2 dist: " << dist << std::endl;
                      */
                  }

                device_->setQ(qCurrent, state_);
             }
        }
      }
      else
      {
          if(moveRobot)
          {
              // Handel the error
             ROS_ERROR_STREAM("No inversekinematic solution to: \n" << robotposeEnd.P() << " \n"
                              << rw::math::RPY<>(robotposeEnd.R()));
          }

          return_status = false;
      }

    ROS_INFO_STREAM("moveCamToPose called end");
    return return_status;
  }

 protected:
  void initWorkCell()
  {
    workcell_ = caros::getWorkCell();
    if (workcell_ == NULL)
    {
      ROS_ERROR("No workcell was loaded - exiting...");
      throw std::runtime_error("Not able to obtain a workcell.");
    }
    else
    {
        state_ = workcell_->getDefaultState();
    }
  }

  void initDevice()
  {
    std::string device_name;
    if (!nodehandle_.getParam("device_name", device_name))
    {
      ROS_FATAL_STREAM("The parameter '" << nodehandle_.getNamespace()
                                         << "/device_name' was not present on the parameter "
                                            "server! This parameter has to be specified "
                                            "for this test-node to work properly.");
      throw std::runtime_error("Not able to obtain device name.");
    }

    ROS_DEBUG_STREAM("Looking for the device '" << device_name << "' in the workcell.");
    device_ = workcell_->findDevice(device_name);
    if (device_ == NULL)
    {
      ROS_FATAL_STREAM("Unable to find device " << device_name << " in the loaded workcell");
      throw std::runtime_error("Not able to find the device within the workcell.");
    }
  }

  void initPathPlannerWithCollisionDetector()
  {
    rw::kinematics::State state = workcell_->getDefaultState();
    /* Collision detector */
    detector_ = rw::common::ownedPtr(
        new rw::proximity::CollisionDetector(workcell_, rwlibs::proximitystrategies::ProximityStrategyYaobi::make()));
    /* PlannerConstraint that uses the collision detector to verify that the _start_ and _end_ configurations are
     * collision free and that the edge(s) between those is/are also collision free. */
    const rw::pathplanning::PlannerConstraint planner_constraint =
        rw::pathplanning::PlannerConstraint::make(detector_, device_, state);

    /* Just using a really simple path planner (straight line in the configuration space) */
    planner_ = rw::pathplanning::QToQPlanner::make(planner_constraint);

  }

  /*
   * Get a Q Path from the RRT planner
   *
  */
  bool queryRRT( rw::math::Q qStart, rw::math::Q qGoal, rw::trajectory::Path<rw::math::Q> &path, double maxTime, double epsilon)
  {
      ROS_INFO_STREAM("Call queryRRT");
      rw::pathplanning::PlannerConstraint _constraint = rw::pathplanning::PlannerConstraint::make(detector_, device_, state_);
      rw::pathplanning::QSampler::Ptr sampler = rw::pathplanning::QSampler::makeConstrained(
              rw::pathplanning::QSampler::makeUniform(device_),
              _constraint.getQConstraintPtr()
      );

      rw::math::QMetric::Ptr _qMetric = rw::math::MetricFactory::makeEuclidean<rw::math::Q>();

      rw::pathplanning::QToQPlanner::Ptr  _rrtPlanner = rwlibs::pathplanners::RRTPlanner::makeQToQPlanner(
              _constraint,
              sampler,
              _qMetric,
              epsilon,
              rwlibs::pathplanners::RRTPlanner::RRTConnect
              );

      ROS_INFO_STREAM("run queryRRT");
      bool vaild_path_found = _rrtPlanner->query(qStart, qGoal, path, maxTime);

      ROS_INFO_STREAM( "RRT found path " << std::to_string(vaild_path_found) << " size: " << path.size());

      if(path.size() > 1)
      {
          path.erase(path.begin());
      }

      if(path.size() > 4 )
      {
          ROS_INFO_STREAM("push back path");
          rwlibs::pathoptimization::PathLengthOptimizer lengthOptimizer = rwlibs::pathoptimization::PathLengthOptimizer(_constraint, _qMetric);
          lengthOptimizer.partialShortCut(path);
      }
      else if( path.size() < 1)
      {
          ROS_INFO_STREAM("push back single goal");
          path.push_back(qGoal);
      }
      ROS_INFO_STREAM("qeueryRRT end");

      return vaild_path_found;
  }


  rw::math::Q getCurrentJointConfiguration()
  {
    /* Make sure to get and operate on fresh data from the serial device
     * It's assumed that the serial device is not moving
     * ^- That could be asserted / verified using sdsip.isMoving()
     * However other sources could invoke services on the UR that causes it to move...
     */
    ros::Time current_timestamp = ros::Time::now();
    ros::Time obtained_timestamp = sdsip_.getTimeStamp();

    while (current_timestamp > obtained_timestamp)
    {
      ros::Duration(0.1).sleep();  // In seconds
      ros::spinOnce();
      obtained_timestamp = sdsip_.getTimeStamp();
    }
    //ROS_INFO_STREAM("GetCurrentJointValues timestamp: " << obtained_timestamp << std::endl);
    return sdsip_.getQ();
  }

  rw::trajectory::QPath getQPath(const rw::math::Q qDesired)
  {
    rw::math::Q start_configuration = getCurrentJointConfiguration();
    rw::math::Q end_configuration = qDesired;

    rw::trajectory::QPath path;
    bool valid_path = false;
    ROS_ASSERT(planner_);
    valid_path = planner_->query(start_configuration, end_configuration, path);

    if (!valid_path)
    {
      valid_path = queryRRT(start_configuration, end_configuration, path, 1000.0 , 0.02);
    }

    if(!valid_path)
    {
        path.clear();
    }

    return path;
  }


//  /*
//   * Inverse kinematic for the robot
//   * T desired must be converted to robot end Joint6
//  */
  bool inverseKinematics(rw::math::Transform3D<> endPose, rw::math::Q& inverseSolution ){

        //ROS_INFO_STREAM("Inverse kinematics called");
        rw::kinematics::State tempState = workcell_->getDefaultState();

      // Local variable
      bool invKinematic = false;

      // Inverse kinematics solver
      rw::invkin::JacobianIKSolver iksolver(device_, tempState);

      // Meta solver to keep in the joint limits
      rw::invkin::IKMetaSolver mSolver(&iksolver, device_, detector_);

      // Generate joint configurations
      std::vector<rw::math::Q> result;
      result = mSolver.solve( endPose , tempState, 50, false);

      if(result.size() == 0){
          // No solutions
          invKinematic = false;
          //ROS_ERROR("No inverse kinematics solution");
      }
      else
      {
          rw::math::Q bestQ(6);
          double differenceQ = 0.0;
          double bestN2Q = 100000.0;

          // Find the closets solution
          for( auto it = result.begin() ; it != result.end(); ++it)
          {
              // Get the current Q
              differenceQ = abs(it->norm2() - device_->getQ(state_).norm2());
              if(differenceQ < bestN2Q)
              {
                  bestQ = *it;
                  bestN2Q = differenceQ;
              }
          }
          invKinematic = true;
          inverseSolution = bestQ;
      }
      return invKinematic;
  }


 protected:

  std::mutex mutex_pointcloud;
  std::mutex mutex_image;

  ros::CallbackQueue queue_pathplanner;

  ros::NodeHandle nodehandle_;
  caros::SerialDeviceSIProxy sdsip_;

  ros::ServiceServer IKcheckPose_;
  ros::ServiceServer setCamPose_service_;
  ros::ServiceServer setBoundingBox_srv;
  ros::ServiceServer saveQtoFile_;
  ros::Publisher publishPointCloud_;

  ros::Subscriber sub_pointcloud_;
  sensor_msgs::PointCloud2 pointCloud_;

  unsigned int numimage;
  unsigned int rotation_steps;

  ros::Subscriber sub_image;
  cv::Mat image_;

  rw::models::WorkCell::Ptr workcell_;
  rw::kinematics::State state_;
  rw::models::Device::Ptr device_;
  rw::pathplanning::QToQPlanner::Ptr planner_;
  rw::proximity::CollisionDetector::Ptr detector_;

  rw::kinematics::MovableFrame* objectFrame_;
  rw::kinematics::Frame* camFrame_;
  rw::kinematics::MovableFrame* boundingFrame_;

  rw::math::Transform3D<> worldTObjectRef;
  rw::math::Transform3D<> worldTRobotBase;
  rw::math::Transform3D<> endTcam;

  rw::geometry::Box boundBoxGeo_;
  rw::geometry::Geometry::Ptr geoPtr_;
};

#endif // PATHPLANNIG_H
