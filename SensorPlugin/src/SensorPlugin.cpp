#include "SensorPlugin.hpp"

#include <rws/RobWorkStudio.hpp>
#include <boost/foreach.hpp>

#include <rw/sensor.hpp>
#include <rw/common/macros.hpp>
#include <rw/common/StringUtil.hpp>
#include <rw/kinematics/Kinematics.hpp>
#include <rw/math/Rotation3D.hpp>
#include <rw/math/Quaternion.hpp>

#include <rw/sensor/ImageUtil.hpp>
#include <rwlibs/simulation/SimulatedCamera.hpp>
#include <rwlibs/simulation/SimulatedScanner25D.hpp>
#include <rwlibs/simulation/SimulatedScanner2D.hpp>
#include <rwlibs/simulation/SimulatedKinect.hpp>
#include <rwlibs/opengl/RenderScan.hpp>
//#include <rwlibs/simulation/SimulatedScanner1D.hpp>

#include <boost/foreach.hpp>
#include <QMessageBox>

//PCL includes
#include <pcl/io/io.h>
#include <pcl/io/pcd_io.h>
#include <pcl/point_types.h>
#include <pcl/point_cloud.h>
#include <pcl/console/parse.h>
#include <pcl/features/normal_3d.h>
#include <pcl/common/transforms.h>
#include <pcl/visualization/cloud_viewer.h>
#include <pcl/visualization/pcl_visualizer.h>

// Standard library includes
#include <iostream>
#include <string>
#include <sstream>
#include <fstream>
#include <stdio.h>

using namespace rw::math;
using namespace rw::common;
using namespace rw::sensor;
using namespace rw::kinematics;
using namespace rw::models;
using namespace rwlibs::simulation;
using namespace rw::graphics;
using namespace rws;
using namespace rwlibs::opengl;

SensorPlugin::SensorPlugin() :
		RobWorkStudioPlugin("SensorPlugin", QIcon(":/pa_icon.png")) {
    std::cout << "Constructor " << std::endl;
    setupUi(this);
	// now connect stuff from the ui component

    _withNoiseCheckBox->setChecked(true);

    QDoubleValidator* validator_R = new QDoubleValidator(1,10,2,lineEdit_R);
    lineEdit_R->setValidator(validator_R);

    QDoubleValidator* validator_P = new QDoubleValidator(1,10,2,lineEdit_P);
    lineEdit_P->setValidator(validator_P);

    QDoubleValidator* validator_Y = new QDoubleValidator(1,10,2,lineEdit_Y);
    lineEdit_Y->setValidator(validator_Y);

	connect(_btn0, SIGNAL(pressed()), this, SLOT(btnPressed()));
    connect(_withNoiseCheckBox, SIGNAL(clicked()), this, SLOT(noiseChecked()));
    connect(_btnChangeRot, SIGNAL(pressed()), this, SLOT(btnChangeRotPressed()));
	rw::math::Math::seed();
	_wc = NULL;

}

SensorPlugin::~SensorPlugin() {

}

void SensorPlugin::initialize() {
	getRobWorkStudio()->stateChangedEvent().add(
			boost::bind(&SensorPlugin::stateChangedListener, this, _1), this);

    _view_frame_num = 1;
    _noise_model = true;

    std::cout << "Make tread and rosserver" << std::endl;
    _threadRosServer = new QThread;
    _rosServer = new rosServer();

     std::cout << "Move to thread" << std::endl;
    _rosServer->moveToThread(_threadRosServer);

    qRegisterMetaType< rw::math::Transform3D<double> >("rw::math::Transform3D<double>");

    std::cout << "Connect signals" << std::endl;
    connect(_threadRosServer, SIGNAL(started()),_rosServer, SLOT(run()));
    connect( _rosServer, SIGNAL(getPointCloudSignal(rw::math::Transform3D<double>)),this,
            SLOT(getPointCloudROS(rw::math::Transform3D<double>)), Qt::BlockingQueuedConnection);

    connect( _rosServer, SIGNAL(changeObjectPoseSig(rw::math::Transform3D<double>)),this,
            SLOT(setObjectPoseROS(rw::math::Transform3D<double>)), Qt::BlockingQueuedConnection);

    std::cout << "Start" << std::endl;
    _threadRosServer->start();
}

void SensorPlugin::open(WorkCell* workcell) {

	if (workcell == NULL) {
		std::cout << "OPEN NULL" << std::endl;
		return;
	} else {
		_wc = workcell;

		std::vector<rw::kinematics::Frame*> frames = _wc->getFrames();
		int framecount = frames.size();

        for(unsigned int i = 0; i < frames.size(); ++i)
        {
            std::cout << "Frame index: " << i << " name: " << (*frames[i]).getName() << std::endl;
        }

        _state = getRobWorkStudio()->getWorkcell()->getDefaultState();
		_state = workcell->getDefaultState();
        _gldrawer = getRobWorkStudio()->getView()->getSceneViewer();

        // Set the default values from the
        if(framecount > 1)
        {
            rw::kinematics::Frame::Ptr frame_objectgeo(_wc->findFrame("objectFrame"));

            // Set the default values from the
            rw::math::RPY<> temp_rpy(frame_objectgeo->getTransform(_state).R());

            lineEdit_R->setText(QString::number(temp_rpy[0],'f',4));
            lineEdit_P->setText(QString::number(temp_rpy[1],'f',4));
            lineEdit_Y->setText(QString::number(temp_rpy[2],'f',4));

        }

    }
}
void SensorPlugin::close() {
}

void SensorPlugin::noiseChecked()
{
    // Set the noise
    _noise_model = _withNoiseCheckBox->isChecked();
}

void SensorPlugin::btnChangeRotPressed()
{
    // Change the object pose

    rw::math::RPY<> temp_rpy;
    temp_rpy[0] = (lineEdit_R->text()).toDouble();
    temp_rpy[1] = (lineEdit_P->text()).toDouble();
    temp_rpy[2] = (lineEdit_Y->text()).toDouble();

    std::cout << "Set RPY btn" <<temp_rpy << std::endl;

    State state = getRobWorkStudio()->getState();
    Transform3D<> tempobject = Transform3D<>(Vector3D<>(0,0,0), temp_rpy.toRotation3D());

    changeObejctTransform(tempobject);

}

void SensorPlugin::btnPressed() {
	QObject *obj = sender();
    if (obj == _btn0) {

//		pcl::PointCloud<pcl::PointXYZRGBNormal>::Ptr merged_cloud(new pcl::PointCloud<pcl::PointXYZRGBNormal>);
		// Traverse the found kinect sensors and capture pointclouds
//		std::cout << _sensorComboBox->count() << std::endl;

//		for (signed int i = 0; i < _sensorComboBox->count(); i++) {
//			Frame* framei = _wc->findFrame(
//                    _sensorComboBox->itemText(i).toStdString());

//			if (framei->getPropertyMap().has("Scanner25D")) {
//				pcl::PointCloud<pcl::PointXYZRGBNormal>::Ptr rgbn = grabPointcloud(framei);

//				// Add points to mergedCloud and resize
//				for(unsigned int j=0;j<rgbn->points.size();j++){
//				merged_cloud->points.push_back(rgbn->points[j]);
//				}
//			}
//		}
		//save merged Cloud
//		pcl::io::savePCDFileBinary("MergedCloud.pcd",*merged_cloud);

    }
    else if (obj != _btn0 ) {
		// Traverse sensors and grasp camera images.
//		std::cout << _sensorComboBox->count() << std::endl;
//		for (signed int i = 0; i < _sensorComboBox->count(); i++) {
//			Frame* framei = _wc->findFrame(
//					_sensorComboBox->itemText(i).toStdString());
//			if (framei->getPropertyMap().has("Camera")) {
//                grabImage(   framei);
//				//Saving calibration for the individual cameras
//				SensorPlugin::CameraParameters camP = getCamPar(framei);
//				std::stringstream filename;
//				filename << framei->getName() << "_calibration.txt";
//				std::vector<SensorPlugin::CameraParameters> cm;
//				cm.push_back(camP);
//				saveCalibration(filename.str(), cm);
//			}
//
//		}
    }
}

void SensorPlugin::stateChangedListener(const State& state) {
	// Update local state variable when state change in RobWorkStudio
    _state = state;
}

/*
 * Grab 2D rgb images
 */
void SensorPlugin::grabImage(rw::kinematics::Frame* frame) {

	_gldrawer = getRobWorkStudio()->getView()->getSceneViewer();
	double fovy;
	int width, height;
	std::string camId("Camera");
	std::string camParam = frame->getPropertyMap().get<std::string>(camId);
	std::istringstream iss(camParam, std::istringstream::in);
	iss >> fovy >> width >> height;
	std::cout << fovy << " " << width << " " << height << std::endl;

	GLFrameGrabber::Ptr frameGrabber = ownedPtr(
			new GLFrameGrabber(width, height, fovy));
	frameGrabber->init(_gldrawer);
	State state = getRobWorkStudio()->getState();

	frameGrabber->grab(frame, state);

	const Image* img = &frameGrabber->getImage();
	std::stringstream filename0;
		filename0 << frame->getName() << "Non.ppm";
	img->saveAsPPM(filename0.str());
	Image::Ptr imgflipped;
	imgflipped = img->copyFlip(true, false);

	std::stringstream filename;
	filename << frame->getName() << ".ppm";
	std::cout << "Outputfile: " << filename.str() << std::endl;
	imgflipped->saveAsPPM(filename.str());
}

/*
 * Method for changing the object pose
 *
 */
void SensorPlugin::setObjectPoseROS(rw::math::Transform3D<double> trans)
{
    std::cout << "Changed object pose from ROS" << std::endl;
    std::cout << "RPY" << RPY<>(trans.R()) << std::endl;
    changeObejctTransform(trans);

    rw::kinematics::Frame::Ptr frame_objectgeo(_wc->findFrame("objectFrame"));

    // Set the default values from the
    rw::math::RPY<> temp_rpy(frame_objectgeo->getTransform(_state).R());

    lineEdit_R->setText(QString::number(temp_rpy[0],'f',4));
    lineEdit_P->setText(QString::number(temp_rpy[1],'f',4));
    lineEdit_Y->setText(QString::number(temp_rpy[2],'f',4));
}

/*
 * Method to get a point cloude to the ROS interface
 *
*/
void SensorPlugin::getPointCloudROS(rw::math::Transform3D<double> trans)
{
    std::cout << "SensorPlugin P:" << std::endl << trans.P() << " R: " << RPY<>(trans.R()) << std::endl;

    rw::kinematics::MovableFrame* viewFrame = dynamic_cast< rw::kinematics::MovableFrame*>(_wc->findFrame("View0"));
    viewFrame->setTransform(trans, _state);

    rw::kinematics::Frame* tempView = new FixedFrame(viewFrame->getName()+std::to_string(_view_frame_num),viewFrame->getTransform(_state));
    _view_frame_num++;

    // Add the new frame to RWS
    _wc->addFrame(tempView, _wc->getWorldFrame());
    _state = getRobWorkStudio()->getWorkcell()->getStateStructure()->upgradeState(_state);

    getRobWorkStudio()->setState(_state);
    getRobWorkStudio()->getWorkCellScene()->setVisible(true, tempView);

    MovableFrame* framei = dynamic_cast<MovableFrame*>(_wc->findFrame("Kinect1"));
    double fovy;
    int width,height;
    std::string camId("Scanner25D");
    std::string camParam = framei->getPropertyMap().get<std::string>(camId);
    std::istringstream iss (camParam, std::istringstream::in);
    iss >> fovy >> width >> height;

    // SimulatedSensor25D
    _gldrawer = getRobWorkStudio()->getView()->getSceneViewer();
    GLFrameGrabber25D::Ptr framegrabber25d = ownedPtr( new GLFrameGrabber25D(width, height,fovy) );
    framegrabber25d->init(_gldrawer);
    State stateScan25D = getRobWorkStudio()->getState();

    // Create and init 25D sensor
    SimulatedScanner25D* simscan25 = new SimulatedScanner25D("SimulatedScanner25D", framei, framegrabber25d);
    simscan25->open();

    simscan25->acquire();
    Simulator::UpdateInfo infoScan(1.0);
    simscan25->update(infoScan, stateScan25D);
    simscan25->close();

    //std::cout << "Now we prepare to get a scan!" << std::endl;
    rw::geometry::PointCloud cloud = simscan25->getScan();

    if(_noise_model == true)
    {
        applyNoise(cloud);
    }

    const std::vector<rw::math::Vector3D<float> > cloud_data = cloud.getData();

    std::cout << "Robwork studio cloud size: " << cloud.size() << std::endl;

    // Camera sensor
    _gldrawer = getRobWorkStudio()->getView()->getSceneViewer();
    GLFrameGrabber::Ptr frameGrabber = ownedPtr(
            new GLFrameGrabber(width, height, fovy));
    frameGrabber->init(_gldrawer);
    State stateCam = getRobWorkStudio()->getState();

    frameGrabber->grab(framei, stateCam);
    const rw::sensor::Image* img2D = &frameGrabber->getImage();

    //std::cout << "Capturing from: " << simscan25->getName()
         //<< "\nProperties\n\t" << "Width: " << cloud.getWidth() << "\n\t"
         //<< "Height: " << cloud.getHeight()
         //<< "\n\tBits per pixel: "
         //<< img2D->getBitsPerPixel() << "\n\t" << "ColorEncoding: "
         //<< img2D->getColorEncoding() << "\n\t" << "Pixeldepth: "
         //<< img2D->getPixelDepth() << "\n\t" << "Channels: "
         //<< img2D->getNrOfChannels()
         //<< std::endl;

     std::vector<Vector3D<float> > colors(
             img2D->getHeight() * img2D->getWidth());

     for (unsigned int j = 0; j < img2D->getHeight(); j++) {
         for (unsigned int i = 0; i < img2D->getWidth(); i++) {
             colors[(j * img2D->getWidth()) + i] = Vector3D<float>(
                     img2D->getPixelValue(i, j, 0),
                     img2D->getPixelValue(i, j, 1),
                     img2D->getPixelValue(i, j, 2));
        }
     }

    pcl::PointCloud<pcl::PointXYZRGB>::Ptr dst(new pcl::PointCloud<pcl::PointXYZRGB>());
    // Populate cloud
    for (unsigned int i = 0; i < cloud_data.size(); i++)
    {
         //larger than 3 meters remove background points
         if(cloud_data[i][2]>-1.5) //currently iterateing incorrectly
         {
         pcl::PointXYZRGB pn;
         pn.x = cloud_data[i][0];
         pn.y = -cloud_data[i][1];
         pn.z = -cloud_data[i][2];
         pn.r = (int)255*colors[i][0];
         pn.g = (int)255*colors[i][1];
         pn.b = (int)255*colors[i][2];
         dst->push_back(pn);
         }
     }

    //std::cout << "Coping the data over" << std::endl;
    _rosServer->_pointCloud = dst;
}

double calcSigma( double r, double d ){
    const double p00 = 2.344;
    const double p10 = -1.202E-2;
    const double p01 = -1.734E-3;
    const double p20 =  1.818E-5;
    const double p11 =  6.516E-6;
    const double p02 =  1.233E-6;
    return p00 + p10*r + p01*d + p20*r*r + p11*r*d + p02*d*d;
}

//
// Kinect noise model taken from Robwork's simulated kinect
//
void SensorPlugin::applyNoise(rw::geometry::PointCloud& scan){

    // walk through data and change z accordingly
    std::vector<Vector3D<float> > &data = scan.getData();

    double center_x = scan.getWidth()/2.0;
    double center_y = scan.getHeight()/2.0;

    for(int y=0;y<scan.getHeight();y++){
        for(int x=0;x<scan.getWidth();x++){
            Vector3D<float> &v = data[y*scan.getWidth() + x];
            double r = std::sqrt( Math::sqr(x - center_x) + Math::sqr(y - center_y) );
            // we convert to mm
            double d = (double)fabs(v[2])*1000.0;
            double sigma = calcSigma(r, d);
            // this will produce the error in m
            double noise_err = Math::ranNormalDist( 0 , sigma )/1000.0;
            v[2] += (float)noise_err;
        }
    }
}


void SensorPlugin::changeObejctTransform(rw::math::Transform3D<double> trans)
{
    // Change the object pose
    rw::kinematics::MovableFrame::Ptr frame_objectgeo = dynamic_cast< rw::kinematics::MovableFrame*>(_wc->findFrame("objectFrame"));

    State state = getRobWorkStudio()->getState();
    Transform3D<> tempobject = frame_objectgeo->getTransform(state) * trans;

    frame_objectgeo->setTransform(tempobject,state);
    getRobWorkStudio()->setState(state);
    getRobWorkStudio()->updateAndRepaint();
}

Q_EXPORT_PLUGIN(SensorPlugin)
