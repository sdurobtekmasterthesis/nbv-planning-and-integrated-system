#ifndef NBV_PLANETARIAN_H
#define NBV_PLANETARIAN_H

#include "pcl/point_types.h"
#include "pcl/point_cloud.h"
#include "pcl/octree/octree.h"
#include "Eigen/Core"
#include "vector"
#include <fstream>

struct transform
{
    Eigen::Vector3d translation;
    Eigen::Quaterniond rotation;
};

static bool load_data = false;

class NBV_Planetarian
{
    public:
        NBV_Planetarian(double sphere_size=1,
                        int views = 100,
                        int max_iterations = 20,
                        int stopping_threshold = 100,
                        int resolution_camera_update = 16,
                        int resolution_camera_evaluation = 32,
                        bool &load_view_points = load_data,
                        bool log_data = false,
                        std::string filename = "data_default");
        transform next(const pcl::PointCloud<pcl::PointXYZ> &pointCloud, transform current, bool &done);
        //void resize_sphere();
        void set_filename(std::string filename);
        int get_iteration();
        void rotate_scene(transform object_movement);
        transform hybrid(std::vector<transform> options, bool &change);

        std::vector<transform> getViewPoints()
        {
            return view_points;
        }
        void setViewPoints(std::vector<transform> view_points);

    private:
        void generateViewpointsUniform(int iterationCount, bool hemisphere);
        void instantiateOctree();
        void instantiateDenseSearchOctree(pcl::PointXYZ center, double bounding_box, double voxel_resolution);
        int updateOctreePCL(const pcl::PointCloud<pcl::PointXYZ> &pointCloud);
        int updateExploredOctreePCL();
        double evaluateViewPCL(transform view);
        //void resize_sphere(double distance);
        bool check_closed_list(int view);

        void info_resolution();
        void info_information(int iteration, int voxels_found, int view, transform pose);

        void saveViewPoints(std::vector<transform> views);
        std::vector<transform> loadViewPoints();

        // New ray intersection
        pcl::octree::OctreePointCloudSearch<pcl::PointXYZ>::AlignedPointTVector
        findIntersectedVoxels( Eigen::Vector3d translation, Eigen::Quaternion<double> quaternion, Eigen::Matrix4d camMaxtrix, pcl::PointXY pixel_step, bool update_octree = true);

        pcl::octree::OctreePointCloudSearch<pcl::PointXYZ> octree;
        pcl::PointCloud<pcl::PointXYZ>::Ptr octree_cloud;

        pcl::octree::OctreePointCloudSearch<pcl::PointXYZ> explored_octree;
        pcl::PointCloud<pcl::PointXYZ>::Ptr explored_octree_cloud;

        pcl::octree::OctreePointCloudSearch<pcl::PointXYZ> octree_dense_search;
        pcl::PointCloud<pcl::PointXYZ>::Ptr octree_dense_search_cloud;

        int iteration_counter;
        int iteration_max;
        int views;
        int voxels_threshold;
        Eigen::Matrix4d global_pose;
        transform current_pose;
	//these view points are row major, so carefull when combining with other system
        std::vector<transform> view_points;
        std::vector<int> closed_list;

        pcl::PointXY res_update;
        pcl::PointXY res_evaluation;
        bool addaptive;
        int *addaptive_resolution;
        int best;
        double best_score;
        std::ofstream data_file;
        bool log_data;
        bool done;
        double sphere;
	std::string filename;
};

#endif // PLANETARIAN_H
