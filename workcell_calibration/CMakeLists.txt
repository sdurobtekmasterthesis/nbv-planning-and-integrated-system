project(workcell_calibration)
cmake_minimum_required(VERSION 2.8.6)

set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -std=c++11")
set(CMAKE_INCLUDE_CURRENT_DIR ON)


# We use the settings that robwork studio uses
SET(CMAKE_BUILD_TYPE Debug)
SET(RW_ROOT "$ENV{RW_ROOT}")

#Include default settings for constructing a robwork dependent project
FIND_PACKAGE(
  RobWork
  QUIET
  HINTS
    "${RW_ROOT}")
#The following is a workaround for the old version of RobWork. Remove once no longer needed (everyone switched to a RobWork version newer than April 2012)
IF(NOT ROBWORK_FOUND)
  MESSAGE(STATUS "Could not find RobWork with the new method. Trying the old one.")
  INCLUDE(${RW_ROOT}/build/FindRobWork.cmake)
ENDIF(NOT ROBWORK_FOUND)

find_package(OpenCV 3.1.0 REQUIRED )
include_directories(${OpenCV_INCLUDE_DIR})
link_directories( ${OpenCV_LIBS} )
add_definitions(${OpenCV_DEFINITIONS})

INCLUDE_DIRECTORIES( ${ROBWORK_INCLUDE_DIRS} ${ROBWORKSTUDIO_INCLUDE_DIRS} ${OpenCV_INCLUDE_DIR})
LINK_DIRECTORIES(${ROBWORK_LIBRARY_DIRS} ${ROBWORKSTUDIO_LIBRARY_DIRS} ${catkin_LIBRARIES})

MESSAGE( STATUS "Robwork includedirs ${ROBWORK_INCLUDE_DIRS}" )

SET(SrcFiles src/workcell_calibration.cpp)

add_executable(${PROJECT_NAME} ${SrcFiles})
target_link_libraries(${PROJECT_NAME} ${ROBWORK_LIBRARIES} ${OpenCV_LIBS})
MESSAGE(${PROJECT_NAME} "Done!")
