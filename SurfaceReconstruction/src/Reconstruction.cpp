#include "Reconstruction.h"
#include <pcl/io/pcd_io.h>
#include <pcl/io/ply_io.h>
#include <fstream>

//viewer stuff
#include <pcl_conversions/pcl_conversions.h>
#include <pcl/visualization/cloud_viewer.h>
#include <pcl/visualization/pcl_visualizer.h>
#include "../../KdTreeNormal/src/KdTreeNormal.h"

#include "../../NBV_algorithms/Shared/Visulisation.h"

Reconstruction::Reconstruction(bool log_data, std::string filename):
clouds(new pcl::PointCloud<pcl::PointNormal>() ),
clouds_2(new pcl::PointCloud<pcl::PointNormal>() )
{
    this->log_data = log_data;
    setFilename(filename);
    name_number = 1;
}

void Reconstruction::setFilename(std::string filename)
{
    if(log_data)
    {
        this->filename = filename;
        boost::filesystem::path dir("../../../Data/" + filename + "/Reconstruction");
        if(!(boost::filesystem::exists(dir)))
        {

            if(!boost::filesystem::create_directories(dir))
            {
                ROS_ERROR("Directory could not be created");
            }
        }

    }
}


pcl::PolygonMesh::Ptr Reconstruction::marchingCubes()
{
    // Create search tree*
    //pcl::search::KdTree<pcl::PointNormal>::Ptr tree (new pcl::search::KdTree<pcl::PointNormal>);
    //tree->setInputCloud (clouds);
    KdTreeNormal::Ptr tree (new KdTreeNormal);
    tree->setInputCloud (clouds);

    std::cout << "begin marching cubes reconstruction" << std::endl;

    float default_iso_level = 0.0f;
    float default_extend_percentage = 0.0f;
    int default_grid_res = 50;
    float default_off_surface_displacement = 0.01f;

    pcl::PolygonMesh::Ptr triangles(new  pcl::PolygonMesh);
    pcl::MarchingCubesHoppe<pcl::PointNormal> mc;
    //mc.setOffSurfaceDisplacement(default_off_surface_displacement);
    mc.setIsoLevel(default_iso_level);
    mc.setPercentageExtendGrid(default_extend_percentage);
    //mc.setSearchMethod(tree);
    mc.setGridResolution(default_grid_res,default_grid_res,default_grid_res);

    mc.setInputCloud (clouds);
    mc.reconstruct (*triangles);

    return triangles;
}

pcl::PolygonMesh::Ptr Reconstruction::greedyTriangulation()
{
    //pcl::search::KdTree<pcl::PointNormal>::Ptr tree2 (new pcl::search::KdTree<pcl::PointNormal>);
    //tree2->setInputCloud (clouds);
    KdTreeNormal::Ptr tree2 (new KdTreeNormal);
    tree2->setInputCloud (clouds);

    //Visulisation::showNormals(cloud, cloud_with_normals);

    // Initialize objects
    pcl::GreedyProjectionTriangulation<pcl::PointNormal> gp3;
    pcl::PolygonMesh::Ptr triangles(new pcl::PolygonMesh);

    // Set the maximum distance between connected points (maximum edge length)
    gp3.setSearchRadius (0.015);

    // Set typical values for the parameters
    gp3.setMu (2.5);
    gp3.setMaximumNearestNeighbors (150);
    gp3.setMaximumSurfaceAngle(M_PI/4); // 45 degrees
    gp3.setMinimumAngle(M_PI/18); // 10 degrees
    gp3.setMaximumAngle(2*M_PI/3); // 120 degrees
    gp3.setNormalConsistency(true);

    // Get result
    gp3.setInputCloud (clouds);
    gp3.setSearchMethod (tree2);
    //gp3.reconstruct (*triangles);

    // Additional vertex information
    std::vector<int> parts = gp3.getPartIDs();
    std::vector<int> states = gp3.getPointStates();

    return triangles;
}

void Reconstruction::addCloudConsistentNormals(pcl::PointCloud<pcl::PointXYZ>::Ptr cloud, Eigen::Vector3d dir)
{
    // Normal estimation*
    pcl::NormalEstimation<pcl::PointXYZ, pcl::Normal> n;
    pcl::PointCloud<pcl::Normal>::Ptr normals (new pcl::PointCloud<pcl::Normal>);
    pcl::search::KdTree<pcl::PointXYZ>::Ptr tree (new pcl::search::KdTree<pcl::PointXYZ>);
    tree->setInputCloud (cloud);
    n.setInputCloud (cloud);
    n.setSearchMethod (tree);
    n.setKSearch (20);
    n.compute (*normals);
    //* normals should not contain the point normals + surface curvatures

    for(int i=0; i<normals->size(); i++)
    {
        Eigen::Vector3d normal_dir(normals->at(i).normal_x, normals->at(i).normal_y, normals->at(i).normal_z);
        //std::cout << "dot product: " << dir.dot(normal_dir) << std::endl;
        if(dir.dot(normal_dir) > 0.0)
        {
            //flip
            normals->at(i).normal_x = -normals->at(i).normal_x;
            normals->at(i).normal_y = -normals->at(i).normal_y;
            normals->at(i).normal_z = -normals->at(i).normal_z;
        }
    }

    // Concatenate the XYZ and normal fields*
    pcl::PointCloud<pcl::PointNormal> cloud_with_normals;
    pcl::concatenateFields (*cloud, *normals, cloud_with_normals);
    //* cloud_with_normals = cloud + normals
    *clouds += cloud_with_normals;
}

void Reconstruction::saveCloud()
{
    saveCloud(clouds);
}

void Reconstruction::saveCloud( pcl::PointCloud<pcl::PointNormal>::Ptr clouds)
{        
    if(log_data)
    {
        pcl::io::savePLYFile("../../../Data/" + filename + "/Reconstruction/fixed_cloud" + std::to_string(name_number) + ".ply", *clouds);
        if(name_number == 1)
        {
            clouds_2 = this->clouds->makeShared();
            this->clouds->clear();
            //clouds_2 = pcl::PointCloud<pcl::PointNormal>::Ptr(new pcl::PointCloud<pcl::PointNormal>(*(this->clouds)));
        }
        name_number++;
    }    

}

void Reconstruction::combineCloud(Eigen::Matrix4d transform)
{
    pcl::PointCloud<pcl::PointNormal>::Ptr finalCloud(new pcl::PointCloud<pcl::PointNormal>);
    pcl::transformPointCloud(*clouds_2, *finalCloud, transform.cast<float>());
    *finalCloud += *clouds;

    if(log_data)
    {
        std::ofstream global_file("../../../Data/" + filename + "/Reconstruction/global.txt");
        global_file << transform << std::endl;
        global_file.close();
    }

    saveCloud(finalCloud);

}

