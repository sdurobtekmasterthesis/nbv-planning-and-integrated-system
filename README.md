# NBV Planning and Interface #

### What is this repository for? ###

This repository gives next best view planning based on point cloud input and also gives a full implemented system that gives a GUI for easy use. This project is free to use as desired, but an acknowledgment when using the system or the implemented algorithms is appreciated.

### Dependencies ###
Needed libaries:
ROS(http://wiki.ros.org/indigo/Installation/Ubuntu)

PCL(http://pointclouds.org/documentation/tutorials/compiling_pcl_posix.php#compiling-pcl-posix) 1.7.0.2 should be sufficient but 1.8.0 is used
Weird installations

If you happen to install in some non obvious repository (let us say in Documents for evils) then you can help cmake find PCLConfig.cmake adding this line:

set(PCL_DIR "/path/to/PCLConfig.cmake")
before find_package.

Robwork(Only simulator and robot controll http://www.robwork.dk/apidoc/nightly/rw/)

CAROS (robot manipulation https://gitlab.com/caro-sdu/caros )

Qt compiler(Only GUI)

BOOST(comes with other)

Eigen(comes with other)

### Compiling and Running ###
To compile create a catkin folder and run catkin_make.

To run use the launch script in the launch folder

-Setup environment variables
All the dependencies environment variables should be setup accordingly to there installation guides.
The following environment variables has to be added to the bash shell:

* RW_ROOT - RobWork folder

* RWS_ROOT - RobWorkStudio folder

* RWHW_ROOT - RobWorkHardware folder

* MASTER_CATKIN_ROOT (The catkin root folder)

### Folder Structure ###
Each folder contains interface.cpp which couples the functionality to ROS communication, so another interface could be applied
This repository contains:

-NBV_algorithms

* view_picker - this takes views from other algorithms and is used to make a pick system
* planetarium - This implements the planetarium algorithm which is the most basic and used algorithm for NBV
* normal - uses a normal/surface based approach for less constrained views* 
* shared - Implements some functionalities that can be used in more code. Currently some ros translation, pcl viewer for easy use and quaternion calculations for setting views pointing towards the center
* NBV_Random_ros - gives random view and is for testing

-GUI - Does not contain a interface file because combining qt and ros changes the system. Is coupled as an action server that continuously receives feedback from the view_picker and shows to the user.

-ImageRegistration - Implements least trimmed squares for registration and filtering of point cloud data

-SurfaceReconstruction - Implements surface reconstruction to obtain a cad model

-Workcells - Workcell for the simulator

-SensorPlugin - A simulator loading workcells and giving views requested by the system

### Who do I talk to? ###
* Morten Gundesen mogun11@student.sdu.dk
* Rolf Green rogre11@student.sdu.dk