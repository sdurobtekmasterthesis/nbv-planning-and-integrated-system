#include "nbv_surfacebased.h"

#include "../../Shared/Quaternion.h"
#include "../../Shared/libaryTranslation.h"

#include "ros/ros.h"
#include <std_msgs/Bool.h>
#include <std_msgs/String.h>
#include <geometry_msgs/Transform.h>
#include <surfacebased/pose_array.h>
#include <simsensorplugin/pointCloud.h>
#include <view_pick/pose.h>
#include <pathplanner/isPoseValid.h>

#include <sensor_msgs/PointCloud2.h>
#include <pcl_conversions/pcl_conversions.h>
#include <pcl/visualization/cloud_viewer.h>
#include <pcl/visualization/pcl_visualizer.h>

// Boost timing headers
#include "boost/date_time/posix_time/posix_time.hpp"
typedef boost::posix_time::ptime Time;
typedef boost::posix_time::time_duration TimeDuration;

pcl::PointCloud<pcl::PointXYZRGB> cloudRGB;
pcl::PointCloud<pcl::PointXYZ> cloud;
transform pose;
std::string filename;
ros::Publisher pose_message;
ros::Publisher option_message;
ros::Publisher done_message;
ros::ServiceClient pose_client;

NBV_surfacebased* method;
bool done = false;

//parameters
bool log_data;
int max_iterations;
double object_distance, filter_distance;


//function for datalogging of time usage
void datalog(int points, long time)
{
    static bool started = false;
    static std::ofstream file("surface_times.txt");

    if(started == false)
    {
        std::cout << "logging data: planetarium" << std::endl;
        file << "points, time" << std::endl;
        started = true;
    }

    file << points << ", "<< time/1000.0 << std::endl;
}


void resetCallback(const std_msgs::BoolConstPtr reset)
{
    delete method;
    method = new NBV_surfacebased(log_data, max_iterations, object_distance, filter_distance, filename);
}

void filenameCallback(const std_msgs::StringConstPtr file)
{
    filename = file->data;
    method->set_filename(file->data);
}


void turnCallback(const geometry_msgs::TransformConstPtr turn)
{

    transform ransac_transform;
    ransac_transform.translation = Libary::toEigenVector3FromROS(*turn);
    ransac_transform.rotation = Libary::toEigenQuaternionFromROS(*turn);
    method->rotate_scene(ransac_transform);
}


void pointCloudCallback(const simsensorplugin::pointCloudPtr pointCloud)
{

    sensor_msgs::PointCloud2 data = pointCloud->pointcloud;
    transform received;
    received.translation = Libary::toEigenVector3FromROS(pointCloud->transform);
    received.rotation = Libary::toEigenQuaternionFromROS(pointCloud->transform);
    received.rotation = received.rotation.inverse();

    pcl::fromROSMsg(data, cloudRGB);
    pcl::copyPointCloud(cloudRGB, cloud);

    std::vector<transform> options = method->next(cloud, done);
    //pose = method->selectView(options, done);

    surfacebased::pose_array pose_array_msg;
    std::vector<transform> options_no_collision;
    pose_array_msg.header.stamp = ros::Time::now();
    pathplanner::isPoseValid::Request  req;
    pathplanner::isPoseValid::Response res;
    for(unsigned int i=0; i < options.size(); i++)
    {
        req.transf = Libary::toROSFromEigen(options[i].translation, options[i].rotation);
        pose_client.call(req, res);
        if(res.isValid && options[i].translation.z() > 0)
        {
            pose_array_msg.transform.push_back(Libary::toROSFromEigen(options[i].translation, options[i].rotation));
            options_no_collision.push_back(options[i]);
        }
    }
    option_message.publish(pose_array_msg);
    pose = method->selectView(options_no_collision, done);


    //publish current pose
    if(done == false)
    {
        view_pick::pose pose_msg;
        pose_msg.header.stamp = ros::Time::now();
        pose_msg.transform = Libary::toROSFromEigen(pose.translation, pose.rotation);
        pose_msg.score = 0;
        pose_message.publish(pose_msg);
    }
    //publish a done signal
    else
    {
        done = false;
        std_msgs::Bool msg;
        msg.data = true;
        done_message.publish(msg);
    }

}

int main(int argc, char **argv)
{
    ros::init(argc, argv, "surfacebased_node");
    ros::NodeHandle nodeHandler("surfacebased");

    //local handle for parameters
    ros::NodeHandle n("~");
    //load parameters
    n.param<bool>("log_data", log_data, false);
    n.param<int>("max_iterations", max_iterations, 30);
    n.param<double>("object_distance", object_distance, 0.3);
    n.param<double>("filter_distance", filter_distance, 0.05);

    //Create Method
    method = new NBV_surfacebased(log_data, max_iterations, object_distance, filter_distance);

    done_message = nodeHandler.advertise<std_msgs::Bool>("finished", 1);
    pose_message = nodeHandler.advertise<view_pick::pose>("pose", 1);
    option_message = nodeHandler.advertise<surfacebased::pose_array>("options", 6);

    ros::Subscriber sub_cloud = nodeHandler.subscribe("/registration/PointCloudMsg_Part", 4, pointCloudCallback);
    ros::Subscriber sub_reset = nodeHandler.subscribe("/view_pick_node/reset", 4, resetCallback);
    ros::Subscriber sub_turn = nodeHandler.subscribe("/registration/Global_Transform", 4, turnCallback);
    ros::Subscriber sub_file = nodeHandler.subscribe("/GUI/filename", 4, filenameCallback);
    pose_client = nodeHandler.serviceClient<pathplanner::isPoseValid>("/pathplanner/isPoseValid");

    ros::Rate r(10);
    ros::spin();

    return 0;
}
