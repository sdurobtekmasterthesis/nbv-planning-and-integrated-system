#include "pclviewer.h"
#include "ui_pclviewer.h"
#include "rosserver.h"
#include "eigen3/Eigen/Core"
#include "eigen3/Eigen/Geometry"
#include <pcl/io/ply_io.h>
#include <QIcon>

PCLViewer::PCLViewer (QWidget *parent) :
  QMainWindow (parent),
  ui (new Ui::PCLViewer)
{
    iteration_nr = 0;
    midway = 0;
    pause = true;
    finished = false;

    ui->setupUi (this);
    this->setWindowTitle ("Next Best View Planning");

    // Setup the cloud pointer
    // The number of points in the cloud

    // Set up the QVTK window
    viewer.reset (new pcl::visualization::PCLVisualizer ("viewer", false));
    ui->qvtkWidget->SetRenderWindow (viewer->getRenderWindow ());
    viewer->setupInteractor (ui->qvtkWidget->GetInteractor (), ui->qvtkWidget->GetRenderWindow ());
    ui->qvtkWidget->update ();

    qRegisterMetaType< geometry_msgs::Transform >("geometry_msgs::Transform");

    //start ros interface
    ros_server_thread = new QThread;
    server = new RosServer;
    server->moveToThread(ros_server_thread);

    //Connect the ros server for spinning and the start button
    connect(ros_server_thread, SIGNAL(started()), server, SLOT(run()));
    connect(ui->pushButton_start, SIGNAL(clicked()), this, SLOT(startButton()));
    connect(this, SIGNAL(send_goal()), server, SLOT(send_goal()));
    connect(ui->pushButton_reset, SIGNAL(clicked()), this, SLOT(resetButton()));
    connect(this, SIGNAL(send_reset()), server, SLOT(send_reset()));
    connect(ui->pushButton_save, SIGNAL(clicked()), this, SLOT(saveButton()));
    connect(ui->pushButton_color, SIGNAL(clicked()), this, SLOT(colorButton()));
    connect(ui->comboBox_side, SIGNAL(currentIndexChanged(int)), this, SLOT(sideBox(int)));

    ros_server_thread->start();

    //connect the gui when a new point cloud is ready

    connect (server, SIGNAL (updatePointCloud()), this, SLOT (updatePointCloud()), Qt::BlockingQueuedConnection);
    connect (server, SIGNAL (updateIteration(double)), this, SLOT (updateIteration(double)));
    connect (server, SIGNAL (updatePose(geometry_msgs::Transform)), this, SLOT (updatePose(geometry_msgs::Transform)));
    connect (server, SIGNAL (done(int)), this, SLOT (done(int)) );

    // Connect point size slider
    connect (ui->horizontalSlider_view, SIGNAL (valueChanged (int)), this, SLOT (viewSliderValueChanged (int)));

    //ui->label_infotext->setVisible(true);
    ui->label_infotext->setText("<font color='green'>Place object to scan</font>");

    viewer->addCoordinateSystem(0.05);
    /*pcl::ModelCoefficients sphere;
    sphere.values.resize(4);
    sphere.values[0] = 0;
    sphere.values[1] = 0;
    sphere.values[2] = 0;
    sphere.values[3] = 1;
    viewer->addSphere(sphere);*/
    viewer->resetCamera ();

    //set icon
    //QIcon icon("/home/morten/icon.png");
    QIcon icon("../../../src/GUI/icon.png");
    this->setWindowIcon(icon);

    ui->qvtkWidget->update ();
}

void PCLViewer::colorButton ()
{
    static bool black = true;
    if(black)
    {
        black = false;
        viewer->setBackgroundColor(255,255,255);
    }
    else
    {
        black = true;
        viewer->setBackgroundColor(0,0,0);
    }
    ui->qvtkWidget->update ();
}

void PCLViewer::sideBox(int index)
{
    updateCloudViewer(ui->horizontalSlider_view->value());
}

void PCLViewer::startButton ()
{
    if(finished)
    {
        ui->pushButton_reset->setEnabled(true);
        ui->pushButton_reset->setText("Reset");

        resetButton();
    }
    else
    {
        if(pause)
        {
            ui->pushButton_start->setText("Pause");
            ui->pushButton_reset->setEnabled(false);
            ui->pushButton_reset->setText("Reset(pause first)");
            ui->label_infotext->setText("<font color='green'>Please wait for scanning</font>");
            pause = false;
        }
        else
        {
            ui->pushButton_start->setText("Resume");
            ui->pushButton_reset->setEnabled(true);
            ui->pushButton_reset->setText("Reset");
            ui->label_infotext->setText("<font color='green'>Paused</font>");
            pause = true;
        }

    }
    emit send_goal();
}

void PCLViewer::resetButton ()
{
    if(pause)
    {
        ui->label_infotext->setText("<font color='green'>Place object to scan</font>");
        ui->pushButton_start->setText("Start");
        ui->comboBox_side->setEnabled(false);
        ui->comboBox_side->setCurrentIndex(0);
        for(unsigned int i=0; i<clouds.size(); i++)
        {
            viewer->removePointCloud ("cloud" + std::to_string(i+1) );
            viewer->removeCoordinateSystem("pose" + std::to_string(i+1));
        }
        clouds.clear();
        view_points.clear();
        ui->lcdNumber->display(0);

        iteration_nr = 0;
        midway = 0;
        ui->horizontalSlider_view->setMaximum(0);
        ui->qvtkWidget->update ();

        //this is ugly logic, but currently the rosserver handles the reset in the state machine
        if(finished)
        {
            finished = false;
        }
        else
        {
            emit send_reset();
        }
    }
}

void PCLViewer::saveButton()
{
    for(unsigned int i=0; i<clouds.size(); i++)
    {
        pcl::io::savePLYFile("cloud" + std::to_string(i+1) + ".ply", *clouds[i]);
    }
}


void PCLViewer::updatePointCloud ()
{
    std::cout << "update point cloud" << std::endl;
    clouds.push_back(pcl::PointCloud<pcl::PointXYZRGB>::Ptr(new pcl::PointCloud<pcl::PointXYZRGB>(*server->_pointCloud)));
    ui->horizontalSlider_view->setMaximum(iteration_nr);

    updateCloudViewer(-1);
}

//-1 is when the function have a new cloud
//-2 is when there is a new pose
//0-max is from the slider
void PCLViewer::updateCloudViewer (int view)
{
    unsigned int init;
    unsigned int end;

    switch (ui->comboBox_side->currentIndex())
    {
    case 0:
        std::cout << "both sides" << std::endl;
        init = 0;
        end = clouds.size();
        break;
    case 1:
        std::cout << "first sides" << std::endl;
        init = 0;
        end = midway;
        break;
    case 2:
        std::cout << "second sides" << std::endl;
        init = midway;
        end = clouds.size();
        break;
    default:
        throw("how did you even!!!");
        break;
    }

    if(view == -1 && ui->horizontalSlider_view->value() == 0)
    {
        if(end == (unsigned int)iteration_nr)
        {
            std::cout << "adding cloud: " << "cloud" + std::to_string(iteration_nr) << std::endl;
            viewer->addPointCloud (clouds[iteration_nr-1], "cloud" + std::to_string(iteration_nr) );
        }
    }
    if(view == -2 && ui->horizontalSlider_view->value() == 0)
    {
        if(end == (unsigned int)iteration_nr)
        {
            viewer->addCoordinateSystem(0.1, view_points[iteration_nr-1], ("pose" + std::to_string(iteration_nr)) );
        }
    }

    if(view == 0)
    {
        for(unsigned int i = 0; i<clouds.size(); i++)
        {
            viewer->removePointCloud ("cloud" + std::to_string(i+1) );
            viewer->removeCoordinateSystem("pose" + std::to_string(i+1));
        }


        for(unsigned int i = init; i<end; i++)
        {
            viewer->addPointCloud (clouds[i], "cloud" + std::to_string(i+1) );
            viewer->addCoordinateSystem(0.1, view_points[i], "pose" + std::to_string(i+1));
        }
    }
    if(view > 0)
    {
        for(unsigned int i=0; i<clouds.size(); i++)
        {
            viewer->removePointCloud ("cloud" + std::to_string(i+1) );
            viewer->removeCoordinateSystem("pose" + std::to_string(i+1));
        }
        std::cout << "showing cloud: " << "cloud" + std::to_string(view) << std::endl;
        viewer->addPointCloud (clouds[view-1], "cloud" + std::to_string(view) );
        viewer->addCoordinateSystem (0.1, view_points[view-1], "pose" + std::to_string(view) );
    }

    ui->qvtkWidget->update ();
}


void PCLViewer::updateIteration ( double value )
{
    ui->lcdNumber->display((int)value);
    iteration_nr = (int)value;
    ui->qvtkWidget->update();
}

void PCLViewer::done (int state)
{
    if(state == 0)
    {
        midway = iteration_nr;
        ui->comboBox_side->setEnabled(true);
        ui->comboBox_side->setCurrentIndex(2);

        updateCloudViewer(ui->horizontalSlider_view->value());
        pause = true;
        ui->pushButton_start->setText("Continue");
        ui->label_infotext->setText("<font color='green'>Turn the object upside down</font>");
    }
    if(state == 1)
    {
        pause = true;
        finished = true;
        ui->pushButton_start->setText("Wait");
        ui->pushButton_start->setEnabled(false);
        ui->pushButton_reset->setText("");
        ui->pushButton_reset->setEnabled(false);
        ui->label_infotext->setText("<font color='green'>Wait for matching sides</font>");
    }
    if(state == 2)
    {
        ui->pushButton_start->setEnabled(true);
        ui->pushButton_start->setText("Restart");
        ui->label_infotext->setText("<font color='green'>Done scanning the object</font>");
    }

    if(state == -1)
    {
        throw("something went wrong in GUI logic");
    }
}

void PCLViewer::updatePose(geometry_msgs::Transform pose)
{
    std::cout << "new pose" << std::endl;
    Eigen::Vector3f p;
    p[0] = pose.translation.x;
    p[1] = pose.translation.y;
    p[2] = pose.translation.z;
    Eigen::Quaternionf r;
    r.w() = pose.rotation.w;
    r.x() = pose.rotation.x;
    r.y() = pose.rotation.y;
    r.z() = pose.rotation.z;
    Eigen::Affine3f transform;

    transform.fromPositionOrientationScale(p,r, Eigen::Vector3f::Ones());
    view_points.push_back(transform);

    updateCloudViewer(-2);
}

void PCLViewer::viewSliderValueChanged (int value)
{
    if(value == 0)
    {
        ui->label_view->setText( QString::fromStdString("All") );
    }
    else
    {
        ui->label_view->setText( QString::fromStdString(std::to_string(value)) );
    }
    updateCloudViewer(value);
}



PCLViewer::~PCLViewer ()
{
  delete ui;
}
