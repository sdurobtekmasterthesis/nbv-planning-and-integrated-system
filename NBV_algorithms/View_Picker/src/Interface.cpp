#include "ros/ros.h"
#include "geometry_msgs/Transform.h"
#include "simsensorplugin/pointCloud.h"
#include "simsensorplugin/getPointCloud.h"
#include "view_pick/pose.h"
#include "view_pick/feedback.h"
#include <sensor_msgs/PointCloud2.h>
#include <sensor_msgs/point_cloud2_iterator.h>
#include <std_msgs/Bool.h>
#include "../../Shared/Quaternion.h"

#include <actionlib/server/simple_action_server.h>
#include "view_pick/nbv_plannerAction.h"
#include <string>
#include <vector>

class ViewPickAction
{
protected:

    ros::NodeHandle nh_;
    // NodeHandle instance must be created before this line. Otherwise strange error may occur.
    actionlib::SimpleActionServer<view_pick::nbv_plannerAction> as_;
    std::string action_name_;
    // create messages that are used to published feedback/result
    view_pick::nbv_plannerFeedback feedback_;
    view_pick::nbv_plannerResult result_;

    //create object to connect relevant topics
    ros::Subscriber sub_planetarian_;
    ros::Subscriber sub_done_;
    ros::Subscriber sub_surfacebased_;
    ros::Subscriber sub_done_surface_;
    ros::Subscriber sub_pointCloud_;
    ros::Subscriber sub_pause_;


    ros::Publisher feedback_signal_;
    ros::Publisher reset_signal_;

    ros::ServiceClient client_;

    int iteration_counter;
    sensor_msgs::PointCloud2 cloud;
    geometry_msgs::Transform pose;

    //variable to say if the system is paused, could be built into mode, but this is easier code to read
    bool pause;


    //logic to secure syncronistation on pause reset and normal operation
    bool new_cloud;
    bool new_pose;
    bool data_updated;
    bool first_iteration;
    //start position for first iteration
    double start[3];
    int mode;
    bool delay_registration;

    //variables to store data from outcoming methods
    bool done;
    bool done_surface;
    std::vector<view_pick::pose> planetarian_list;
    std::vector<view_pick::pose> surfacebased_list;

public:

    ViewPickAction(std::string name) :
    as_(nh_, name, boost::bind(&ViewPickAction::executeCB, this, _1), false),
    action_name_(name)
    {
        new_cloud = false;
        new_pose = false;
        pause = false;
        iteration_counter = 0;

        sub_planetarian_ = nh_.subscribe("/planetarian/pose", 20, &ViewPickAction::planetarian_read, this);
        sub_done_ = nh_.subscribe("/planetarian/finished", 20, &ViewPickAction::planetarian_done, this);
        sub_surfacebased_ = nh_.subscribe("/surfacebased/pose", 20, &ViewPickAction::surfacebased_read, this);
        sub_done_surface_ = nh_.subscribe("/surfacebased/finished", 20, &ViewPickAction::surfacebased_done, this);
        sub_pointCloud_ = nh_.subscribe("/registration/PointCloudMsg_Part", 20, &ViewPickAction::pointCloudUpdate, this);
        sub_pause_ = nh_.subscribe("/GUI/pause", 20, &ViewPickAction::pauseCb, this);

        feedback_signal_ = nh_.advertise<view_pick::feedback>("/view_pick_node/feedback_queue", 5);
        reset_signal_ = nh_.advertise<std_msgs::Bool>("/view_pick_node/reset", 5);

        client_ = nh_.serviceClient<simsensorplugin::getPointCloud>("/getPointCloudService");

        //Load start pose
        ros::NodeHandle n("~");
        n.param<double>("start_x", start[0], 0.7071);
        n.param<double>("start_y", start[1], 0);
        n.param<double>("start_z", start[2], 0.7071);
        n.param<bool>("delay_global_registration", delay_registration, 0);
        n.param<int>("mode", mode, 0);
        std::cout << "view picker is set in mode: " << mode << std::endl;
        as_.start();
    }

    ~ViewPickAction(void)
    {
    }

    void executeCB(const view_pick::nbv_plannerGoalConstPtr &goal)
    {
        ros::Rate r(5); //rate of the action servor
        //setup start state
        bool success = true;
        done = false;
        done_surface = false;
        pause = false;
        //sync states
        new_cloud = false;
        new_pose = false;
        data_updated = true;        
        first_iteration = true;
        //on start secure the list are empty
        planetarian_list.clear();
        surfacebased_list.clear();

        std::cout << "goal given: " << goal->order << std::endl;
        ROS_INFO("Server was called");
        //Reset the system
        if(goal->order == -1)
        {
            iteration_counter = 0;
            success = false;
            reset_message(true);
            ros::spinOnce();
            result_.end_state = 0;
            as_.setSucceeded(result_);
        }
        else
        {
            while(pick_logic())
            {
                // check that preempt has not been requested by the client
                //std::cout << goal->order << std::endl;
                if (as_.isPreemptRequested() || !ros::ok()) //Important check so the server can be stopped while acting
                {
                    ROS_INFO("%s: Preempted", action_name_.c_str());
                    // set the action state to preempted
                    as_.setPreempted();

                    iteration_counter = 0;
                    success = false;
                    //wait until data queues are cleared
                    while(data_updated == false)
                    {
                        //delete(don't send) data when the last message has arrived
                        if(new_pose == true && new_cloud == true)
                        {
                            data_updated = true;
                        }
                        ros::spinOnce();
                        r.sleep();
                    }
                    reset_message(true);
                    break;
                }

                send_feedback();
                ros::spinOnce();
                r.sleep();
            }
        }

        if(success)
        {
            //send last message if any
            while(new_pose == true)
            {
                send_feedback();
                ros::spinOnce();
                r.sleep();
            }
            if(delay_registration)
            {
                reset_message(false);
            }

            //result_.sequence = feedback_.sequence;
            ROS_INFO("%s: Succeeded", action_name_.c_str());
            // set the action state to succeeded
            result_.end_state = 1;
            as_.setSucceeded(result_); //result given to the done callback in the client
        }

        ROS_INFO("%s: Out of callback loop", action_name_.c_str());

    }

    /*
     * return true while there is more views to pick
     */
    bool pick_logic()
    {
        if(pause == false)
        {
            //first view posistion is selected from start parameters
            if(first_iteration)
            {
                first_iteration = false;
                simsensorplugin::getPointCloud::Request  req;
                simsensorplugin::getPointCloud::Response res;

                Eigen::Vector3d translation = Eigen::Vector3d(start[0], start[1], start[2]);
                Eigen::Quaterniond rotation = Quaternion::ToOrigin(translation);

                req.transf.translation.x = translation(0);
                req.transf.translation.y = translation(1);
                req.transf.translation.z = translation(2);
                req.transf.rotation.w = rotation.w();
                req.transf.rotation.x = rotation.x();
                req.transf.rotation.y = rotation.y();
                req.transf.rotation.z = rotation.z();
                new_pose = true;

                client_.call(req, res);
                iteration_counter++;
                data_updated = false;
            }
            else if(mode == 0 && planetarian_list.empty() == false && new_pose == false)
            {
                std::cout << "calling service" << std::endl;

                simsensorplugin::getPointCloud::Request  req;
                simsensorplugin::getPointCloud::Response res;

                req.transf = planetarian_list[0].transform;
                planetarian_list.clear();
                //save it for the gui
                pose = req.transf;
                //feedback_.transform = req.transf;
                new_pose = true;

                client_.call(req, res);

                iteration_counter++;
                data_updated = false;

            }
            else if(mode == 1 && surfacebased_list.empty() == false && new_pose == false)
            {
                std::cout << "calling service" << std::endl;

                simsensorplugin::getPointCloud::Request  req;
                simsensorplugin::getPointCloud::Response res;

                req.transf = surfacebased_list[0].transform;
                surfacebased_list.clear();
                //save it for the gui
                pose = req.transf;
                //feedback_.transform = req.transf;
                new_pose = true;

                client_.call(req, res);

                iteration_counter++;
                data_updated = false;

            }

            if(mode == 0 && done)
            {
                return false;
            }
            if(mode == 1 && done_surface)
            {
                return false;
            }
        }

        return true;
    }

    void reset_message(bool type)
    {
        std_msgs::Bool msg;
        msg.data = type;
        reset_signal_.publish(msg);
    }

    bool send_feedback()
    {
        bool ret = false;
        if(new_pose == true && new_cloud == true)
        {
            /*feedback_.progress = iteration_counter;
            feedback_.transform = pose;
            feedback_.pointcloud = cloud;
            feedback_.done = done;
            as_.publishFeedback(feedback_);*/
            //new queued feedback
            view_pick::feedback msg;
            msg.header.stamp = ros::Time::now();
            msg.progress = iteration_counter;
            msg.transform = pose;
            msg.pointcloud = cloud;
            msg.done = done;
            feedback_signal_.publish(msg);

            new_pose = false;
            new_cloud = false;
            ret = true;
            data_updated = true;
        }
        return ret;
    }

    void planetarian_read(const view_pick::poseConstPtr& msg)
    {
        std::cout << "got pose" << std::endl;
        planetarian_list.push_back(*msg);
    }

    void planetarian_done(const std_msgs::BoolConstPtr& msg)
    {
        done = msg->data;
    }

    void surfacebased_read(const view_pick::poseConstPtr& msg)
    {
        std::cout << "got pose" << std::endl;
        surfacebased_list.push_back(*msg);
    }

    void surfacebased_done(const std_msgs::BoolConstPtr& msg)
    {
        done_surface = msg->data;
    }

    void pointCloudUpdate(const simsensorplugin::pointCloudConstPtr& msg)
    {
        cloud = msg->pointcloud;
        pose = msg->transform;
        new_cloud = true;
    }

    void pauseCb(const std_msgs::BoolConstPtr& msg)
    {
        pause = msg->data;
    }

};


int main(int argc, char **argv)
{
    ros::init(argc, argv, "view_pick_node");
    ros::NodeHandle nodeHandler("view_pick");

    /*ros::Publisher done_message = nodeHandler.advertise<registration::finish_model>("finished", 1);

    simsensorplugin::getPointCloud::Request  req;
    simsensorplugin::getPointCloud::Response res;
    client.call(req, res);

    registration::finish_model msg;
    msg.header.stamp = ros::Time::now();
    msg.make_model.data = true;
    done_message.publish(msg);*/

    ViewPickAction server(ros::this_node::getName());
    ros::spin();

    return 0;
}
