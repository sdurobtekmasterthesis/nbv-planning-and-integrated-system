#ifndef VISULISATION_H
#define VISULISATION_H

#include "pcl/point_types.h"
#include "pcl/point_cloud.h"
#include "pcl/octree/octree.h"
#include <pcl/visualization/cloud_viewer.h>
#include <pcl/visualization/pcl_visualizer.h>

class Visulisation
{
public:
    static void showOctree(pcl::octree::OctreePointCloud<pcl::PointXYZ> octree)//, double voxelsize)
    {
        //visualizer to create boxes
        pcl::visualization::PCLVisualizer show("octree");

        pcl::octree::OctreePointCloudSearch<pcl::PointXYZ>::AlignedPointTVector centers;
        int length = octree.getOccupiedVoxelCenters(centers);

        for(int i=0; i<length; i++)
        {
            //std::cout << "center "<< i << ": " << centers[i] << std::endl;
            /*show.addCube(centers[i].x-voxelsize/2,
                         centers[i].x+voxelsize/2,
                         centers[i].y-voxelsize/2,
                         centers[i].y+voxelsize/2,
                         centers[i].z-voxelsize/2,
                         centers[i].z+voxelsize/2,
                         1, 1, 1, std::to_string(i));*/
        }

        show.spin();
        while (!show.wasStopped ())
        {
        }
    }

    static void showNormals(pcl::PointCloud<pcl::PointXYZ>::ConstPtr cloud, pcl::PointCloud<pcl::PointNormal>::ConstPtr normals)
    {
        boost::shared_ptr<pcl::visualization::PCLVisualizer> viewer (new pcl::visualization::PCLVisualizer ("3D Viewer"));
        viewer->setBackgroundColor (0, 0, 0);
        viewer->addPointCloud<pcl::PointXYZ> (cloud, "sample cloud");
        //viewer->setPointCloudRenderingProperties (pcl::visualization::PCL_VISUALIZER_POINT_SIZE, 3, "sample cloud");
        viewer->addPointCloudNormals<pcl::PointNormal> (normals, 10, 0.05, "normals");
        viewer->addCoordinateSystem (1.0);
        viewer->initCameraParameters ();

        viewer->spin();
        while (!viewer->wasStopped ())
        {
        }
    }

    static void showNormals(pcl::PointCloud<pcl::PointNormal>::ConstPtr normals)
    {
        boost::shared_ptr<pcl::visualization::PCLVisualizer> viewer (new pcl::visualization::PCLVisualizer ("3D Viewer"));
        viewer->setBackgroundColor (0, 0, 0);
        viewer->addPointCloudNormals<pcl::PointNormal> (normals, 10, 0.05, "normals");
        viewer->addCoordinateSystem (1.0);
        viewer->initCameraParameters ();

        viewer->spin();
        while (!viewer->wasStopped ())
        {
        }
    }
};

#endif // VISULISATION
