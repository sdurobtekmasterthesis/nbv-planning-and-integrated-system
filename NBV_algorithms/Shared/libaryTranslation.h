#ifndef LIBARY_H
#define LIBARY_H

#include "eigen3/Eigen/Core"
#include "eigen3/Eigen/Geometry"
#include "geometry_msgs/Transform.h"
#include "Quaternion.h"

class Libary
{
public:
	static geometry_msgs::Transform toROSFromEigen(Eigen::Matrix4d transform)
	{
	    geometry_msgs::Transform msg;

	    Eigen::Quaternion<double> rotation(transform.block<3,3>(0,0));
	    msg.rotation.x = rotation.x();
	    msg.rotation.y = rotation.y();
	    msg.rotation.z = rotation.z();
	    msg.rotation.w = rotation.w();

	    msg.translation.x = transform(0,3);
	    msg.translation.y = transform(1,3);
	    msg.translation.z = transform(2,3);

	    return msg;
	}

	static geometry_msgs::Transform toROSFromEigen(Eigen::Vector3d translation ,Eigen::Quaterniond rotation)
	{
	    geometry_msgs::Transform msg;

	    msg.rotation.x = rotation.x();
	    msg.rotation.y = rotation.y();
	    msg.rotation.z = rotation.z();
	    msg.rotation.w = rotation.w();

	    msg.translation.x = translation(0);
	    msg.translation.y = translation(1);
	    msg.translation.z = translation(2);

	    return msg;
	}

        static Eigen::Matrix4d toEigenFromROS(geometry_msgs::Transform transform)
	{
	    Eigen::Matrix4d eigenTransform(4,4);
	    eigenTransform.setZero();
	    eigenTransform(3,3) = 1.0;
            Eigen::Quaterniond rotation( transform.rotation.w, transform.rotation.x, transform.rotation.y, transform.rotation.z);
	    eigenTransform.block<3,3>(0,0) = rotation.toRotationMatrix();

	    eigenTransform(0,3) = transform.translation.x;
	    eigenTransform(1,3) = transform.translation.y;
	    eigenTransform(2,3) = transform.translation.z;

	    return eigenTransform;
	}

        static Eigen::Quaterniond toEigenQuaternionFromROS(geometry_msgs::Transform transform)
	{
	    Eigen::Quaterniond rotation;
	    rotation.w() = transform.rotation.w;
	    rotation.x() = transform.rotation.x;
	    rotation.y() = transform.rotation.y;
	    rotation.z() = transform.rotation.z;
            return rotation;
	}

        static Eigen::Vector3d toEigenVector3FromROS(geometry_msgs::Transform transform)
	{
	    Eigen::Vector3d translation;
            translation(0) = transform.translation.x;
            translation(1) = transform.translation.y;
            translation(2) = transform.translation.z;
	    return translation;
	}

};

#endif // LIBARY_H
