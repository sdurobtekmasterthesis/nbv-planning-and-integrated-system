// Includes

// ROS
#include "ros/ros.h"
#include "ur_controller/getConfiguration.h"
#include "ur_controller/setConfiguration.h"
#include "ur_controller/setTransformation.h"
#include "ur_controller/getQueueSize.h"
#include "ur_controller/stopRobot.h"
#include "std_msgs/String.h"

// UR
#include <rwhw/universalrobots/URCallBackInterface.hpp>
#include <rwhw/universalrobots/UniversalRobotsRTLogging.hpp>

// Other
#include <rw/math/Q.hpp>
#include <rw/math/Transform3D.hpp>
#include <rw/math/Vector3D.hpp>
#include <rw/math/RPY.hpp>
#include <iostream>
#include <string>

// Namespace
using namespace rwhw;
using namespace rw::math;
using namespace std;

// Defines
#define RADTODEGREE		(180.0/M_PI)

// Prototypes
bool setConfigurationCallback(ur_controller::setConfiguration::Request &req, ur_controller::setConfiguration::Response &res);
bool setTransformationCallback(ur_controller::setTransformation::Request &req, ur_controller::setTransformation::Response &res);
bool getConfigurationCallback(ur_controller::getConfiguration::Request &req, ur_controller::getConfiguration::Response &res);
bool getQueueSizeCallback(ur_controller::getQueueSize::Request &req, ur_controller::getQueueSize::Response &res);
bool stopRobotCallback(ur_controller::stopRobot::Request &req, ur_controller::stopRobot::Response &res);
void connectToRobot();

// Global var
URCallBackInterface* _ur;
UniversalRobotsRTLogging* _urrt;
bool isConnected = false;

// Main
int main(int argc, char **argv)
{
	// Init ros
	ros::init(argc, argv, "ur");

	// Create node handler
	ros::NodeHandle n;

	// Create service handlers
    ros::ServiceServer getConfigurationService = n.advertiseService("/UR5/GetConfiguration", getConfigurationCallback);
    ros::ServiceServer setConfigurationService = n.advertiseService("/UR5/SetConfiguration", setConfigurationCallback);
    //ros::ServiceServer setTransformationService = n.advertiseService("/UR5/SetTransformation", setTransformationCallback);
    ros::ServiceServer getQueueSizeService = n.advertiseService("/UR5/GetQueueSize", getQueueSizeCallback);
    ros::ServiceServer stopRobotService = n.advertiseService("/UR5/StopRobot", stopRobotCallback);

	// Inform user
	ROS_INFO("UR Controller node is running..");

    // If not connected to robot, connect
    if(!isConnected)
        connectToRobot();

	// ros::spin() will enter a loop, pumping callbacks.
	ros::spin();

	// Return
  	return 0;
}

bool getQueueSizeCallback(ur_controller::getQueueSize::Request &req, ur_controller::getQueueSize::Response &res)
{
    // Get queue size
    res.queueSize = _ur->getQueueSize();

    // Return
    return true;
}

bool stopRobotCallback(ur_controller::stopRobot::Request &req, ur_controller::stopRobot::Response &res)
{
    // Stop
    _ur->stopRobot();

    // Return
    return true;
}

bool setConfigurationCallback(ur_controller::setConfiguration::Request &req, ur_controller::setConfiguration::Response &res)
{
	// If not connected to robot, connect
	if(!isConnected)
		connectToRobot();

	// Get joint values
	Q q(6, req.q[0], req.q[1], req.q[2], req.q[3], req.q[4], req.q[5]);

	//cout << q << endl;

	// Move robot
    if(req.speed > 0.0)
	{
        _ur->moveQ(q, req.speed, STANDARD_ACC_Q, req.blend);
		//cout << "moveQ(q, speed)" << endl;
	}
	else
	{
        _ur->moveQ(q, STANDARD_SPEED_Q, STANDARD_ACC_Q, req.blend);
		//cout << "moveQ(q)" << endl;
	}
	
	// Return
	return true;
}

bool getConfigurationCallback(ur_controller::getConfiguration::Request &req, ur_controller::getConfiguration::Response &res)
{
	// If not connected to robot, connect
	if(!isConnected)
		connectToRobot();

    // Get Q
    //_ur->getConfiguration();
	Q q = _ur->getConfiguration();

	//cout << q << endl;

	// Put into res array
    for(unsigned int i=0; i<q.size(); i++)
		res.q[i] = q[i];

	// Return
	return true;
}

//bool setTransformationCallback(ur_controller::setTransformation::Request &req, ur_controller::setTransformation::Response &res)
//{
//    // If not connected to robot, connect
//    if(!isConnected)
//        connectToRobot();

//    // Get joint values
//    Transform3D<> T(Vector3D<>(req.transformation[0],req.transformation[1],req.transformation[2]), RPY<>(req.transformation[3],req.transformation[4],req.transformation[5]));

//    //cout << T << endl;

//    // Move robot
//    if(req.speed>0.0)
//    {
//        _ur->moveT(T, req.speed);
//        //cout << "moveT(T, speed)" << endl;
//    }
//    else
//    {
//        _ur->moveT(T);
//        //cout << "moveT(T)" << endl;
//    }

//    // Return
//    return true;
//}

void connectToRobot()
{
	// Connect to ur robot
    delete _urrt;
    delete _ur;


    _ur = new URCallBackInterface();
	_urrt = new UniversalRobotsRTLogging();
    std::string ip("192.168.100.4");
	_urrt->connect(ip, 30003);

	isConnected = _ur->connect(ip, 30002);
	if(!isConnected)
	{
		ROS_ERROR("Couldn't connect to UR robot!");
		return;
	}
	_ur->startInterface(33334);
	_urrt->start();
	ROS_INFO("Successfully connected to UR robot!");
}
