#ifndef ROSSERVER_HPP
#define ROSSERVER_HPP

#include <iostream>
#include <fstream>
#include <sstream>
#include <mutex>
#include <string>
#include <vector>

#include <pcl/io/io.h>
#include <pcl/io/pcd_io.h>
#include <pcl/point_types.h>
#include <pcl/point_cloud.h>
// Need to include the pcl ros utilities
#include <pcl/visualization/cloud_viewer.h>
#include <pcl/visualization/pcl_visualizer.h>

// ROS
#include "ros/ros.h"
#include <ros/callback_queue.h>
#include <actionlib/client/simple_action_client.h>
#include "geometry_msgs/Transform.h"
#include <sensor_msgs/PointCloud2.h>
#include "view_pick/nbv_plannerAction.h"
#include "view_pick/feedback.h"

// Need to include the pcl ros utilities
#include <pcl_ros/point_cloud.h>

#include <QObject>
#include <QThread>
#include <QMetaType>
#include <QTimer>

#include <boost/filesystem.hpp>
#include "boost/date_time/posix_time/posix_time.hpp"
typedef boost::posix_time::ptime Time;
typedef boost::posix_time::time_duration TimeDuration;

class RosServer : public QObject
{
    Q_OBJECT
public:
    RosServer();

    // Signal the (transform) GUI to get at point cloude
    pcl::PointCloud<pcl::PointXYZRGB>::Ptr _pointCloud;

signals:
    // Get the point cloude from RWS and put it on ROS
    // Signal the GUI
    void updatePointCloud();
    void updateIteration(double value);
    void updatePose (geometry_msgs::Transform pose);
    void done(int state);


public slots:
    // RWS requests information
    // The info is fetched from ROS to the GUI
    void send_goal();
    void send_reset();
    void run();
    void spin_timer();

private:

    //Timer
    QTimer* ros_refresh_timer;

    enum class GUI_State{STARTUP, RUN_1, RUN_2, PAUSE_1, PAUSE_2, MIDWAY, FINISHED};

    //state
    GUI_State state;

    // Ros
    ros::NodeHandle *_nodeHandleClient;
    ros::Publisher *_pause_publish;
    ros::Publisher *_global_registration_publish;
    ros::Publisher *_data_string_publish;
    ros::Subscriber *_feedback_subscriber;
    ros::Subscriber *_global_registration_subscriber;


    // ROS client
    actionlib::SimpleActionClient<view_pick::nbv_plannerAction> *action_client;

    std::string fix_time(int time);

    void doneCb(const actionlib::SimpleClientGoalState& state,
                const view_pick::nbv_plannerResultConstPtr& result);

    void activeCb();

    void setFilename(std::string file);
    std::string file;
    std::ofstream data_file;
    Time t1,t2;

    void feedbackCb(const view_pick::feedbackConstPtr result);
    void registrationCb(const geometry_msgs::TransformConstPtr result);
    //void feedbackCb(const view_pick::nbv_plannerFeedbackConstPtr& result);


};

#endif // ROSSERVER_HPP
