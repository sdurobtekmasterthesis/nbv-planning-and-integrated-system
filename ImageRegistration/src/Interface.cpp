#include <iostream>
#include <fstream>
#include "boost/date_time/posix_time/posix_time.hpp"

typedef boost::posix_time::ptime Time;
typedef boost::posix_time::time_duration TimeDuration;

#include "Registration.h"
#include "ros/ros.h"

//message
#include "simsensorplugin/pointCloud.h"
#include <sensor_msgs/PointCloud2.h>
#include "geometry_msgs/Transform.h"
#include <std_msgs/Bool.h>
#include <std_msgs/String.h>

#include "../../NBV_algorithms/Shared/libaryTranslation.h"
#include "../../NBV_algorithms/Shared/Quaternion.h"

//viewer stuff
#include <pcl_conversions/pcl_conversions.h>
#include <pcl/visualization/cloud_viewer.h>
#include <pcl/visualization/pcl_visualizer.h>
ros::Publisher publishPointCloud;
ros::Publisher publishPointCloudPart;
ros::Publisher publishGlobalTransform;
ros::Publisher publishGlobalTransformDelay;
Registration *registration;
Eigen::Matrix4d global_registration;
bool turned;

//parameters
bool log_data;
bool delay_registration;
double ground_filter;
double plane_filter_max_dist;
int filter_type;
std::string filename;

//dataloging function
void datalog(int points_model, int points, long filter_time, long registration_time)
{
    static bool started = false;
    //static std::ofstream file("/home/morten/registration_times.txt");
    static std::ofstream file("registration_times.txt");

    if(started == false)
    {
        std::cout << "logging data: registration" << std::endl;
        file << "model_points, source_points, filter_time(s), registration_time(s)" << std::endl;
        started = true;
    }

    file << points_model << ", " << points << ", "<< filter_time/1000.0 << ", "<< registration_time/1000.0 << std::endl;
}

void resetCallback(const std_msgs::BoolConstPtr reset)
{
    if(reset->data)
    {
        std::cout << "true reset" << std::endl;
        delete registration;
        registration = new Registration(ground_filter, plane_filter_max_dist, log_data);
        //registration = new Registration(ground_filter, plane_filter_max_dist, log_data, filename);
        delay_registration = false;
        turned = false;
        global_registration.setIdentity();
    }
    //false reset indicates to keep cloud for delayed registration
    else
    {
        std::cout << "delay reset" << std::endl;

        //first done swap cloud
        if(!delay_registration)
        {
            delay_registration = true;
            registration->model_cloud_2 = pcl::PointCloud<pcl::PointXYZRGB>::Ptr(new pcl::PointCloud<pcl::PointXYZRGB>(*(registration->model_cloud)));
            registration->model_cloud->clear();
            registration->view_count = 0;
        }
        //next time do the global registration
        else
        {
            global_registration = registration->findGlobalTransform(*registration->model_cloud_2);
            pcl::PointCloud<pcl::PointXYZRGB> finalCloud;
            pcl::transformPointCloud(*registration->model_cloud_2, finalCloud, global_registration );
            global_registration = global_registration * registration->addPointCloud(finalCloud, 0.90);

            std::cout << "Show the final cloud" << std::endl;

            geometry_msgs::Transform msg = Libary::toROSFromEigen(global_registration);
            publishGlobalTransformDelay.publish(msg);

            /*boost::shared_ptr<pcl::visualization::PCLVisualizer> viewer2 (new pcl::visualization::PCLVisualizer ("3D mesh Viewer"));
            //viewer2->addPointCloud(finalCloud.makeShared());
            viewer2->addPointCloud(registration->model_cloud);
            viewer2->addCoordinateSystem (0.3);
            viewer2->setBackgroundColor(255,255,255);
            while (!viewer2->wasStopped() && ros::ok() )
            {
                viewer2->spinOnce (100);
                boost::this_thread::sleep (boost::posix_time::microseconds (100000));
            }*/
        }
    }
}

void turnCallback(const std_msgs::BoolConstPtr turn)
{
    turned = turn->data;
}

void filenameCallback(const std_msgs::StringConstPtr file)
{
    filename = file->data;
    registration->set_filename(file->data);
}

void pointCloudCallback(const simsensorplugin::pointCloudPtr pointCloud)
{
    ROS_INFO_STREAM("registration callback");
    Time t1(boost::posix_time::microsec_clock::local_time());

    pcl::PointCloud<pcl::PointXYZRGB> cloud;
    pcl::fromROSMsg(pointCloud->pointcloud, cloud);
    std::cout << "start cloud size: " << cloud.size() << std::endl;


    //put it to origin
    Eigen::Matrix4d transform = Libary::toEigenFromROS(pointCloud->transform);
    //Take rotation inverse because view is seen from target
    Eigen::Matrix3d change = transform.block<3,3>(0,0);
    transform.block<3,3>(0,0) = change.transpose();

    //aplly the transform
    pcl::PointCloud<pcl::PointXYZRGB> originCloud;
    registration->transformOrigin(cloud, originCloud, transform );

    //Filter unimportant
    pcl::PointCloud<pcl::PointXYZRGB> filteredCloud;

    switch (filter_type) {
    case 0:
          registration->filterGround(originCloud, filteredCloud);
          break;
    case 1:
        registration->filterGroundPlane(originCloud, filteredCloud);
        break;
    default:
        break;
    }
    std::cout << "cloud filtered size: " << filteredCloud.size() << std::endl;

    //Filter unimportant
    pcl::PointCloud<pcl::PointXYZRGB> statisticalCloud;
    registration->filterStatisticalDistance(filteredCloud, statisticalCloud, 15);

    if(turned && delay_registration == false)
    {
        global_registration = registration->findGlobalTransform(statisticalCloud);
        //registration->symmetryCheck(filteredCloud, global_registration);
        //consider adding local to the final global
        turned = false;

        geometry_msgs::Transform msg = Libary::toROSFromEigen(global_registration);
        publishGlobalTransform.publish(msg);
    }
    //apply global transformation(could be merged with other)
    pcl::PointCloud<pcl::PointXYZRGB> turnedCloud;
    registration->transformOrigin(statisticalCloud, turnedCloud, global_registration );


    /*boost::shared_ptr<pcl::visualization::PCLVisualizer> viewer (new pcl::visualization::PCLVisualizer ("3D mesh Viewer"));
    viewer->addPointCloud(filteredCloud.makeShared());
    viewer->addCoordinateSystem (0.3);
    viewer->setBackgroundColor(255,255,255);
    while (!viewer->wasStopped() && ros::ok() )
	{
        viewer->spinOnce (100);
	    boost::this_thread::sleep (boost::posix_time::microseconds (100000));
    }*/

    //This also aligns the input cloud(turnedCloud)
    Eigen::Matrix4d final_transform = global_registration * transform * registration->addPointCloud(turnedCloud);
    //Eigen::Matrix4d final_transform = global_registration * transform;// * registration->addPointCloud(turnedCloud);

    ROS_INFO_STREAM(" Final transform: \n" << final_transform << std::endl);

    simsensorplugin::pointCloud msg;
    msg.header.stamp = ros::Time::now();
    //the transformation
    msg.transform = Libary::toROSFromEigen(final_transform);

    //the point cloud
    //pcl::toROSMsg((*registration->model_cloud), msg.pointcloud );
    //publishPointCloud.publish(msg);

    //publish the splitted cloud
    //the point cloud
    pcl::toROSMsg((turnedCloud), msg.pointcloud );
    publishPointCloudPart.publish(msg);
}

int main(int argc, char **argv)
{
    ros::init(argc, argv, "Registration_node");
    ros::NodeHandle nodeHandler("registration");
    //local handle for parameters
    ros::NodeHandle n("~");

    //load parameters
    n.param<bool>("log_data", log_data, false);
    n.param<double>("ground_filter", ground_filter, 0.02);
    n.param<double>("plane_filter_max_dist", plane_filter_max_dist, 0.01);
    n.param<int>("filter_type", filter_type, 1);
    filename = "data_default";

    registration = new Registration(ground_filter, plane_filter_max_dist, log_data);
    delay_registration = false;

    global_registration.setIdentity();

    //connect with the topic
    ros::Subscriber sub = nodeHandler.subscribe("/PointCloudMsg", 1, pointCloudCallback);
    ros::Subscriber sub_reset = nodeHandler.subscribe("/view_pick_node/reset", 4, resetCallback);
    ros::Subscriber sub_file = nodeHandler.subscribe("/GUI/filename", 4, filenameCallback);
    ros::Subscriber sub_global = nodeHandler.subscribe("/GUI/global_registration", 4, turnCallback);

    //publishPointCloud = nodeHandler.advertise<simsensorplugin::pointCloud>("PointCloudMsg", 4);
    publishPointCloudPart = nodeHandler.advertise<simsensorplugin::pointCloud>("PointCloudMsg_Part", 4);
    publishGlobalTransform = nodeHandler.advertise<geometry_msgs::Transform>("Global_Transform", 4);
    publishGlobalTransformDelay = nodeHandler.advertise<geometry_msgs::Transform>("Global_Transform_Delay", 4);

    ros::spin();

    return 0;
}
