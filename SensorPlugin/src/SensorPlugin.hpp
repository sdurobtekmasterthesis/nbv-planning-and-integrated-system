#ifndef SENSORPLUGIN_HPP
#define SENSORPLUGIN_HPP

#include "ui_SensorPlugin.h"

#include "rw/rw.hpp"
#include <rw/loaders/model3d/LoaderAC3D.hpp>
#include <rws/RobWorkStudioPlugin.hpp>
#include <rw/models/WorkCell.hpp>
#include <rw/graphics/SceneViewer.hpp>
#include <rw/kinematics/State.hpp>
#include <rw/kinematics/Frame.hpp>
#include <rw/math/Transform3D.hpp>
//#include <rw/sensor/Image25D.hpp>

#include <rws/RobWorkStudioPlugin.hpp>

#include <rw/models/WorkCell.hpp>
#include <rw/kinematics/State.hpp>
#include <rw/common/Message.hpp>
#include <rw/trajectory/Path.hpp>

#include <rw/sensor/Scanner25D.hpp>
#include <rw/sensor/Scanner2D.hpp>
#include <rw/sensor/Scanner1D.hpp>
#include <rw/sensor/Camera.hpp>

#include <rwlibs/simulation/SimulatedSensor.hpp>

#include <rwlibs/simulation/GLFrameGrabber.hpp>
#include <rwlibs/simulation/GLFrameGrabber25D.hpp>

#include <iostream>
#include <vector>
#include <string>

#include <boost/tuple/tuple.hpp>

// Need to include the pcl ros utilities
#include <pcl/visualization/cloud_viewer.h>
#include <pcl/visualization/pcl_visualizer.h>
#include <pcl/io/io.h>
#include <pcl/io/pcd_io.h>
#include <pcl/point_types.h>
#include <pcl/point_cloud.h>

#include "rosserver.hpp"

#include <QObject>
#include <QString>
#include <QComboBox>
#include <QWidget>
#include <QMetaType>
#include <QThread>

#include <QTreeView>
#include <QStandardItemModel>
#include <QItemSelectionModel>

// Point cloud types
typedef pcl::PointXYZRGB PointT;
typedef pcl::PointCloud<PointT> CloudT;
typedef std::vector<CloudT,Eigen::aligned_allocator<CloudT> > CloudVecT;


class SensorPlugin: public rws::RobWorkStudioPlugin, private Ui::SensorPlugin
{

Q_OBJECT
Q_INTERFACES( rws::RobWorkStudioPlugin )
public:
    SensorPlugin();
	virtual ~SensorPlugin();

    virtual void open(rw::models::WorkCell* workcell);

    virtual void close();

    virtual void initialize();

    typedef boost::tuple<double, double, double, rw::math::Transform3D<> > CameraParameters;

public slots:
    void getPointCloudROS(rw::math::Transform3D<double> trans);
    void setObjectPoseROS(rw::math::Transform3D<double> trans);

private slots:
    //TODO: add slot for chancing the pose and getting point cloud

    void btnPressed();
    void noiseChecked();
    void btnChangeRotPressed();

private:
    void stateChangedListener(const rw::kinematics::State& state);
    void grabImage(rw::kinematics::Frame* frame);
    void applyNoise(rw::geometry::PointCloud& scan);
    void changeObejctTransform(rw::math::Transform3D<double> trans);

    rosServer* _rosServer;
    QThread* _threadRosServer;

    rw::models::WorkCell* _wc;
    rw::graphics::SceneViewer::Ptr _gldrawer;
    rw::kinematics::State _state;

    unsigned int _view_frame_num;
    bool _noise_model;
};

#endif /*SENSORPLUGIN_HPP*/
