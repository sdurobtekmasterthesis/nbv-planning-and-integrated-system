#include "nbv_surfacebased.h"

//viewer stuff
#include <pcl_conversions/pcl_conversions.h>
#include <pcl/visualization/cloud_viewer.h>
#include <pcl/visualization/pcl_visualizer.h>
#include <pcl/filters/voxel_grid.h>
#include <pcl/common/pca.h>

#include "../../Shared/Quaternion.h"
#include "../../Shared/Visulisation.h"
#include <stdlib.h>
#include <pcl/search/kdtree.h>
#include <algorithm>
#include <queue>

#include <boost/filesystem.hpp>
#include "boost/date_time/posix_time/posix_time.hpp"
typedef boost::posix_time::ptime Time;
typedef boost::posix_time::time_duration TimeDuration;

#define CLUSTER_ITERATION_LIMIT 10
#define CLUSTER_MIN_SIZE 0.05
#define CLUSTER_ACCEPT_LIMIT 0.90
#define CLUSTER_RADIUS_INCREASE 1.5
#define FILTER_VOXELSIZE 0.002

NBV_surfacebased::NBV_surfacebased(bool log_data, int max_iterations, double object_distance, double filter_distance, std::string filename):
pointcloud(new pcl::PointCloud<pcl::PointXYZ>()),
pointnormals(new pcl::PointCloud<pcl::Normal>())
{
    save_counter = 0;
    iteration_counter = 0;
    iteration_max = max_iterations;
    this->object_distance = object_distance;
    this->filter_distance = filter_distance;
    this->log_data = log_data;

    global_translation.setZero();
    global_rotation.setIdentity();
    global_transformation.setIdentity();

    set_filename(filename);
}

void NBV_surfacebased::set_filename(std::string filename)
{
    if(data_file.is_open())
    {
        data_file.close();
    }

    if(log_data)
    {
        this->filename = filename;
        boost::filesystem::path dir("../../../Data/" + filename + "/Surface/clusters");
        if(!(boost::filesystem::exists(dir)))
        {

            if(!boost::filesystem::create_directories(dir))
            {
                ROS_ERROR("Directory could not be created");
            }
        }

        std::cout << "writing file" << std::endl;
        data_file.open("../../../Data/" + filename + "/Surface/data.txt", std::ofstream::app);
        data_file << "Time edge finding, Amount of views, Time clustering" << std::endl;

        std::ofstream settings_file("../../../Data/" + filename + "/Surface/settings.txt");
        settings_file << "Filter distance: " << filter_distance << std::endl;
        settings_file << "Cluster minimum size: " << CLUSTER_MIN_SIZE << std::endl;
        settings_file << "Required cluster: " << CLUSTER_ACCEPT_LIMIT << std::endl;
        settings_file << "Down sample voxelsize: " << FILTER_VOXELSIZE << std::endl;
        settings_file.close();
    }
}

std::vector<transform> NBV_surfacebased::next(const pcl::PointCloud<pcl::PointXYZ> &pointCloud, bool &done)
{
    std::vector<transform> poses;
    if(!pointCloud.empty())
    {
        //add to the full cloud
        (*pointcloud) += pointCloud;
        Time t1,t2;
        if(log_data)
        {
            t1 = boost::posix_time::microsec_clock::local_time();
        }
        //make a filtered cloud
        pcl::PointCloud<pcl::PointXYZ>::Ptr cloud_filtered = cloudDownSampling(pointcloud);
        //generate boundaries based on the current cloud
        pcl::PointCloud<pcl::PointXYZ>::Ptr boundaries = boundaryEstimation(cloud_filtered);
        if(log_data)
        {
            t2 = boost::posix_time::microsec_clock::local_time();
            TimeDuration dt = t2- t1;
            data_file << (long)dt.total_milliseconds()/1000.0 << ", ";
        }
        //save(boundaries);
        if(log_data)
        {
            t1 = boost::posix_time::microsec_clock::local_time();
        }
        //generates a view through clustering
        poses = viewGeneration(boundaries);
        if(log_data)
        {
            t2 = boost::posix_time::microsec_clock::local_time();
            TimeDuration dt = t2- t1;
            data_file << (long)dt.total_milliseconds()/1000.0 << ", " << std::endl;
        }

    }

    iteration_counter++;
    if(iteration_counter >= iteration_max)
    {
        poses.clear();
        done = true;
    }

    return poses;
}

pcl::PointCloud<pcl::PointXYZ>::Ptr NBV_surfacebased::cloudDownSampling(pcl::PointCloud<pcl::PointXYZ>::Ptr cloud)
{
    pcl::PointCloud<pcl::PointXYZ>::Ptr cloud_filtered(new pcl::PointCloud<pcl::PointXYZ>);
    pcl::VoxelGrid<pcl::PointXYZ> sample;
    sample.setInputCloud (cloud);
    sample.setLeafSize (FILTER_VOXELSIZE, FILTER_VOXELSIZE, FILTER_VOXELSIZE);
    sample.filter (*cloud_filtered);
    return cloud_filtered;
}


pcl::PointCloud<pcl::PointXYZ>::Ptr NBV_surfacebased::boundaryEstimation(pcl::PointCloud<pcl::PointXYZ>::Ptr cloud)
{
    pcl::NormalEstimationOMP<pcl::PointXYZ, pcl::Normal> ne;
    //pcl::NormalEstimation<pcl::PointXYZ, pcl::Normal> ne;

    pcl::search::KdTree<pcl::PointXYZ>::Ptr tree1 (new pcl::search::KdTree<pcl::PointXYZ>);
    tree1->setInputCloud (cloud);
    ne.setInputCloud (cloud);
    ne.setSearchMethod (tree1);
    ne.setKSearch (40);
    pcl::PointCloud<pcl::Normal>::Ptr normals (new pcl::PointCloud<pcl::Normal>);
    ne.compute (*normals);

    pcl::PointCloud<pcl::PointNormal> cloud_with_normals;
    pcl::concatenateFields (*cloud, *normals, cloud_with_normals);

    pcl::PointCloud<pcl::Boundary> boundaries;
    pcl::BoundaryEstimation<pcl::PointXYZ, pcl::Normal, pcl::Boundary> est;
    est.setInputCloud (cloud);
    est.setInputNormals(normals);
    est.setRadiusSearch(0.02);   // 2cm radius
    est.setSearchMethod(pcl::search::KdTree<pcl::PointXYZ>::Ptr (new pcl::search::KdTree<pcl::PointXYZ>));

    pcl::PointCloud<pcl::PointXYZ>::Ptr output_cloud (new pcl::PointCloud<pcl::PointXYZ>);
    pcl::PointCloud<pcl::PointNormal>::Ptr output_cloud2 (new pcl::PointCloud<pcl::PointNormal>);

    auto indices_points = cloud->points;

    est.compute(boundaries);

    for(unsigned int i=0; i < boundaries.size(); i++)
    {
        if( boundaries[i].boundary_point == 1)
        {
            pcl::PointXYZ temp_point = indices_points.at(i);
            pcl::PointNormal temp_point2 = cloud_with_normals.at(i);
            output_cloud->push_back(temp_point);
            output_cloud2->push_back(temp_point2);
        }
    }

    //Visulisation::showNormals(output_cloud, output_cloud2);

    return output_cloud;
}

std::vector<transform> NBV_surfacebased::viewGeneration(pcl::PointCloud<pcl::PointXYZ>::Ptr cloud)
{
    std::vector<pcl::PointCloud<pcl::PointXYZ>::Ptr> cluster_list = cluster(cloud);
    if(log_data)
    {
        data_file << cluster_list.size() << ", ";
    }

    //This generates views for all cluster when needed in hybrid
    current_options.clear();
    for(unsigned int i=0; i<cluster_list.size(); i++)
    {
        current_options.push_back( viewFromPCA(cluster_list[i]) );

        std::cout << "cluster: " << i << std::endl <<
        current_options[i].translation << std::endl <<
        current_options[i].rotation.toRotationMatrix() << std::endl;
    }

    return current_options;
}

/*
 * This will not fix if its in collision only filter close views from the list
 * */
transform NBV_surfacebased::selectView(std::vector<transform> current_options, bool &done)
{
    bool new_view = false;
    unsigned int view_index = 0;
    transform current_view;
    while( !new_view && view_index < current_options.size() )
    {
        current_view = current_options[view_index];
        new_view = viewFilter(current_view);
        view_index++;
    }

    if(new_view == false)
    {
        done = true;
    }

    picked_views.push_back(current_view);

    std::cout << "position of view: \n" << current_view.translation << std::endl;

    return current_view;
}

/*
 * returns false if too simular to another view
 */
bool NBV_surfacebased::viewFilter(transform view)
{
    bool new_view=true;
    //check if too close to each other
    for(unsigned int i=0; i<picked_views.size(); i++)
    {
        cout << "distance from view " << i << ": " << (view.translation-picked_views[i].translation).squaredNorm() << std::endl;
        if( (view.translation-picked_views[i].translation).squaredNorm() < filter_distance )
        {
            new_view = false;
            break;
        }
    }
    //check if view looks a the same
    return new_view;
}


std::vector<pcl::PointCloud<pcl::PointXYZ>::Ptr> NBV_surfacebased::cluster(pcl::PointCloud<pcl::PointXYZ>::Ptr cloud)
{
    std::vector<int> options;
    float radius = estimateSearchRadius(cloud, options);
    std::cout << "radius found: " << radius << std::endl;

    //build and set a structure for fast searching of match
    found = new bool[cloud->size()];
    for(unsigned int i=0; i< cloud->size(); i++)
    {
        found[i] = false;
    }

    std::vector<pcl::PointCloud<pcl::PointXYZ>::Ptr> cluster_list;
    int total_found = 0;
    int iteration = 0;
    //search until 90% of the points is clustered
    do
    {
        for(unsigned int i=0; i<cloud->size(); i++)
        {
            std::vector<int> cluster_index = estimateCluster(cloud, i, radius);
            //check the cluster have at least 5%
            if(cluster_index.size() > cloud->size()*CLUSTER_MIN_SIZE && cluster_index.size() > 4)
            {
                total_found += cluster_index.size();

                pcl::PointCloud<pcl::PointXYZ>::Ptr cluster_cloud(new pcl::PointCloud<pcl::PointXYZ>() );
                pcl::copyPointCloud(*cloud, cluster_index, *cluster_cloud);
                cluster_list.push_back(cluster_cloud);
                //save them for testing
                save(cluster_cloud);
            }
            //if not put them back on not found list
            else
            {
                for(unsigned int j=0; j<cluster_index.size(); j++)
                {
                    found[cluster_index[j]] = false;
                }
            }
        }
        //increase radius for next search
        iteration++;
        radius *= CLUSTER_RADIUS_INCREASE;
        //std::cout << "radius increased: " << radius << std::endl;
    }while(total_found < cloud->size()*CLUSTER_ACCEPT_LIMIT && iteration < CLUSTER_ITERATION_LIMIT);

    save_counter = 0;
    delete found;
    return cluster_list;
}


/*
 * Returns a list of indexes that belongs to the estimated cluster in the cloud
 * */
std::vector<int> NBV_surfacebased::estimateCluster(pcl::PointCloud<pcl::PointXYZ>::Ptr cloud, int start_index, float radius)
{
    //setup search(consider building it into the class)
    pcl::search::KdTree<pcl::PointXYZ> tree;
    tree.setInputCloud(cloud);
    std::vector<int> cluster_list;
    //if already part of cluster exit
    if(found[start_index])
    {
        return cluster_list;
    }

    found[start_index] = true;
    cluster_list.push_back(start_index);
    std::queue<int> search_list;
    search_list.push(start_index);
    while(!search_list.empty())
    {
        std::vector<int> pointIdxRadiusSearch;
        std::vector<float> pointRadiusSquaredDistance;
        //search the nearest points
        tree.radiusSearch (search_list.front(), radius, pointIdxRadiusSearch, pointRadiusSquaredDistance);
        search_list.pop();
        for(unsigned int i=0; i<pointIdxRadiusSearch.size(); i++)
        {
            //if the index is found ignore it else add for search
            if(found[pointIdxRadiusSearch[i]] == false)
            {
                found[pointIdxRadiusSearch[i]] = true;
                search_list.push(pointIdxRadiusSearch[i]);
                cluster_list.push_back(pointIdxRadiusSearch[i]);
            }
        }
    }

    return cluster_list;
}

//used for sorting a data structure, used in estimateSearchRadius
struct ClusterOptions
{
    int index;
    float distance;
};

bool operator <(const ClusterOptions& x, const ClusterOptions& y) {
    return std::tie(x.distance) < std::tie(y.distance);
}

float NBV_surfacebased::estimateSearchRadius(pcl::PointCloud<pcl::PointXYZ>::Ptr cloud, std::vector<int> &cluster_options)
{
    cluster_options.clear();

    pcl::search::KdTree<pcl::PointXYZ> tree;
    tree.setInputCloud(cloud);
    std::vector<ClusterOptions> options;

    //take 100 random samples for optional clusters
    for(int i=0; i<100; i++)
    {
        ClusterOptions temp;
        std::vector<int> tempIdx;
        std::vector<float> tempDistance;
        temp.index = rand()%cloud->size();
        pcl::PointXYZ search_point = cloud->at(temp.index);
        //Find the nearest neighbour for each option
        tree.nearestKSearch(search_point, 2, tempIdx, tempDistance);
        temp.distance = tempDistance[1];
        options.push_back(temp);
    }

    //sort the best options
    std::sort (options.begin(), options.end());
    float sum=0;

    //use the 10 best to estimate a search radius
    /*for(int i=0; i<10 ; i++)
    {
        sum += std::sqrt(options[i].distance);
        std::cout << "distance" << i << ": " << options[i].distance << std::endl;
        cluster_options.push_back(options[i].index);
    }*/

    //use the 10 best to estimate a search radius
    for(int i=0; i<10 ; i++)
    {
        std::vector<int> tempIdx;
        std::vector<float> tempDistance;
        tree.nearestKSearch(cloud->at(options[i].index), 11, tempIdx, tempDistance);
        //use the 10 closest for a fair average
        for(int j=1; j<11; j++)
        {
            sum += std::sqrt(tempDistance[j]);
        }
    }

    //return sum/10*4; // four is just a tested scale factor
    return sum/100;
}

transform NBV_surfacebased::viewFromPCA(pcl::PointCloud<pcl::PointXYZ>::Ptr cluster)
{
    std::cout << "Cluster size: " <<cluster->size() << std::endl;

    transform ret;
    pcl::PCA<pcl::PointXYZ> pca;
    pca.setInputCloud(cluster);
    //use mean as a starting point for the view
    ret.translation = pca.getMean().head(3).cast<double>() - global_translation;

    Eigen::Matrix3d eigen_vectors = pca.getEigenVectors().cast<double>();
    Eigen::Vector3d z_axis = eigen_vectors.block<3,1>(0,2);
    //check that the direction is towards or away from the object

    Eigen::Vector3d start = ret.translation;

    if(z_axis.dot(ret.translation) < 0)
    {
        //std::cout << "case 1" << std::endl;
        z_axis = -z_axis;
    }
    else
    {
        //std::cout << "case 2" << std::endl;
    }

    ret.translation += z_axis*object_distance;
    //ret.rotation = Quaternion::ToOrigin(ret.translation, start);

    //adjust it back to real views
    if(!global_transformation.isIdentity())
    {

        Eigen::Vector4d temp;
        temp.block<3,1>(0,0) = ret.translation;
        temp[3] = 1;
        temp =  global_transformation * temp;
        ret.translation = temp.block<3,1>(0,0);

        temp.block<3,1>(0,0) = start;
        temp[3] = 1;
        temp =  global_transformation * temp;
        start = temp.block<3,1>(0,0);

        /*Eigen::Matrix4d transform_orig;
        transform_orig.setIdentity();
        transform_orig.block<3,3>(0,0) = ret.rotation.toRotationMatrix();
        transform_orig.block<3,1>(0,3) = ret.translation;
        //std::cout << "transformation before adjustment\n" << transform_orig << std::endl;

        transform_orig = global_transformation.inverse() * transform_orig;

        //std::cout << "transformation after adjustment\n" << transform_orig << std::endl;

        ret.rotation = Eigen::Quaterniond(transform_orig.block<3,3>(0,0));
        ret.translation = transform_orig.block<3,1>(0,3);*/
    }

    ret.rotation = Quaternion::ToOrigin(ret.translation, start);

    return ret;
}

void NBV_surfacebased::rotate_scene(transform object_movement)
{
    global_translation = object_movement.translation;
    global_rotation = object_movement.rotation.toRotationMatrix();
    global_transformation.block<3,3>(0,0) = global_rotation;
    global_transformation.block<3,1>(0,3) = global_translation;
    Eigen::Vector4d zero_point;
    zero_point.setZero();
    zero_point[3] = 1;
    zero_point =  global_transformation * zero_point;
    global_translation = zero_point.block<3,1>(0,0);

    //clear views, because they are not relevant after turn
    picked_views.empty();

    std::cout << "new 0 point after registration:\n" << global_translation << std::endl;
}

void NBV_surfacebased::save(pcl::PointCloud<pcl::PointXYZ>::Ptr cloud)
{
    //std::ofstream file(name + std::to_string(save_counter) + ".txt");
    std::ofstream file("../../../Data/" + filename + "/Surface/clusters/iteration" + std::to_string(iteration_counter) +  "_cluster" + std::to_string(save_counter) + ".txt");
    file << "x, y, z" << std::endl;
    save_counter++;
    for(unsigned int i=0; i<cloud->size(); i++)
    {
        file << cloud->at(i).x << ", " << cloud->at(i).y << ", " << cloud->at(i).z << std::endl;;
    }
    file.close();
}

