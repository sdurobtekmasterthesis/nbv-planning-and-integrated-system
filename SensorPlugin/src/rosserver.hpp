#ifndef ROSSERVER_HPP
#define ROSSERVER_HPP

#include <iostream>
#include <fstream>
#include <sstream>
#include <mutex>
#include <string>

// RobWork
#include <rw/rw.hpp>

#include <pcl/io/io.h>
#include <pcl/io/pcd_io.h>
#include <pcl/point_types.h>
#include <pcl/point_cloud.h>
// Need to include the pcl ros utilities
#include <pcl/visualization/cloud_viewer.h>
#include <pcl/visualization/pcl_visualizer.h>
#include <pathplanner/isPoseValid.h>

// ROS
#include "ros/ros.h"
#include <ros/callback_queue.h>
#include <simsensorplugin/getPointCloud.h>
#include <simsensorplugin/setObjectTransform.h>
#include <simsensorplugin/pointCloud.h>

// Need to include the pcl ros utilities
#include <pcl_ros/point_cloud.h>

#include <QObject>
#include <QThread>
#include <QMetaType>


class rosServer : public QObject
{
    Q_OBJECT
public:
    rosServer();

    std::mutex _pointCloudMutex;

    // Signal the (transform) GUI to get at point cloude
    pcl::PointCloud<pcl::PointXYZRGB>::Ptr _pointCloud;

signals:
    // Get the point cloude from RWS and put it on ROS
    // Signal the GUI
    void getPointCloudSignal(rw::math::Transform3D<double> trans);
    void changeObjectPoseSig(rw::math::Transform3D<double> trans);

public slots:
    // RWS requests information
    // The info is fetched from ROS to the GUI
    void getInfoSignal();
    void run();

private:

    // Ros CallBacks
    bool callbackGetPointCloud(simsensorplugin::getPointCloud::Request& req,
                                simsensorplugin::getPointCloud::Response& res);

    bool callbackSetObjectTransform(simsensorplugin::setObjectTransform::Request& req,
                                    simsensorplugin::setObjectTransform::Response& res);

    bool callbackIsPoseValid(pathplanner::isPoseValid::Request &req,
                             pathplanner::isPoseValid::Response &res);

    // Ros
    ros::NodeHandle *_nodeHandleClient;
    ros::NodeHandle *_nodeHandleServer;

    // ROS services
    ros::ServiceServer _serviceServersetObjectPose;
    ros::ServiceServer _serviceServergetPointCloud;
    ros::ServiceServer _serviceIsPoseValid;
    ros::ServiceClient _serviceClientgetInfoClient;

    //ROS message
    ros::Publisher _publishPointCloud;
};

#endif // ROSSERVER_HPP
