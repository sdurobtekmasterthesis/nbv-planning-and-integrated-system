#ifndef QUATERNION_H
#define QUATERNION_H

//#include "Eigen/Core"
#include "eigen3/Eigen/Core"
#include "eigen3/Eigen/Geometry"

class Quaternion
{
public:
//This could be implemented in smarter ways(graahm smith or make a orthogonal vector to the known z and take a cross product)
        static Eigen::Quaterniond ToOrigin(Eigen::Vector3d view, Eigen::Vector3d start = Eigen::Vector3d::Zero() )
	{
	    //Eigen::Vector3d start;
	    //start.setZero(3);
	    Eigen::Vector3d direction = start-view;
	    direction.normalize();
	    
	    //Find the shortest rotation assumeing the direction from start is directly upwards 
	    Eigen::Vector3d z_axis(0,0,1);
	    Eigen::Vector3d eaa_axis = direction.cross(z_axis);
	    //handle case with parallel axis
	    if(eaa_axis.squaredNorm() < 0.001)
	    {
		eaa_axis = Eigen::Vector3d(1,0,0);
	    }
	    else
	    {
	    	eaa_axis.normalize();
	    }
	    double eaa_rotation = acos(direction.dot(z_axis));

            Eigen::Quaterniond quat;
            quat = Eigen::AngleAxisd(eaa_rotation, eaa_axis); //seems wierd but: http://eigen.tuxfamily.org/dox/group__TutorialGeometry.html

            //Eigen::AngleAxisd convert(eaa_rotation, eaa_axis);
            //Eigen::Quaterniond quat(convert);

	    return quat;
	}
};

#endif // QUATERNION_H
