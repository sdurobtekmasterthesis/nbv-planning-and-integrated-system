#ifndef NBV_RANDOM_H
#define NBV_RANDOM_H

#include "pcl/point_cloud.h"
#include "pcl/point_types.h"
#include "Eigen/Core"

struct transform
{
    Eigen::Vector3d translation;
    Eigen::Quaterniond rotation;
};

class NBV_Random
{
    public:
        NBV_Random();
        transform next(pcl::PointCloud<pcl::PointXYZ> PointCloud, bool &done);
        void getInfo();

    private:
        int iteration_counter;
        int iteration_max;
};

#endif // RANDOM_H
