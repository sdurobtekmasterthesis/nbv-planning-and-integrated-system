#include "rosserver.hpp"

using namespace std;

rosServer::rosServer()
{
}

void  rosServer::run()
{
    // Set up ros
    // Setup ROS
    char** argv = NULL;
    int argc = 0;
    ros::init(argc, argv, "rosserverPointCloud");

    // ROS service server and client
    _nodeHandleServer = new ros::NodeHandle;
    //_nodeHandleClient = new ros::NodeHandle;

    // Publish and subscribe
    //_serviceClientgetInfoClient = _nodeHandleClient->serviceClient<simsensorplugin::getInfo>("getInfo");
    _serviceServersetObjectPose = _nodeHandleServer->advertiseService((string) "/setObjectPose", &rosServer::callbackSetObjectTransform, this);
    _serviceServergetPointCloud = _nodeHandleServer->advertiseService(( (string) "/getPointCloudService" ), &rosServer::callbackGetPointCloud, this);
    _serviceIsPoseValid = _nodeHandleServer->advertiseService( "/pathplanner/isPoseValid", &rosServer::callbackIsPoseValid, this);
    _publishPointCloud = _nodeHandleServer->advertise<simsensorplugin::pointCloud>( (string) "/PointCloudMsg", 1);

    //Set loop rate
    ros::Rate loop_rate(10);

    // While loop
    while (ros::ok())
    {
        ros::spinOnce();
        loop_rate.sleep();
    }
}

// Callbacks
bool rosServer::callbackGetPointCloud(simsensorplugin::getPointCloud::Request& req,
                            simsensorplugin::getPointCloud::Response& res)
{
    //ROS_INFO_STREAM("Callback getPointCloud called!");

    //Conver to Robwork format
    rw::math::Quaternion<double> quater(req.transf.rotation.x,req.transf.rotation.y, req.transf.rotation.z, req.transf.rotation.w);
    rw::math::Transform3D<double> trans(rw::math::Vector3D<double>(req.transf.translation.x,req.transf.translation.y, req.transf.translation.z), rw::math::inverse(quater.toRotation3D()));

    std::cout << " Rosserver" << std::endl;

    // Blocking call (see the connect method) waits to slot to finish
    emit getPointCloudSignal(trans);

    //publish the point cloud
    simsensorplugin::pointCloud msg;
    pcl::toROSMsg(*_pointCloud, msg.pointcloud );
    msg.header.stamp = ros::Time::now();
    msg.transform = req.transf;
    _publishPointCloud.publish(msg);

    //ROS_INFO_STREAM("Coping to ROS from PCL");
    //pcl::toROSMsg(*_pointCloud, res.pointcloud );

    ros::spinOnce();

    res.isPoseReached = true;

    return true;
}

bool rosServer::callbackIsPoseValid(pathplanner::isPoseValid::Request &req,
                                    pathplanner::isPoseValid::Response &res)
{
    res.isValid = true;
    return true;
}


bool rosServer::callbackSetObjectTransform(simsensorplugin::setObjectTransform::Request& req,
                                simsensorplugin::setObjectTransform::Response& res)
{
    std::cout << "Ros server called set object" << std::endl;

    //Conver to Robwork format
    rw::math::Quaternion<double> quater(req.transf.rotation.x,req.transf.rotation.y, req.transf.rotation.z, req.transf.rotation.w);
    rw::math::Transform3D<double> trans(rw::math::Vector3D<double>(req.transf.translation.x,req.transf.translation.y, req.transf.translation.z), rw::math::inverse(quater.toRotation3D()));

    //emit the signal
    emit changeObjectPoseSig(trans);

    ros::spinOnce();

    return true;
}

void rosServer::getInfoSignal()
{
    // QT Slot for getting information
    // Signal from the GUI to parse the information form ROS to GUI
    // Gets all information from the methods in ROS and returns it to the GUI

    ROS_INFO_STREAM("Get info slot called");
}
