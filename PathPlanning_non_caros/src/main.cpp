#include <string>
#include <exception>

#include <rw/loaders/WorkCellLoader.hpp>
#include <ros/ros.h>


#include "PathPlanning.hpp"

std::string worckcellpath;
std::string devicename;

int main(int argc, char *argv[])
{

try{
    ros::init(argc, argv, "pathplanner");
    ros::NodeHandle n("~");
    n.param<std::string>("workcell_path", worckcellpath, "");
    n.param<std::string>("device_name", devicename, "");


    PathPlanning pathplan(worckcellpath);

    pathplan.run();

    }
    catch(const rw::common::Exception &e)
    {
        std::cerr << "Error exception catched!!!: "<< std::endl << e << std::endl;

    }

  return 0;
}
