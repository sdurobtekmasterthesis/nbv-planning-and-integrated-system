#include "Registration.h"
#include "FeatureCloud.h"
#include "TemplateAlignment.h"

//viewer stuff
#include <pcl_conversions/pcl_conversions.h>
#include <pcl/visualization/cloud_viewer.h>
#include <pcl/visualization/pcl_visualizer.h>
#include <pcl/io/ply_io.h>

#include <iostream>
#include <pcl/io/pcd_io.h>
#include <pcl/point_types.h>
#include <pcl/ModelCoefficients.h>
#include <pcl/filters/extract_indices.h>
#include <pcl/sample_consensus/method_types.h>
#include <pcl/sample_consensus/model_types.h>
#include <pcl/segmentation/sac_segmentation.h>

#include <boost/filesystem.hpp>
#include "boost/date_time/posix_time/posix_time.hpp"
typedef boost::posix_time::ptime Time;
typedef boost::posix_time::time_duration TimeDuration;

Registration::Registration(double ground_filter, double plane_filter_max_dist, bool log_data, std::string filename):
    model_cloud(new pcl::PointCloud<pcl::PointXYZRGB>())
{
    view_count = 0;
    save_count = 0;
    this->ground_filter = ground_filter;
    this->plane_filter_max_dist = plane_filter_max_dist;
    //setup trimmed ICP
    trimmed_rejector = pcl::registration::CorrespondenceRejectorTrimmed::Ptr(new pcl::registration::CorrespondenceRejectorTrimmed);
    trimmed_rejector->setOverlapRatio(OVERLAP_RATIO);
    trimmed_icp.addCorrespondenceRejector(trimmed_rejector);

    trimmed_icp.setMaximumIterations (1000);
    // Set the transformation epsilon (criterion 2)
    trimmed_icp.setTransformationEpsilon (1e-8);
    // Set the euclidean distance difference epsilon (criterion 3)
    trimmed_icp.setEuclideanFitnessEpsilon (0.0001);


    // Set the maximum number of iterations (criterion 1)
    //trimmed_icp.setMaximumIterations (999999);
    // Set the transformation epsilon (criterion 2)
    //trimmed_icp.setTransformationEpsilon (1e-6);
    // Set the euclidean distance difference epsilon (criterion 3)
    //trimmed_icp.setEuclideanFitnessEpsilon (0.0001);

    this->log_data = log_data;
    set_filename(filename);
}

//returns transformation from the input cloud to the model cloud
Eigen::Matrix4d Registration::findGlobalTransform(pcl::PointCloud<pcl::PointXYZRGB> &cloud)
{
    Time t1,t2;
    if(log_data)
    {
        t1 = boost::posix_time::microsec_clock::local_time();
    }

    std::cout << "starting find global transform" << std::endl;
    const float voxel_grid_size = 0.003f;
    pcl::VoxelGrid<pcl::PointXYZ> vox_grid;
    vox_grid.setLeafSize (voxel_grid_size, voxel_grid_size, voxel_grid_size);

    // Assign the templates that needs a match to previous model
    pcl::PointCloud<pcl::PointXYZ>::Ptr copy_cloud(new pcl::PointCloud<pcl::PointXYZ>);
    copyPointCloud(cloud, *copy_cloud);
    // ... and downsampling the point cloud
    vox_grid.setInputCloud (copy_cloud);
    pcl::PointCloud<pcl::PointXYZ>::Ptr downSampledCloud (new pcl::PointCloud<pcl::PointXYZ>);
    vox_grid.filter (*downSampledCloud);
    // Assign to the target FeatureCloud
    FeatureCloud template_cloud;
    template_cloud.setInputCloud(downSampledCloud);

    //convert model_cloud
    pcl::PointCloud<pcl::PointXYZ>::Ptr copy_input(new pcl::PointCloud<pcl::PointXYZ>);
    pcl::copyPointCloud(*model_cloud, *copy_input);
    // ... and downsampling the point cloud
    vox_grid.setInputCloud (copy_input);
    pcl::PointCloud<pcl::PointXYZ>::Ptr downSampledTargetCloud (new pcl::PointCloud<pcl::PointXYZ>);
    vox_grid.filter (*downSampledTargetCloud);
    // Assign to the target FeatureCloud
    FeatureCloud target_cloud;
    target_cloud.setInputCloud (downSampledTargetCloud);

    // Set the TemplateAlignment inputs
    TemplateAlignment template_align;
    template_align.setTargetCloud (target_cloud);

    TemplateAlignment::Result best_alignment;
    template_align.align(template_cloud, best_alignment);

    if(log_data)
    {
        t2 = boost::posix_time::microsec_clock::local_time();
        TimeDuration dt = t2- t1;
        data_file << "global registration time, " << (long)dt.total_milliseconds()/1000.0 << ", local registration time, ";
    }


    /*for (size_t i = 0; i < object_templates.size (); ++i)
    {
        template_align.addTemplateCloud (object_templates[i]);
    }

    // Find the best template alignment
    TemplateAlignment::Result best_alignment;
    int best_index = template_align.findBestAlignment (best_alignment);
    const FeatureCloud &best_template = object_templates[best_index];

    // Print the alignment fitness score (values less than 0.00002 are good)
    printf ("Best fitness score: %f\n", best_alignment.fitness_score);*/

    // Print the rotation matrix and translation vector
    Eigen::Matrix3f rotation = best_alignment.final_transformation.block<3,3>(0, 0);
    Eigen::Vector3f translation = best_alignment.final_transformation.block<3,1>(0, 3);
    std::cout << "rotation\n" << rotation << "\ntranslation\n" << translation << std::endl;

    return best_alignment.final_transformation.cast<double>();
}


/*
 * This is just a work in progress, so don't use it
 * */
Eigen::Matrix4d Registration::symmetryCheck(pcl::PointCloud<pcl::PointXYZRGB> &cloud, Eigen::Matrix4d initial_transform)
{
    //transform to make a more sensfull search
    pcl::PointCloud<pcl::PointXYZRGB> turnedCloud;
    pcl::transformPointCloud(cloud, turnedCloud, initial_transform );

    //down sample for calculation time
    const float voxel_grid_size = 0.005f;
    pcl::VoxelGrid<pcl::PointXYZRGB> vox_grid;
    vox_grid.setLeafSize (voxel_grid_size, voxel_grid_size, voxel_grid_size);
    vox_grid.setInputCloud (model_cloud);
    pcl::PointCloud<pcl::PointXYZRGB>::Ptr sampledCloud (new pcl::PointCloud<pcl::PointXYZRGB>);
    vox_grid.filter (*sampledCloud);

    pcl::search::KdTree<pcl::PointXYZRGB> tree;
    tree.setInputCloud(sampledCloud);
    std::vector<int> pointIdxRadiusSearch;
    std::vector<float> pointRadiusSquaredDistance;
    std::vector<int> outlier_indexes;
    //search the nearest points
    for(unsigned int i = 0; i<turnedCloud.size(); i++)
    {
        //check if there are any neigbours
        if(tree.radiusSearch (turnedCloud[i], 0.02, pointIdxRadiusSearch, pointRadiusSquaredDistance) == 1)
        {
            outlier_indexes.push_back(i);
        }
    }
    //outlier cloud
    pcl::PointCloud<pcl::PointXYZRGB> outlierCloud;
    pcl::copyPointCloud(turnedCloud, outlier_indexes, outlierCloud);
    pcl::io::savePLYFile("outlierCloud.ply", outlierCloud);


    Eigen::Matrix4d ret;
    ret.setIdentity();

    return ret;
    //check if it is likely occlusion space

}


void Registration::set_filename(std::string filename)
{
    if(data_file.is_open())
    {
        data_file.close();
    }

    if(log_data)
    {
        this->filename = filename;

        boost::filesystem::path dir("../../../Data/" + filename + "/Registration");
        if(!(boost::filesystem::exists(dir)))
        {
            if(!boost::filesystem::create_directories(dir))
            {
                ROS_ERROR("Directory could not be created");
            }
        }

        std::cout << "writing file: " << filename << std::endl;
        data_file.open("../../../Data/" + filename + "/Registration/data.txt");
        data_file << "Time transform origin, Time ground filter, Global transform, Time TrICP" << std::endl;

        std::ofstream settings_file("../../../Data/" + filename + "/Registration/settings.txt");
        settings_file << "Ground filter: " << ground_filter << std::endl;
        settings_file << "Overlap ratio: " << OVERLAP_RATIO << std::endl;
        settings_file << "Max iteration: " << trimmed_icp.getMaximumIterations() << std::endl;
        settings_file << "Fitness criterion: " << trimmed_icp.getTransformationEpsilon() << std::endl;
        settings_file << "Epsilon: " << trimmed_icp.getEuclideanFitnessEpsilon() << std::endl;
        settings_file.close();
    }
}


Eigen::Matrix4d Registration::addPointCloud(pcl::PointCloud<pcl::PointXYZRGB> &cloud, double overlap)
{
    view_count++;
    save_count++;
    Eigen::Matrix4d transform;

    trimmed_icp.removeCorrespondenceRejector(0);
    trimmed_rejector->setOverlapRatio(overlap);
    trimmed_icp.addCorrespondenceRejector(trimmed_rejector);

    Time t1,t2;
    if(log_data)
    {
        t1 = boost::posix_time::microsec_clock::local_time();
    }

    if(view_count == 1)
    {
        (*model_cloud) += cloud;
        transform.setIdentity();
    }
    else
    {
        pcl::PointCloud<pcl::PointXYZRGB> old_cloud(cloud);
        trimmed_icp.setInputSource( cloud.makeShared() );
        //trimmed_icp.setInputSource(cloud);
        trimmed_icp.setInputTarget(model_cloud);
        //pcl::PointCloud<pcl::PointXYZRGB> Final_Trimmed;
        //trimmed_icp.align(Final_Trimmed);
        trimmed_icp.align(cloud);
        transform = trimmed_icp.getFinalTransformation().cast<double>();

        ROS_INFO_STREAM("Local transform: \n" << transform << std::endl);
        if(std::abs(transform(0,0)) < 0.7 || std::abs(transform(1,1)) < 0.7 || std::abs(transform(2,2)) < 0.7)
        {
            ROS_INFO_STREAM("local transform ignored" << std::endl);
            cloud = old_cloud;
            transform.setIdentity();
        }

        //(*model_cloud) += Final_Trimmed;
        (*model_cloud) += cloud;
    }

    if(log_data)
    {
        t2 = boost::posix_time::microsec_clock::local_time();
        TimeDuration dt = t2- t1;
        data_file << (long)dt.total_milliseconds()/1000.0 << ", " << std::endl;
    }

    if(log_data)
    {
        pcl::io::savePLYFile("../../../Data/" + filename + "/Registration/cloud" + std::to_string(save_count) + ".ply", cloud);
    }


    return transform;
}

void Registration::transformOrigin(const pcl::PointCloud<pcl::PointXYZRGB> &inputCloud, pcl::PointCloud<pcl::PointXYZRGB> &outputCloud, Eigen::Matrix4d transform)
{
    Time t1,t2;
    if(log_data)
    {
        t1 = boost::posix_time::microsec_clock::local_time();
    }

    //don't waste time if there is no transform to be done
    if( !(transform.isIdentity()) )
    {
        pcl::transformPointCloud(inputCloud, outputCloud, transform );
    }
    else
    {
        outputCloud = inputCloud;
    }

    if(log_data)
    {
        t2 = boost::posix_time::microsec_clock::local_time();
        TimeDuration dt = t2- t1;
        data_file << (long)dt.total_milliseconds()/1000.0 << ", ";
    }
}

void Registration::filterGround(const pcl::PointCloud<pcl::PointXYZRGB> &inputCloud, pcl::PointCloud<pcl::PointXYZRGB> &filteredCloud)
{
    ROS_INFO_STREAM("filterGorund called");
    Time t1,t2;
    if(log_data)
    {
        t1 = boost::posix_time::microsec_clock::local_time();
    }

    //Some filtering of points on the ground
    std::vector<int> include;
    pcl::PointXYZRGB tempPoint;
    for(unsigned int i=0; i<inputCloud.size(); i++)
    {
        tempPoint = inputCloud[i];
        if(tempPoint.z > ground_filter && tempPoint.z < 0.65 && tempPoint.x < 0.15 && tempPoint.x > -0.15 && std::abs(tempPoint.y) < 0.15 && tempPoint.y > -0.15 )
        {
            include.push_back(i);
        }
    }
    pcl::copyPointCloud(inputCloud, include, filteredCloud);

    if(log_data)
    {
        t2 = boost::posix_time::microsec_clock::local_time();
        TimeDuration dt = t2- t1;
        data_file << (long)dt.total_milliseconds()/1000.0 << ", ";
    }

    ROS_INFO_STREAM("filterGorund end");
}

void Registration::filterGroundPlane(const pcl::PointCloud<pcl::PointXYZRGB> &inputCloud, pcl::PointCloud<pcl::PointXYZRGB> &filteredCloud)
{
    ROS_INFO_STREAM("filterGorundPlane called");
    Time t1,t2;
    if(log_data)
    {
        t1 = boost::posix_time::microsec_clock::local_time();
    }

    pcl::PointCloud<pcl::PointXYZRGB>::Ptr temp_cloud(new pcl::PointCloud<pcl::PointXYZRGB>);
    pcl::copyPointCloud(inputCloud, *temp_cloud);

     // Segment the ground
     pcl::ModelCoefficients::Ptr plane (new pcl::ModelCoefficients);
     pcl::PointIndices::Ptr 		inliers_plane (new pcl::PointIndices);
     pcl::PointCloud<pcl::PointXYZRGB>::Ptr cloud_plane (new pcl::PointCloud<pcl::PointXYZRGB>);

     // Make room for a plane equation (ax+by+cz+d=0)
     plane->values.resize (4);

     pcl::SACSegmentation<pcl::PointXYZRGB> seg;				// Create the segmentation object
     seg.setOptimizeCoefficients (false);				// Optional
     seg.setMethodType (pcl::SAC_RANSAC);
     seg.setModelType (pcl::SACMODEL_PLANE);
     seg.setDistanceThreshold ((float)plane_filter_max_dist);
     seg.setInputCloud (temp_cloud);
     seg.segment (*inliers_plane, *plane);

     // Extract inliers
     pcl::ExtractIndices<pcl::PointXYZRGB> extract;
     extract.setInputCloud (temp_cloud);
     extract.setIndices (inliers_plane);
     extract.setNegative (true);			// Extract the outliers
     extract.filter (*cloud_plane);		// cloud_outliers contains everything but the plane


     std::cerr << "Model coefficients: " << plane->values[0] << " "
                                         << plane->values[1] << " "
                                         << plane->values[2] << " "
                                         << plane->values[3] << std::endl;

     std::vector<int> include;
     pcl::PointXYZRGB tempPoint;
     for(unsigned int i=0; i<cloud_plane->size(); i++)
     {
         tempPoint = cloud_plane->points[i];
         if( tempPoint.z > ground_filter && tempPoint.z < 0.65 && tempPoint.x < 0.15 && tempPoint.x > -0.15 && tempPoint.y < 0.15 && tempPoint.y > -0.15 )
         {
             include.push_back(i);
         }
     }

    pcl::copyPointCloud(*cloud_plane,include ,filteredCloud);

    if(log_data)
    {
        t2 = boost::posix_time::microsec_clock::local_time();
        TimeDuration dt = t2- t1;
        data_file << (long)dt.total_milliseconds()/1000.0 << ", ";
    }

    ROS_INFO_STREAM("filterGorundPlane end");
}

void Registration::filterStatisticalDistance(const pcl::PointCloud<pcl::PointXYZRGB> &inputCloud, pcl::PointCloud<pcl::PointXYZRGB> &filteredCloud, double dist)
{
    if(!inputCloud.empty())
    {
        //put data into eigen format
        Eigen::MatrixXd data(inputCloud.size(),3);

        for(unsigned int i=0; i<inputCloud.size(); i++)
        {
            data.block<1,3>(i,0) = Eigen::Vector3d(inputCloud[i].x,inputCloud[i].y,inputCloud[i].z);
        }

        //Calculate mean
        Eigen::Vector3d mean = data.colwise().mean();
        //calculate sample variance
        Eigen::MatrixXd centered = data.rowwise() - data.colwise().mean();
        Eigen::MatrixXd cov = (centered.adjoint() * centered) / double(data.rows() - 1);

        /*testFilterStatisticalDistance(mean, cov, dist);
        std::ofstream scan_file("statistical_cloud.txt");
        for(unsigned int i=0; i<inputCloud.size(); i++)
        {
            scan_file << inputCloud[i].x << ", " << inputCloud[i].y << ", " << inputCloud[i].z << std::endl;
        }
        scan_file.close();*/

        //calculate what points are outliers, this will be based on outliers, so there might be a more statistical correct way
        //statistics book 211 bottom
        std::vector<int> include;
        for(unsigned int i=0; i<inputCloud.size(); i++)
        {
            //less than is if the point is accepted
            //n is currently not included in the formula
            Eigen::Vector3d mean_diff = mean.adjoint()-data.block<1,3>(i,0);
            if(mean_diff.adjoint() * cov.inverse() * mean_diff < dist)
            {
                include.push_back(i);
            }
        }

        pcl::copyPointCloud(inputCloud, include, filteredCloud);
    }

}

void Registration::testFilterStatisticalDistance(Eigen::Vector3d mean, Eigen::MatrixXd cov, double dist)
{
    std::ofstream test_file("statistical_elipse.txt");
    double res = 0.02;
    for(double x = -0.5; x < 0.5; x+=res)
    {
        for(double y = -0.5; y < 0.5; y+=res)
        {
            for(double z = -0.5; z < 0.5; z+=res)
            {
                Eigen::Vector3d mean_diff = mean- Eigen::Vector3d(x,y,z);

                //std::cout << x << ", " << y << ", " << z << "has dist value of: " << (mean_diff.adjoint() * cov.inverse() * mean_diff) << std::endl;

                if(mean_diff.adjoint() * cov.inverse() * mean_diff < dist)
                {
                    test_file << x << ", " << y << ", " << z << std::endl;
                }
            }
        }
    }
    test_file.close();
}
