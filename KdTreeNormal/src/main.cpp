#include "KdTreeNormal.h"
#include <pcl/kdtree/kdtree_flann.h>
#include <pcl/features/normal_3d.h>
#include <pcl/surface/gp3.h>


int main(int argc, char **argv)
{
    pcl::PointCloud<pcl::PointNormal>::Ptr cloud (new pcl::PointCloud<pcl::PointNormal>);

     // Generate pointcloud data
     cloud->width = 1000;
     cloud->height = 1;
     cloud->points.resize (cloud->width * cloud->height);

     for (size_t i = 0; i < cloud->points.size (); ++i)
     {
       cloud->points[i].x = 1024.0f * rand () / (RAND_MAX + 1.0f);
       cloud->points[i].y = 1024.0f * rand () / (RAND_MAX + 1.0f);
       cloud->points[i].z = 1024.0f * rand () / (RAND_MAX + 1.0f);
     }

    KdTreeNormal::Ptr tree(new KdTreeNormal);
    tree->setInputCloud(cloud);

    pcl::PointNormal searchPoint;

     searchPoint.x = 1024.0f * rand () / (RAND_MAX + 1.0f);
     searchPoint.y = 1024.0f * rand () / (RAND_MAX + 1.0f);
     searchPoint.z = 1024.0f * rand () / (RAND_MAX + 1.0f);

    // Neighbors within radius search
    std::vector<int> pointIdxRadiusSearch;
    std::vector<float> pointRadiusSquaredDistance;

    float radius = 56;

    std::cout << "Neighbors within radius search at (" << searchPoint.x
            << " " << searchPoint.y
            << " " << searchPoint.z
            << ") with radius=" << radius << std::endl;


    if ( tree->radiusSearch (searchPoint, radius, pointIdxRadiusSearch, pointRadiusSquaredDistance) > 0 )
    {
    for (size_t i = 0; i < pointIdxRadiusSearch.size (); ++i)
      std::cout << "    "  <<   cloud->points[ pointIdxRadiusSearch[i] ].x
                << " " << cloud->points[ pointIdxRadiusSearch[i] ].y
                << " " << cloud->points[ pointIdxRadiusSearch[i] ].z
                << " (squared distance: " << pointRadiusSquaredDistance[i] << ")" << std::endl;
    }

    /*// Initialize objects
    pcl::GreedyProjectionTriangulation<pcl::PointNormal> gp3;
    pcl::PolygonMesh::Ptr triangles(new pcl::PolygonMesh);

    // Set the maximum distance between connected points (maximum edge length)
    gp3.setSearchRadius (0.025);

    // Set typical values for the parameters
    gp3.setMu (2.5);
    gp3.setMaximumNearestNeighbors (300);
    gp3.setMaximumSurfaceAngle(M_PI/4); // 45 degrees
    gp3.setMinimumAngle(M_PI/18); // 10 degrees
    gp3.setMaximumAngle(2*M_PI/3); // 120 degrees
    gp3.setNormalConsistency(false);

    // Get result
    gp3.setInputCloud (clouds);
    gp3.setSearchMethod (tree);
    gp3.reconstruct (*triangles);*/

    return 0;
}
