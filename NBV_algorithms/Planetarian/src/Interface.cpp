#include "planetarian.h"
#include "../../Shared/Quaternion.h"
#include "../../Shared/libaryTranslation.h"
#include "ros/ros.h"
#include "ros/spinner.h"
#include "geometry_msgs/Transform.h"
#include "simsensorplugin/pointCloud.h"
#include <std_msgs/Bool.h>
#include <std_msgs/String.h>
#include "view_pick/pose.h"
#include "surfacebased/pose_array.h"
#include "pathplanner/isPoseValid.h"

#include <mutex>
#include <sensor_msgs/PointCloud2.h>
#include <pcl_conversions/pcl_conversions.h>
#include <pcl/visualization/cloud_viewer.h>
#include <pcl/visualization/pcl_visualizer.h>

#include "boost/date_time/posix_time/posix_time.hpp"
typedef boost::posix_time::ptime Time;
typedef boost::posix_time::time_duration TimeDuration;

pcl::PointCloud<pcl::PointXYZRGB> cloudRGB;
pcl::PointCloud<pcl::PointXYZ> cloud;
ros::Publisher pose_message;
ros::Publisher done_message;
transform pose;
std::vector<transform> pose_options;
NBV_Planetarian *method;
bool done = false;
bool surfaceBased_newData = false;
ros::ServiceClient pose_client;

//parameters
bool log_data;
bool hybrid;
bool load_view_points;
int view_points;
int max_iterations;
int threshold;
std::string filename;
int pixel_stepsize_update, pixel_stepsize_evaluation;
double sphere_size;
std::mutex init;

//function for datalogging of time usage
void datalog(int points, long time)
{
    static bool started = false;
    //static std::ofstream file("/home/morten/planetarium_times.txt");
    static std::ofstream file("planetarium_times.txt");

    if(started == false)
    {
        std::cout << "logging data: planetarium" << std::endl;
        file << "points, time" << std::endl;
        started = true;
    }

    file << points << ", "<< time/1000.0 << std::endl;
}

void filterViewPoints()
{
    pathplanner::isPoseValid::Request  req;
    pathplanner::isPoseValid::Response res;

    std::vector<transform> temp = method->getViewPoints();
    std::vector<int> index;
    for(unsigned int i=0; i<temp.size(); i++)
    {
        req.transf = Libary::toROSFromEigen(temp[i].translation, temp[i].rotation);
        pose_client.call(req, res);

        if(res.isValid)
        {
            index.push_back(i);
        }
    }

    std::vector<transform> updated_views;
    for(unsigned int i=0; i<index.size(); i++)
    {
        updated_views.push_back(temp[index[i]]);
    }

    method->setViewPoints(updated_views);
}

void resetCallback(const std_msgs::BoolConstPtr reset)
{
    delete method;
    init.lock();
    method = new NBV_Planetarian(sphere_size, view_points, max_iterations, threshold, pixel_stepsize_update, pixel_stepsize_evaluation, load_view_points, log_data, filename);
    init.unlock();
    if(!load_view_points)
    {
        filterViewPoints();
    }
}

void filenameCallback(const std_msgs::StringConstPtr file)
{
    init.lock();
    filename = file->data;
    method->set_filename(file->data);
    init.unlock();
}

void turnCallback(const geometry_msgs::TransformConstPtr turn)
{
    ROS_INFO_STREAM("Turn called");
    transform ransac_transform;
    ransac_transform.translation = Libary::toEigenVector3FromROS(*turn);
    ransac_transform.rotation = Libary::toEigenQuaternionFromROS(*turn);
    method->rotate_scene(ransac_transform);
}

void optionsCallback(const surfacebased::pose_arrayConstPtr options)
{
    ROS_INFO_STREAM("got some options");

    pathplanner::isPoseValid::Request  req;
    pathplanner::isPoseValid::Response res;
    transform temp;
    for(unsigned int i=0; i<options->transform.size(); i++)
    {
        temp.translation = Libary::toEigenVector3FromROS(options->transform[i]);
        temp.rotation = Libary::toEigenQuaternionFromROS(options->transform[i]);
        pose_options.push_back(temp);
    }

    surfaceBased_newData = true;
}

void pointCloudCallback(const simsensorplugin::pointCloudPtr pointCloud)
{
    init.lock();
    sensor_msgs::PointCloud2 data = pointCloud->pointcloud;
    transform received;
    received.translation = Libary::toEigenVector3FromROS(pointCloud->transform);
    received.rotation = Libary::toEigenQuaternionFromROS(pointCloud->transform);
    received.rotation = received.rotation.inverse();

    pcl::fromROSMsg(data, cloudRGB);
    pcl::copyPointCloud(cloudRGB, cloud);

    pose = method->next(cloud, received, done);

    if(hybrid)
    {
        ROS_INFO_STREAM("hybrid called");
        transform old_pose;
        ros::Rate r(5);
        while(ros::ok() && !surfaceBased_newData)
        {
            //ROS_INFO_STREAM("waiting on data");
            //ros::spinOnce();
            r.sleep();
        }

        ROS_INFO_STREAM("got topic");
        surfaceBased_newData = false;
        bool change = false;
        pose = method->hybrid(pose_options, change);
        std::cout << "Surfacebased options: " << pose_options.size() <<" pick from surfacebased: " << change <<  std::endl;
        pose_options.clear();
        if(change)
        {
            done = false;
        }
        ROS_INFO_STREAM("hybrid done");
    }

    //publish current pose
    if(done == false)
    {
        view_pick::pose pose_msg;
        pose_msg.header.stamp = ros::Time::now();
        pose_msg.transform = Libary::toROSFromEigen(pose.translation, pose.rotation);
        pose_msg.score = 0;
        pose_message.publish(pose_msg);
    }
    //publish a done signal
    else
    {
        done = false;
        std_msgs::Bool msg;
        msg.data = true;
        done_message.publish(msg);
    }
    init.unlock();
}



int main(int argc, char **argv)
{
    ros::init(argc, argv, "Planetarian_node");
    ros::NodeHandle nodeHandler("planetarian");

    //local handle for parameters
    ros::NodeHandle n("~");
    //load parameters
    n.param<bool>("log_data", log_data, false);
    n.param<bool>("hybrid", hybrid, false);
    n.param<bool>("load_view_points", load_view_points, false);
    n.param<int>("max_iterations", max_iterations, 20);
    n.param<int>("threshold", threshold, 20);
    n.param<int>("view_points", view_points, 100);
    n.param<int>("pixel_stepsize_update", pixel_stepsize_update, 16);
    n.param<int>("pixel_stepsize_evalutaion", pixel_stepsize_evaluation, 32);
    n.param<double>("sphere_size", sphere_size, 1);

    ros::Subscriber sub_cloud = nodeHandler.subscribe("/registration/PointCloudMsg_Part", 4, pointCloudCallback);
    ros::Subscriber sub_reset = nodeHandler.subscribe("/view_pick_node/reset", 4, resetCallback);
    ros::Subscriber sub_turn = nodeHandler.subscribe("/registration/Global_Transform", 4, turnCallback);
    ros::Subscriber sub_file = nodeHandler.subscribe("/GUI/filename", 4, filenameCallback);
    ros::Subscriber sub_options = nodeHandler.subscribe("/surfacebased/options", 4, optionsCallback);

    std::cout << "resolution: " << pixel_stepsize_update << " and " << pixel_stepsize_evaluation << "is used" << std::endl;
    //Create planetarium
    method = new NBV_Planetarian(sphere_size, view_points, max_iterations, threshold, pixel_stepsize_update, pixel_stepsize_evaluation, load_view_points, log_data);

    done_message = nodeHandler.advertise<std_msgs::Bool>("finished", 1);
    pose_message = nodeHandler.advertise<view_pick::pose>("pose", 1);


    pose_client = nodeHandler.serviceClient<pathplanner::isPoseValid>("/pathplanner/isPoseValid");

    Time t1,t2;
    t1 = boost::posix_time::microsec_clock::local_time();

    if(!load_view_points)
    {
        filterViewPoints();
    }
    
    t2 = boost::posix_time::microsec_clock::local_time();
    TimeDuration dt = t2- t1;
    std::cout << "time for filtering: " << (long)dt.total_milliseconds()/1000.0 << std::endl;


    ros::MultiThreadedSpinner spinner(2); // Use 2 threads
    spinner.spin(); // spin() will not return until the node has been shutdown

    return 0;
}
