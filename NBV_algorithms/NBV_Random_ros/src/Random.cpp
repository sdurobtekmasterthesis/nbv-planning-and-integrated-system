#include "Random.h"
#include "../../Shared/EAA.h"
#include "../../Shared/Quaternion.h"
#include "eigen3/Eigen/Geometry"

#include "fstream"
#include "stdlib.h"
#include "math.h"

#define SPHERE 1
#define PI 3.14159265359

NBV_Random::NBV_Random()
{
    iteration_counter = 0;
    iteration_max = 10;
}

transform NBV_Random::next(pcl::PointCloud<pcl::PointXYZ> PointCloud, bool &done)
{
    iteration_counter++;

    if(iteration_counter == iteration_max)
    {
        done = true;
    }

    //polar coordinates
    double r = SPHERE;
    double theta = ((double) rand() / (RAND_MAX))*PI/2.0;
    double phi = ((double) rand() / (RAND_MAX))*2*PI;
    Eigen::Vector3d start;
    start.setZero(3);

    Eigen::Vector3d point = Eigen::Vector3d(r*sin(theta)*cos(phi),r*sin(theta)*sin(phi),r*cos(theta)) + start;

    /*Eigen::Vector3d direction = start-point;
    direction.normalize();

    Eigen::Vector3d z_axis(0,0,1);
    Eigen::Vector3d eaa_axis = direction.cross(z_axis);
    double eaa_rotation = acos(direction.dot(z_axis));
    //Eigen::MatrixXd rotation = Reaa(eaa_axis, eaa_rotation);

    Eigen::AngleAxisd convert(eaa_rotation, eaa_axis);
    Eigen::Quaterniond quat(convert);*/

    transform pose;
    pose.translation = point;
    pose.rotation = Quaternion::ToOrigin(point);

    return pose;
}

void NBV_Random::getInfo()
{
    std::string filename = "";
    filename += "info/iteration_";
    filename += std::to_string(iteration_counter);
    filename += ".txt";

    std::ofstream info( filename.c_str() );

    info << "iterations: " << iteration_counter << std::endl;

    info.close();
}
