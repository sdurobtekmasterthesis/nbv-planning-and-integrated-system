#ifndef RECONSTRUCTION_H
#define RECONSTRUCTION_H


#include "eigen3/Eigen/Core"
#include "eigen3/Eigen/Geometry"

#include "pcl/PolygonMesh.h"
#include <pcl/io/pcd_io.h>
#include <pcl/io/ply_io.h>
#include <pcl/point_types.h>
#include "pcl/point_cloud.h"
#include <pcl/search/kdtree.h>
#include <pcl/features/normal_3d_omp.h>
#include <pcl/surface/marching_cubes_rbf.h>
#include <pcl/surface/marching_cubes_hoppe.h>
#include <pcl/common/transforms.h>

//for greedy
#include <pcl/kdtree/kdtree_flann.h>
#include <pcl/features/normal_3d.h>
#include <pcl/surface/gp3.h>

class Reconstruction
{
    public:
        Reconstruction(bool log_data = false, std::string filename="data_default");
        pcl::PolygonMesh::Ptr marchingCubes();
        pcl::PolygonMesh::Ptr greedyTriangulation();
        void setFilename(std::string filename);
        void addCloudConsistentNormals(pcl::PointCloud<pcl::PointXYZ>::Ptr cloud, Eigen::Vector3d dir);
        void saveCloud();
        void saveCloud(pcl::PointCloud<pcl::PointNormal>::Ptr clouds);
        void combineCloud(Eigen::Matrix4d transform);

    private:
        pcl::PointCloud<pcl::PointNormal>::Ptr clouds;
        pcl::PointCloud<pcl::PointNormal>::Ptr clouds_2;
        std::string filename;
        bool log_data;
        unsigned int name_number;
};

#endif // RECONSTRUCTION_H
