#include "planetarian.h"
#include "../../Shared/Quaternion.h"
#include "eigen3/Eigen/Geometry"

#include <pcl/common/transforms.h>
#include <pcl/common/common.h>
#include <pcl_conversions/pcl_conversions.h>
#include <pcl/visualization/cloud_viewer.h>
#include <pcl/visualization/pcl_visualizer.h>

#include <stdlib.h> // for rand() function
#include <string>
#include <cmath>
#include <vector>
#include <chrono>

#include <boost/filesystem.hpp>
#include "boost/date_time/posix_time/posix_time.hpp"
typedef boost::posix_time::ptime Time;
typedef boost::posix_time::time_duration TimeDuration;

#define PI          3.14159265359

// Ray searchs parameters
#define OCTREE_VOXEL_SIZE 0.005
#define OCTREE_BOUNDBOX_SIZE 0.15 // half lenght of the sides.
#define OCTREE_CENTER_X 0.0
#define OCTREE_CENTER_Y 0.0
#define OCTREE_CENTER_Z 0.15
#define VOXELS_NEW 200

#define CamRes_X 640
#define CamRes_Y 480
//#define PIXEL_STEP 32

NBV_Planetarian::NBV_Planetarian(double sphere_size, int views, int max_iterations, int stopping_threshold, int resolution_camera_update,  int resolution_camera_evaluation, bool &load_view_points, bool log_data, std::string filename):
    octree(OCTREE_VOXEL_SIZE), //octree has no default constructer so constructor has to be called here
    octree_cloud(new pcl::PointCloud<pcl::PointXYZ>()),
    explored_octree(OCTREE_VOXEL_SIZE), //octree has no default constructer so constructor has to be called here
    explored_octree_cloud(new pcl::PointCloud<pcl::PointXYZ>()),
    octree_dense_search(OCTREE_VOXEL_SIZE),
    octree_dense_search_cloud(new pcl::PointCloud<pcl::PointXYZ>())
{
    iteration_counter = 0;
    iteration_max = max_iterations;
    voxels_threshold = stopping_threshold;
    //structure for making addaptive resolution
    addaptive = false;

    global_pose << 1, 0, 0, 0,
                   0, 1, 0, 0,
                   0, 0, 1, 0,
                   0, 0, 0, 1;


    if(resolution_camera_evaluation == 0)
    {
         addaptive = true;
        addaptive_resolution = new int[iteration_max];
        addaptive_resolution[0] = 128;
        addaptive_resolution[1] = 128;
        addaptive_resolution[2] = 32;
        addaptive_resolution[3] = 32;
        for(int i=4; i<iteration_max; i++)
        {
            addaptive_resolution[i] = 32;
        }
    }

    this->views = views;
    //150 is iterations for uniform method and currently just tested
    sphere = sphere_size;

    if(load_view_points)
    {
        setViewPoints(loadViewPoints());
    }
    if(view_points.size() == 0)
    {
        load_view_points = false;
        generateViewpointsUniform(150,true);
        saveViewPoints(view_points);
    }

    res_update.x = resolution_camera_update;
    res_update.y = resolution_camera_update;
    res_evaluation.x = resolution_camera_evaluation;
    res_evaluation.y = resolution_camera_evaluation;

    best = -1;

    instantiateOctree();

    this->log_data = log_data;
    set_filename(filename);
}

/*
 * Function to control each iteration
 * */
transform NBV_Planetarian::next(const pcl::PointCloud<pcl::PointXYZ> &pointCloud, transform current, bool &done)
{
    current_pose = current;

    int voxels_new = 0;

    Time t1,t2;
    if(log_data)
    {
        data_file << std::endl;
        t1 = boost::posix_time::microsec_clock::local_time();
    }

    voxels_new = updateOctreePCL(pointCloud);

    if(log_data)
    {
        t2 = boost::posix_time::microsec_clock::local_time();
        TimeDuration dt = t2- t1;
        data_file << (long)dt.total_milliseconds()/1000.0 << ", ";
    }

//    if(iteration_counter == 2)
//    {
//        std::cout << "Resize octree start " << std::endl;
//        resize_sphere();
//        std::cout << "Resize octree end " << std::endl;
//    }

    //set next value for addatptive if set
    if(addaptive)
    {
        res_evaluation.x = addaptive_resolution[iteration_counter];
        res_evaluation.y = addaptive_resolution[iteration_counter];
    }

    //test for different resolutions with same data
    //info_resolution();

    //simulate all views to find the next option
    best = 0;
    best_score = 0;
    //auto start_time = std::chrono::high_resolution_clock::now();
    if(log_data)
    {
        t1 = boost::posix_time::microsec_clock::local_time();
    }

    if(view_points.size() == 0)
    {
        throw("There is no valid views");
    }

    for(unsigned int i=0; i < view_points.size(); i++)
    {
        if(!check_closed_list(i))//check closed list(true if seen)
        //if(true) //to turn of closed list
        {
            double temp = evaluateViewPCL(view_points[i]);
            if(temp > best_score)
            {
                best_score = temp;
                best = i;
            }
        }
    }

    while(check_closed_list(best))
    {

        ++best;
    }

    if(log_data)
    {
        t2 = boost::posix_time::microsec_clock::local_time();

        TimeDuration dt = t2- t1;
        data_file << (long)dt.total_milliseconds()/1000.0 << ", ";
    }

    closed_list.push_back(best);
    std::cout << "picked view: " << best << " with score: " << best_score << std::endl;
    if(log_data)
    {
        data_file << best << ", " << best_score << ", ";
    }

    //pickBest
    current_pose = view_points[best];

    Eigen::Matrix4d transform_orig; // Your Transformation Matrix
    transform_orig.setIdentity();   // Set to Identity to make bottom row of Matrix 0,0,0,1
    transform_orig.block<3,3>(0,0) = current_pose.rotation.toRotationMatrix();
    transform_orig.block<3,1>(0,3) = current_pose.translation;

    transform_orig = transform_orig * global_pose.inverse();

    current_pose.rotation = Eigen::Quaterniond(transform_orig.block<3,3>(0,0));
    current_pose.translation = transform_orig.block<3,1>(0,3);

    std::cout << "Voxels found:" << voxels_new << " threshold: " << voxels_threshold << std::endl;

    //set termination criteria here
    if(voxels_new < voxels_threshold)
    {
        done = true;
        this->done = done;
    }

    if(iteration_counter == iteration_max-1) // && voxels_new <= VOXELS_NEW)
    {
        done = true;
        this->done = done;
    }
    iteration_counter++;

    return current_pose;
}

/*
 * Hybrid needs to be called after next
 * */
transform NBV_Planetarian::hybrid(std::vector<transform> options, bool &change)
{
    Time t1,t2;
    if(log_data)
    {
        t1 = boost::posix_time::microsec_clock::local_time();
    }


    int old_best_score = best_score;
    for(unsigned int i=0; i < options.size(); i++)
    {
        double temp = evaluateViewPCL(options[i]);
        if(temp > best_score)
        {
            best_score = temp;
            current_pose = options[i];
        }
    }
    if(old_best_score != best_score)
    {
        //remove from close list because it didn't get picked anyway
        closed_list.pop_back();
        change = true;
    }

    if(log_data)
    {
        t2 = boost::posix_time::microsec_clock::local_time();
        TimeDuration dt = t2- t1;
        data_file << (long)dt.total_milliseconds()/1000.0;
    }

    if(done && log_data)
    {
        data_file << std::endl;
    }

    return current_pose;
}

void NBV_Planetarian::info_resolution()
{
    pcl::PointXY res_saved = res_evaluation;
    //for resolution checking
    static std::ofstream info( "resoution_compare.txt" );

    int best = 0;
    double best_score = 0;

    for(res_evaluation.x = 1; res_evaluation.x < CamRes_Y; res_evaluation.x *= 2)
    {
        res_evaluation.y = res_evaluation.x;

        best = 0;
        best_score = 0;
        TimeDuration dt;
        Time t1(boost::posix_time::microsec_clock::local_time());
        for(unsigned int i=0; i < view_points.size(); i++)
        {
            double temp = evaluateViewPCL(view_points[i]);
            if(temp > best_score)
            {
                best_score = temp;
                best = i;
            }
        }
        Time t2(boost::posix_time::microsec_clock::local_time());
        dt = t2- t1;
        std::cout << "best i: " << best << " at resolution: " << res_evaluation.x << "with score: "<< best_score << std::endl;
        long time = dt.total_milliseconds();
        info << best << ", " << res_evaluation.x << ", "<< time/1000.0 << std::endl;
    }
    res_evaluation = res_saved;
}

bool NBV_Planetarian::check_closed_list(int view)
{
    bool ret = false;
    for(unsigned int i = 0; i<closed_list.size(); i++)
    {
        if(view == closed_list[i])
        {
            ret = true;
            break;
        }
    }
    return ret;
}

/*
 * evaluates amount of informaiton gain
*/
void NBV_Planetarian::info_information(int iteration, int voxels_found, int view, transform pose)
{
    //output for different resolutions and the given information gain
    static std::ofstream info( "information_gain(res_update_" + std::to_string((int)res_update.x) + ")(res_evaluation" + std::to_string((int)res_evaluation.x) + ").txt" );
    info << iteration << ", " << voxels_found << ", " << view << std::endl;
    static std::ofstream info_pose( "pose(res_update_" + std::to_string((int)res_update.x) + ")(res_evaluation" + std::to_string((int)res_evaluation.x) + ").txt" );
    info_pose << pose.translation << std::endl;
    info_pose << pose.rotation.toRotationMatrix() << std::endl;
}

void NBV_Planetarian::set_filename(std::string filename)
{
    this->filename = filename;

    if(data_file.is_open())
    {
        data_file.close();
    }

    if(log_data)
    {
        boost::filesystem::path dir("../../../Data/" + filename + "/Planetarium");
        if(!(boost::filesystem::exists(dir)))
        {

            if(!boost::filesystem::create_directories(dir))
            {
                ROS_ERROR("Directory could not be created");
            }
        }

        std::cout << "writing file" << std::endl;
        data_file.open("../../../Data/" + filename + "/Planetarium/data.txt", std::ofstream::app);
        data_file << "Seen voxels, Time update, Time evaluation, View picked, Evaluation value, Hybrid_time";

        std::ofstream settings_file("../../../Data/" + filename + "/Planetarium/settings.txt");
        settings_file << "Addaptive: " << addaptive;
        if(addaptive)
        {
            for(int i=0; i<iteration_max; i++)
            {
                settings_file << addaptive_resolution[i] << " ";
            }
        }
        settings_file << std::endl << "Resolution update: " << res_update.x;
        settings_file << std::endl << "Resolution evaluation: " << res_evaluation.x;
        settings_file << std::endl << "Number of view points: " << views;
        settings_file << std::endl << "Threshold for stopping criteria: " << voxels_threshold;
        settings_file.close();
        std::ofstream views_file("../../../Data/" + filename + "/Planetarium/view_points.txt");
        views_file << "x, y, z" << std::endl;
        for(unsigned int i=0; i<view_points.size(); i++)
        {
            views_file << view_points[i].translation.x() << ", " << view_points[i].translation.y() << ", " << view_points[i].translation.z() << ", " << std::endl;
        }
        views_file.close();
    }
}

/*
 * through bounds is set they can still be broken by adding point out of the box
 *
*/
void NBV_Planetarian::instantiateOctree()
{
    octree.defineBoundingBox(-OCTREE_BOUNDBOX_SIZE, -OCTREE_BOUNDBOX_SIZE, 0, OCTREE_BOUNDBOX_SIZE, OCTREE_BOUNDBOX_SIZE, 2*OCTREE_BOUNDBOX_SIZE); //x,y,z min / x,y,z max
    octree.setInputCloud(octree_cloud);
    octree.addPointsFromInputCloud();

    explored_octree.defineBoundingBox(-OCTREE_BOUNDBOX_SIZE, -OCTREE_BOUNDBOX_SIZE, 0, OCTREE_BOUNDBOX_SIZE, OCTREE_BOUNDBOX_SIZE, 2*OCTREE_BOUNDBOX_SIZE); //x,y,z min / x,y,z max
    explored_octree.setInputCloud(explored_octree_cloud);
    explored_octree.addPointsFromInputCloud();

    pcl::PointXYZ point;
    point.x = OCTREE_CENTER_X;
    point.y = OCTREE_CENTER_Y;
    point.z = OCTREE_CENTER_Z;
    octree_dense_search.defineBoundingBox(-OCTREE_BOUNDBOX_SIZE, -OCTREE_BOUNDBOX_SIZE, 0, OCTREE_BOUNDBOX_SIZE, OCTREE_BOUNDBOX_SIZE, 2*OCTREE_BOUNDBOX_SIZE); //x,y,z min / x,y,z max
    octree_dense_search.setInputCloud(octree_dense_search_cloud);
    instantiateDenseSearchOctree(point, OCTREE_BOUNDBOX_SIZE, OCTREE_VOXEL_SIZE);

}

void NBV_Planetarian::setViewPoints(std::vector<transform> view_points)
{
    this->view_points = view_points;
    saveViewPoints(view_points);
    std::ofstream views_file("../../../Data/" + filename + "/Planetarium/view_points.txt");
    views_file << "x, y, z" << std::endl;
    for(unsigned int i=0; i<view_points.size(); i++)
    {
        views_file << view_points[i].translation.x() << ", " << view_points[i].translation.y() << ", " << view_points[i].translation.z() << ", " << std::endl;
    }
    views_file.close();
    std::cout << "number of view points: " << this->view_points.size() << std::endl;
    std::cout << "number of view points given: " << view_points.size() << std::endl;
}

void NBV_Planetarian::instantiateDenseSearchOctree(pcl::PointXYZ center, double bounding_box, double voxel_resolution)
{
    // PCL Ray intersection
    // Calculate the amount of point need for each leaf voxel
    unsigned int leafVoxel_numbers = std::pow((bounding_box*2.0)/voxel_resolution, 3.0);
    std::cout << "leafVoxel_numbers: " << leafVoxel_numbers << std::endl;

    // Generate pointcloud data
    octree_dense_search_cloud->width = leafVoxel_numbers;
    octree_dense_search_cloud->height = 1;

    std::cout << "Width" << octree_dense_search_cloud->width << " heigt" << octree_dense_search_cloud->height << std::endl;

    octree_dense_search_cloud->points.resize (octree_dense_search_cloud->width * octree_dense_search_cloud->height);

    std::cout << "between" << std::endl;

    unsigned int i = 0;

    for (unsigned int x_step = 0; x_step < (bounding_box*2.0)/voxel_resolution; ++x_step)
    {
        for (unsigned int y_step = 0; y_step < (bounding_box*2.0)/voxel_resolution; ++y_step)
        {
            for (unsigned int z_step = 0; z_step < (bounding_box*2.0)/voxel_resolution; ++z_step)
            {
                //std::cout << "iteration: " << i << std::endl;
                octree_dense_search_cloud->points[i].x = (center.x - bounding_box) + voxel_resolution/2.0 + voxel_resolution * x_step;
                octree_dense_search_cloud->points[i].y = (center.y - bounding_box) + voxel_resolution/2.0 + voxel_resolution * y_step;
                octree_dense_search_cloud->points[i].z = (center.z - bounding_box) + voxel_resolution/2.0 + voxel_resolution * z_step;
                ++i;
            }
        }
    }

    std::cout << "Points made: " << octree_dense_search_cloud->size() << std::endl;
    // Set the search source
    octree_dense_search.addPointsFromInputCloud();

    std::cout << "Numnber of points in octree_dense_search_cloud: " << octree_dense_search_cloud->size() << std::endl;
}


/*
 * Takes in a pointcloud and updates the explored tree based on this
 * currently this is the "full" point cloud
 *
 */

int NBV_Planetarian::updateOctreePCL(const pcl::PointCloud<pcl::PointXYZ> &pointCloud)
{
    (*octree_cloud) += pointCloud;
    octree.deleteTree();
    octree.addPointsFromInputCloud();
    return updateExploredOctreePCL();
}


/*
 *
 */
int NBV_Planetarian::updateExploredOctreePCL()
{
    Eigen::Matrix4d cam_intrinsic;
    //Eigen::Matrix4d cam_extrinsic;

    cam_intrinsic <<
    574.0527954101562 ,     0.0     , 319.5 , 0.0
    , 0.0  , 574.0527954101562      , 239.5 , 0.0
    , 0.0  ,    0.0                 , 1.0   , 0.0
    , 0.0  ,     0.0  , 0.0 ,  1.0;


    //auto start_time = std::chrono::high_resolution_clock::now();
    pcl::octree::OctreePointCloudSearch<pcl::PointXYZ>::AlignedPointTVector centers = findIntersectedVoxels(current_pose.translation, current_pose.rotation, cam_intrinsic, res_update, true);

    //info_information(iteration_counter, centers.size(), best, current_pose);
    if(log_data)
    {
        data_file << centers.size()  << ", ";
    }

    std::cout << "number of voxel centers found in update: " << centers.size() << std::endl;

    // Add points to the explored ocrtee
    pcl::PointCloud<pcl::PointXYZ> temp_centers;
    temp_centers.width = 1;
    temp_centers.height = centers.size();
    temp_centers.reserve(temp_centers.height*temp_centers.width);

    for(unsigned int i = 0; i < centers.size(); i++)
    {
        temp_centers.push_back(centers[i]);
    }

    /*boost::shared_ptr<pcl::visualization::PCLVisualizer> viewer (new pcl::visualization::PCLVisualizer ("3D mesh Viewer"));
    viewer->addPointCloud(temp_centers.makeShared());
    viewer->addCoordinateSystem (0.3);
    while (!viewer->wasStopped() && ros::ok() )
    {
        viewer->spinOnce (100);
        boost::this_thread::sleep (boost::posix_time::microseconds (100000));
    }*/

    (*explored_octree_cloud) += (temp_centers);
    explored_octree.deleteTree();
    explored_octree.addPointsFromInputCloud();

    return centers.size();
}


/*
 * Function to evaluate each individual view and give a score of how good it is
 * Currently a high value is a good view
 *
 */

double NBV_Planetarian::evaluateViewPCL(transform view)
{
    Eigen::Matrix4d cam_intrinsic;

    cam_intrinsic <<
    574.0527954101562 ,     0.0     , 319.5 , 0.0
    , 0.0  , 574.0527954101562      , 239.5 , 0.0
    , 0.0  ,    0.0                 , 1.0   , 0.0
    , 0.0  ,     0.0  , 0.0 ,  1.0;

    pcl::octree::OctreePointCloudSearch<pcl::PointXYZ>::AlignedPointTVector centers = findIntersectedVoxels(view.translation, view.rotation, cam_intrinsic, res_evaluation, false);

    return centers.size();
}

/*
 *
*/
pcl::octree::OctreePointCloudSearch<pcl::PointXYZ>::AlignedPointTVector
NBV_Planetarian::findIntersectedVoxels( Eigen::Vector3d translation, Eigen::Quaternion<double> quaternion, Eigen::Matrix4d camMaxtrix, pcl::PointXY pixel_step, bool update_octree)
{
    pcl::octree::OctreePointCloudSearch<pcl::PointXYZ>::AlignedPointTVector centers_temp;
    pcl::octree::OctreePointCloudSearch<pcl::PointXYZ>::AlignedPointTVector centers;

    // Camera settings
    Eigen::Matrix4d transform; // Your Transformation Matrix
    transform.setIdentity();   // Set to Identity to make bottom row of Matrix 0,0,0,1
    // Use the old toRotation!!!
    transform.block<3,3>(0,0) = quaternion.toRotationMatrix().inverse();
    transform.block<3,1>(0,3) = translation;
    //std::cout << "transform:" << std::endl << transform << std::endl;

    Eigen::Matrix4d projection_combinded = camMaxtrix;

    Eigen::Vector4d start_point;
    Eigen::Vector4d end_point;
    Eigen::Vector4d direction;

    // Camera model + Pose
    for(unsigned int u = 0; u < CamRes_X-pixel_step.x ; u += pixel_step.x)
    {
        for(unsigned int v = 0; v < CamRes_Y-pixel_step.y; v += pixel_step.y)
        {
            //std::cout << "raycast (u,v)" << "(" << u  << "," << v << ")" << std::endl;
            // Find direction bases on pixel and cameraParams and the camera Pose
            start_point(2) = 1.0;
            start_point(0) =  start_point(2)/projection_combinded(0,0) * (u-projection_combinded(0,2));
            start_point(1) =  start_point(2)/projection_combinded(1,1) * (v-projection_combinded(1,2));
            start_point = transform * start_point;

            end_point(2) = 2.0;
            end_point(0) =  end_point(2)/projection_combinded(0,0) * (u-projection_combinded(0,2));
            end_point(1) =  end_point(2)/projection_combinded(1,1) * (v-projection_combinded(1,2));
            end_point = transform * end_point;

            // Find direction and normelize
            direction = end_point-start_point;
            direction.normalize();

            // seacrh for ray-box intersections
            octree_dense_search.getIntersectedVoxelCenters(translation.head(3).cast<float>(), direction.head<3>().cast<float>(), centers_temp );

            // Only return the center that are unxplored
            for(unsigned int i = 0; i < centers_temp.size(); i++)
            {
                if(octree.isVoxelOccupiedAtPoint(centers_temp[i]))
                {
                    // exit the for loop
                    break;
                }
                else if(!explored_octree.isVoxelOccupiedAtPoint(centers_temp[i]))
                {
                    if(update_octree)
                    {
                        //explored_octree_cloud(centers_temp[i]);
                        explored_octree.addPointToCloud(centers_temp[i], explored_octree_cloud);
                    }

                    centers.push_back(centers_temp[i]);
                }

            }
        }
    }

    return centers;
}

/*
 *
 */
//void NBV_Planetarian::resize_sphere()
//{
//    double biggest_distance = 0.0;
//    double biggest_projected_data = 0.0;
//    for(unsigned int i=0; i< octree_cloud->size(); i++)
//    {
//        //update highest projection for octree bounds
//        if(std::abs(octree_cloud->at(i).x) > biggest_projected_data)
//        {
//            biggest_projected_data = std::abs(octree_cloud->at(i).x);
//        }
//        if(std::abs(octree_cloud->at(i).y) > biggest_projected_data)
//        {
//            biggest_projected_data = abs(octree_cloud->at(i).y);
//        }
//        if( octree_cloud->at(i).z/2.0 > biggest_projected_data)
//        {
//            biggest_projected_data = octree_cloud->at(i).z/2.0;
//        }

//        //update highest distance point for sphere bound
//        Eigen::Vector3d point(octree_cloud->at(i).x, octree_cloud->at(i).y, octree_cloud->at(i).z);
//        double temp_distance = point.norm();
//        if(point.norm() > biggest_distance)
//        {
//            biggest_distance = temp_distance;
//        }
//    }

//    if( biggest_projected_data < OCTREE_BOUNDBOX_SIZE )
//    {

//        //Update ray tracing tree
//        octree_dense_search.deleteTree();
//        octree_dense_search_cloud->points.clear();

//        pcl::PointXYZ point;
//        point.x = OCTREE_CENTER_X;
//        point.y = OCTREE_CENTER_Y;
//        point.z = biggest_projected_data;
//        octree_dense_search.defineBoundingBox(-biggest_projected_data, -biggest_projected_data, 0, biggest_projected_data, biggest_projected_data, 2*biggest_projected_data); //x,y,z min / x,y,z max
//        octree_dense_search.setInputCloud(octree_dense_search_cloud);
//        instantiateDenseSearchOctree(point, biggest_projected_data, OCTREE_VOXEL_SIZE);

//        //Update explored tree
//        //find indices that should still be in the tree
//        std::vector<int> indices;

//        for(unsigned int i=0; i< explored_octree_cloud->points.size(); i++)
//        {
//            if( std::abs(explored_octree_cloud->at(i).x) < biggest_projected_data &&
//                std::abs(explored_octree_cloud->at(i).y) < biggest_projected_data &&
//                explored_octree_cloud->at(i).z/2.0 < biggest_projected_data)
//            {
//                indices.push_back(i);
//            }
//        }

//        //copy set of cloud
//        pcl::PointCloud<pcl::PointXYZ>::Ptr temp( new pcl::PointCloud<pcl::PointXYZ>(*explored_octree_cloud, indices) );
//        explored_octree_cloud->points.clear();
//        explored_octree_cloud=temp;

//        //remake octree
//        explored_octree.deleteTree();
//        explored_octree.defineBoundingBox(-biggest_projected_data, -biggest_projected_data, 0, biggest_projected_data, biggest_projected_data, 2*biggest_projected_data); //x,y,z min / x,y,z max
//        explored_octree.setInputCloud(explored_octree_cloud);
//        explored_octree.addPointsFromInputCloud();

//        //make the view sphere smaller
//        //resize_sphere(biggest_distance*1.5);
//    }
//}

//void NBV_Planetarian::resize_sphere(double distance)
//{
//    for(unsigned int i=0; i<view_points.size(); i++)
//    {
//        view_points[i].translation.normalize();
//        view_points[i].translation *= distance;
//    }
//}
/*
 * Turn the view points for object rotatio (fixture problem)
 *
 */
void NBV_Planetarian::rotate_scene(transform object_movement)
{
    Eigen::Matrix4d transform_new; // Your Transformation Matrix
    transform_new.setIdentity();   // Set to Identity to make bottom row of Matrix 0,0,0,1
    transform_new.block<3,3>(0,0) = object_movement.rotation.toRotationMatrix();
    transform_new.block<3,1>(0,3) = object_movement.translation;

    // Save the transformation of the new rotated scene and clear the view close list
    global_pose = transform_new;
    closed_list.clear();

    // Turn all view points
    for(unsigned int i = 0; i < view_points.size(); ++i)
    {

      transform temp = view_points[i];

      Eigen::Matrix4d transform_orig; // Your Transformation Matrix
      transform_orig.setIdentity();   // Set to Identity to make bottom row of Matrix 0,0,0,1
      transform_orig.block<3,3>(0,0) = temp.rotation.toRotationMatrix();
      transform_orig.block<3,1>(0,3) = temp.translation;

      transform_orig = transform_orig * transform_new;

      Eigen::Quaterniond q(transform_orig.block<3,3>(0,0));
      temp.rotation = q;
      temp.translation = transform_orig.block<3,1>(0,3);

      view_points[i] = temp;
    }
}

/*
 * First uses random positions, and is pushed away afterwards to make them uniformly seperated
 *
 */
void NBV_Planetarian::generateViewpointsUniform(int iterationCount, bool hemisphere)
{
    //randomly generate points
    for(int i=0; i<views; i++)
    {
        double u = (double)std::rand()/RAND_MAX;
        double v = (double)std::rand()/RAND_MAX/2;
        //std::cout << "u " << u << " v " << v << std::endl;
        double phi = 2*PI*u;
        double theta = acos(2*v-1);
        transform pose;
        pose.translation = Eigen::Vector3d(sin(theta)*cos(phi),sin(theta)*sin(phi),-cos(theta));;
        view_points.push_back(pose);
    }

    float targetDist;
    if (hemisphere)
        targetDist = 2*sqrt(2.0f/(float)view_points.size());
    else
        targetDist = 2*sqrt(4.0f/(float)view_points.size());

    float limit = targetDist;

    for(int i=0; i<iterationCount; i++)
    {
        //for each point in the list
        for(unsigned int a=0; a<view_points.size(); a++)
        {
            //let the other points push(probaly should go through all points and just ignore current instead)
            for(unsigned int b=0; b<view_points.size(); b++)
            {
                if(a != b)
                {
                //check their distance from each other
                Eigen::Vector3d dist = view_points[a].translation-view_points[b].translation;
                float length = dist.norm();
                if(length < 0.01)
                {
                    dist = Eigen::Vector3d(0.1,0.1,0.1);
                }
                dist.normalize();

                    if (length < limit) // same as bigger than 0
                    {
                        view_points[b].translation -= dist*0.03;

                        //view_points[a].translation.normalize();
                        view_points[b].translation.normalize();

                        //If any is pushed under sphere force them up
                        if (hemisphere)
                        {
                            //if (view_points[a].translation[2] < 0.0f)
                            //    view_points[a].translation[2] = 0.0f;
                        if (view_points[b].translation[2] < 0.40f)
                            view_points[b].translation[2] = 0.40f;
                        }
                    }
                }
            }
        }
    }

    for(unsigned int i=0; i<view_points.size(); i++)
    {
        view_points[i].rotation = Quaternion::ToOrigin(view_points[i].translation);
        view_points[i].translation *= sphere;

        //std::cout << "translation\n" << view_points[i].translation << std::endl;
        //std::cout << "rotation\n" << view_points[i].rotation.toRotationMatrix() << std::endl;

    }
}

void NBV_Planetarian::saveViewPoints(std::vector<transform> views)
{
    std::ofstream views_file("view_points.txt");
    views_file << views.size() << std::endl;
    for(unsigned int i=0; i<views.size(); i++)
    {
        views_file << views[i].translation.x() << "  " << views[i].translation.y() << "  " << views[i].translation.z() << "  " << std::endl;
    }
    views_file.close();
}

std::vector<transform> NBV_Planetarian::loadViewPoints()
{
    std::vector<transform> views;
    std::ifstream views_file("view_points.txt");
    if(views_file.is_open())
    {
        int nr_of_views;
        views_file >> nr_of_views;
        transform view;
        for(int i = 0; i<nr_of_views; i++)
        {
            views_file >> view.translation[0];
            views_file >> view.translation[1];
            views_file >> view.translation[2];
            view.rotation = Quaternion::ToOrigin(view.translation);
            views.push_back(view);
        }
    }
    return views;
}


int NBV_Planetarian::get_iteration()
{
    return iteration_counter;
}
