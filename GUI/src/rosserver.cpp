#include "rosserver.h"
#include "pclviewer.h"
#include "sensor_msgs/PointCloud2.h"
#include "std_msgs/Bool.h"
#include "std_msgs/String.h"
#include <ctime>



RosServer::RosServer():
_pointCloud(new pcl::PointCloud<pcl::PointXYZRGB>())
{
    ros_refresh_timer = new QTimer(this);
}

void RosServer::run()
{
    char** argv = NULL;
    int argc = 0;
    ros::init(argc, argv, "GUI");
    _nodeHandleClient = new ros::NodeHandle("GUI");

    //setup the ability to pause
    state = GUI_State::STARTUP;

    _pause_publish = new ros::Publisher();
    *_pause_publish = _nodeHandleClient->advertise<std_msgs::Bool>("pause", 1);
    _global_registration_publish = new ros::Publisher();
    *_global_registration_publish = _nodeHandleClient->advertise<std_msgs::Bool>("global_registration", 1);
    _data_string_publish = new ros::Publisher();
    *_data_string_publish = _nodeHandleClient->advertise<std_msgs::String>("filename", 1);
    _feedback_subscriber = new ros::Subscriber();
    *_feedback_subscriber = _nodeHandleClient->subscribe("/view_pick_node/feedback_queue", 5, &RosServer::feedbackCb, this);
    _global_registration_subscriber = new ros::Subscriber();
    *_global_registration_subscriber = _nodeHandleClient->subscribe("/registration/Global_Transform_Delay", 5, &RosServer::registrationCb, this);

    //name to connect and make a thread for spinning
    action_client = new actionlib::SimpleActionClient<view_pick::nbv_plannerAction>("view_pick_node", true);

    std::cout << "Waiting for action server to start." << std::endl;
    action_client->waitForServer(); //will wait for infinite time
    std::cout << "Action server started" << std::endl;

    connect(ros_refresh_timer, SIGNAL(timeout()), this, SLOT(spin_timer()));
    ros_refresh_timer->start(200);
}

void RosServer::spin_timer()
{
    ros::spinOnce();
}

std::string RosServer::fix_time(int time)
{
    std::string ret = std::to_string(time);
    if(ret.length() == 1)
    {
        ret = "0" + ret;
    }
    return ret;
}

void RosServer::send_goal()
{
    std_msgs::Bool msg;
    switch (state)
    {
    case GUI_State::STARTUP:
        {
        //create filename for all
        std_msgs::String filename;
        time_t t = time(0);   // get time now
        struct tm * now = localtime( & t );
        filename.data = "Data_" + fix_time((now->tm_mon + 1)) + "_"
                                + fix_time(now->tm_mday) + "_"
                                + fix_time(now->tm_hour) + "_"
                                + fix_time(now->tm_min) + "_"
                                + fix_time(now->tm_sec);
        setFilename(filename.data);
        t1 = boost::posix_time::microsec_clock::local_time();
        _data_string_publish->publish(filename);
        view_pick::nbv_plannerGoal goal;
        goal.order = 1;
        //send the order
        action_client->sendGoal(goal,
                                boost::bind(&RosServer::doneCb, this, _1, _2),
                                boost::bind(&RosServer::activeCb, this)//,
                                //boost::bind(&RosServer::feedbackCb, this, _1)
                                );
        state = GUI_State::RUN_1;
        break;
        }

    case GUI_State::RUN_1:
        //pause
        msg.data = true;
        _pause_publish->publish(msg);
        state = GUI_State::PAUSE_1;
        break;
    case GUI_State::PAUSE_1:
        //unpause
        msg.data = false;
        _pause_publish->publish(msg);
        state = GUI_State::RUN_1;
        break;
    case GUI_State::RUN_2:
        //pause
        msg.data = true;
        _pause_publish->publish(msg);
        state = GUI_State::PAUSE_2;
        break;
    case GUI_State::PAUSE_2:
        //unpause
        msg.data = false;
        _pause_publish->publish(msg);
        state = GUI_State::RUN_2;
        break;
    case GUI_State::MIDWAY:
        {
        //order global registration
        msg.data = true;
        _global_registration_publish->publish(msg);
        //send the order
        view_pick::nbv_plannerGoal goal;
        t1 = boost::posix_time::microsec_clock::local_time();
        goal.order = 1;
        action_client->sendGoal(goal,
                                boost::bind(&RosServer::doneCb, this, _1, _2),
                                boost::bind(&RosServer::activeCb, this)//,
                                //boost::bind(&RosServer::feedbackCb, this, _1)
                                );
        state = GUI_State::RUN_2;
        break;
        }

    case GUI_State::FINISHED:
        {
        //send reset order
        view_pick::nbv_plannerGoal goal;
        //-1 is requesting a reset
        std::cout << "goal set to -1" << std::endl;
        goal.order = -1;
        action_client->sendGoal(goal,
                                boost::bind(&RosServer::doneCb, this, _1, _2),
                                boost::bind(&RosServer::activeCb, this)//,
                                //boost::bind(&RosServer::feedbackCb, this, _1)
                                );
        state = GUI_State::STARTUP;
        break;
        }

        break;
    default:
        break;
    }
}

void RosServer::setFilename(std::string filename)
{
    if(data_file.is_open())
    {
        data_file.close();
    }

    boost::filesystem::path dir("../../../Data/" + filename + "/GUI");
    if(!(boost::filesystem::exists(dir)))
    {
        if(!boost::filesystem::create_directories(dir))
        {
            ROS_ERROR("Directory could not be created");
        }
    }

    data_file.open("../../../Data/" + filename + "/GUI/times.txt");
    data_file << "Scan 1, Scan 2" << std::endl;
}

void RosServer::send_reset()
{
    state = GUI_State::STARTUP;
    action_client->cancelAllGoals();
}

void RosServer::doneCb(const actionlib::SimpleClientGoalState& state,
            const view_pick::nbv_plannerResultConstPtr& result)
{
    //checks for succesfull return
    if(result->end_state == 1)
    {
        int state = -1;
        if(this->state == GUI_State::RUN_1)
        {
            this->state = GUI_State::MIDWAY;
            state = 0;

            t2 = boost::posix_time::microsec_clock::local_time();
            TimeDuration dt = t2- t1;
            data_file << (long)dt.total_milliseconds()/1000.0 << ", ";
        }
        else if(this->state == GUI_State::RUN_2)
        {
            std::cout << "state set to finished" << std::endl;
            this->state = GUI_State::FINISHED;
            state = 1;
            t2 = boost::posix_time::microsec_clock::local_time();
            TimeDuration dt = t2- t1;
            data_file << (long)dt.total_milliseconds()/1000.0 << std::endl;
        }

        emit done(state);
    }
}

void RosServer::activeCb()
{

}

void RosServer::feedbackCb(const view_pick::feedbackConstPtr result)
{
    //std::cout << "getting feedback, iteration: " << result->progress << std::endl;

    sensor_msgs::PointCloud2 data = result->pointcloud;

    emit updateIteration(result->progress);

    pcl::fromROSMsg(data, *_pointCloud);
    emit updatePointCloud();

    emit updatePose(result->transform);
}

void RosServer::registrationCb(const geometry_msgs::TransformConstPtr result)
{
    emit done(2);
}


/*void RosServer::feedbackCb(const view_pick::nbv_plannerFeedbackConstPtr& result)
{
    //std::cout << "getting feedback, iteration: " << result->progress << std::endl;

    sensor_msgs::PointCloud2 data = result->pointcloud;

    emit updateIteration(result->progress);

    //if(data.data.empty() == false)
    //{
        pcl::fromROSMsg(data, *_pointCloud);
        emit updatePointCloud();
    //}
    cout << "z value: " << result->transform.translation.z << std::endl;
    //if(result->transform.translation.z > 0.01)//view in table, which is not possible so same as no view
    //{
        emit updatePose(result->transform);
    //}
}*/
