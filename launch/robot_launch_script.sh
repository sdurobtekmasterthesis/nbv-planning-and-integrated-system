#!/bin/bash
#Make sure the catkin_ws is sourced
source ../../devel/setup.sh
#Wait a bit (let rest of the nodes start before we start RobWorkStudio)
#xterm -e roscore &
#sleep 2s
#xterm -e roslaunch ur ur_controller.launch &
sleep 3s
# Load the pathplanner nodes
xterm -e roslaunch pathplanner_non_caros workcell_pathplanner.launch &
sleep 2s
# Load the camera node
xterm -e roslaunch openni2_launch openni2.launch depth_registered:=true &
#This comes before the other because it blocks, might result in timing issues but so far none is seen
sleep 2s
xterm -e roslaunch system.launch --wait #All other nodes
#
#Start robworkstudioand open the Scene (also locates RobWorkStudio.ini, and starts the plug in defined there)
#xterm -e ${RWS_ROOT}/bin/debug/RobWorkStudio "../Workcells/CameraAndKinect/CameraKinectTest.wc.xml"
