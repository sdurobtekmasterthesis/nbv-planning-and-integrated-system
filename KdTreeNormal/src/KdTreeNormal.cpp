#include "KdTreeNormal.h"
#include "cmath"



#define PUNISH_WEIGHT 10

KdTreeNormal::KdTreeNormal()
{

}

const std::string KdTreeNormal::getName()
{
    std::string name = "KdNormal";
    return name;
}

void KdTreeNormal::setSortedResults (bool sorted)
{
    sorted_results_ = sorted;
}

bool KdTreeNormal::getSortedResults ()
{
    return sorted_results_;
}

void KdTreeNormal::setInputCloud (const PointCloudConstPtr& cloud,
                    const IndicesConstPtr &indices)
{
    input_ = cloud;
    tree.setInputCloud(getInputCloud() );
    indices_ = indices;
}

int KdTreeNormal::nearestKSearch (const pcl::PointNormal &point, int k,
                                  std::vector<int> &k_indices,
                                  std::vector<float> &k_sqr_distances) const
{
    //std::cout << "nearest was called" << std::endl;

    k_indices.clear();
    k_sqr_distances.clear();

    std::vector<int> tempIdx;
    std::vector<float> tempDistance;
    //increase k search because some are filtered away
    int size = tree.nearestKSearch(point, k*2, tempIdx, tempDistance);

    for(unsigned int i = 0; i<tempDistance.size(); i++)
    {
        double angle = angleBetweenPoints(point, input_->at( tempIdx[i] ) );
        //increase the radial distance if the angle is not fitting
        //pointNKNSquaredDistance[i] *= ((angle/3.1415)+1)*PUNISH_WEIGHT;
        if(angle < 2 && k_indices.size() < k)//about 120 degrees
        {
            k_indices.push_back(tempIdx[i]);
            k_sqr_distances.push_back(tempDistance[i]);
        }
    }

    return size;
}

int KdTreeNormal::nearestKSearch (const PointCloud &cloud, int index, int k,
                        std::vector<int> &k_indices,
                        std::vector<float> &k_sqr_distances) const
{
    return nearestKSearch(cloud[index], k, k_indices, k_sqr_distances);
}

int KdTreeNormal::nearestKSearch (int index, int k,
                        std::vector<int> &k_indices,
                        std::vector<float> &k_sqr_distances) const
{
    return nearestKSearch(input_->at(index), k, k_indices, k_sqr_distances);
}


void KdTreeNormal::nearestKSearch (const PointCloud& cloud, const std::vector<int>& indices,
                        int k, std::vector< std::vector<int> >& k_indices,
                        std::vector< std::vector<float> >& k_sqr_distances) const
{
    std::cout << "not implemented yet(k nearest)" << std::endl;
}

int KdTreeNormal::radiusSearch (const pcl::PointNormal &point, double radius,
                                std::vector<int> &k_indices, std::vector<float> &k_sqr_distances, unsigned int max_nn) const
{
    //std::cout << "radius was called" << std::endl;

    k_indices.clear();
    k_sqr_distances.clear();

    std::vector<int> tempIdx;
    std::vector<float> tempDistance;
    int size = tree.radiusSearch(point, radius, tempIdx, tempDistance, max_nn);

    for(unsigned int i = 0; i<tempDistance.size(); i++)
    {
        double angle = angleBetweenPoints(point, input_->at( tempIdx[i] ) );
        //increase the radial distance if the angle is not fitting
        //pointRadiusSquaredDistance[i] *= ((angle/3.1415)+1)*PUNISH_WEIGHT;
        if(angle < 2)//about 120 degrees
        {
            k_indices.push_back(tempIdx[i]);
            k_sqr_distances.push_back(tempDistance[i]);
        }
    }

    return size;
}

int KdTreeNormal::radiusSearch (const PointCloud &cloud, int index, double radius,
                      std::vector<int> &k_indices, std::vector<float> &k_sqr_distances,
                      unsigned int max_nn) const
{
    return radiusSearch(cloud[index], radius, k_indices, k_sqr_distances, max_nn);
}

int KdTreeNormal::radiusSearch (int index, double radius, std::vector<int> &k_indices,
                      std::vector<float> &k_sqr_distances, unsigned int max_nn) const
{
    return radiusSearch(input_->at(index), radius, k_indices, k_sqr_distances, max_nn);
}
void KdTreeNormal::radiusSearch (const PointCloud& cloud,
                      const std::vector<int>& indices,
                      double radius,
                      std::vector< std::vector<int> >& k_indices,
                      std::vector< std::vector<float> > &k_sqr_distances,
                      unsigned int max_nn) const
{
    std::cout << "not implemented yet(radius)" << std::endl;
}



double KdTreeNormal::angleBetweenPoints(pcl::PointNormal p1, pcl::PointNormal p2) const
{
    Eigen::Vector3d v1(p1.normal_x, p1.normal_y, p1.normal_z);
    Eigen::Vector3d v2(p2.normal_x, p2.normal_y, p2.normal_z);

    return acos( v1.dot(v2)/(v1.norm()*v2.norm()) );
}
