#ifndef KD_TREE_NORMAL_HPP
#define KD_TREE_NORMAL_HPP

#include <string>
#include "vector"

//for the interface
#include <pcl/point_types.h>
#include <pcl/point_cloud.h>
#include <pcl/search/search.h>
//for the KdTree
#include <pcl/search/kdtree.h>
#include <pcl/features/normal_3d_omp.h>
#include <pcl/features/normal_3d.h>



class KdTreeNormal: public pcl::search::Search<pcl::PointNormal>
{
public:

    using pcl::search::Search<pcl::PointNormal>::indices_;
    using pcl::search::Search<pcl::PointNormal>::input_;
    using pcl::search::Search<pcl::PointNormal>::getIndices;
    using pcl::search::Search<pcl::PointNormal>::getInputCloud;
    using pcl::search::Search<pcl::PointNormal>::nearestKSearch;
    using pcl::search::Search<pcl::PointNormal>::radiusSearch;
    using pcl::search::Search<pcl::PointNormal>::sorted_results_;

    KdTreeNormal();
    const std::string getName();
    void setSortedResults (bool sorted);
    bool getSortedResults ();
    void setInputCloud (const PointCloudConstPtr& cloud,
                        const IndicesConstPtr &indices = IndicesConstPtr ());

    int nearestKSearch (const pcl::PointNormal &point, int k,
                        std::vector<int> &k_indices, std::vector<float> &k_sqr_distances) const;
    int nearestKSearch (const PointCloud &cloud, int index, int k,
                            std::vector<int> &k_indices,
                            std::vector<float> &k_sqr_distances) const;
    int nearestKSearch (int index, int k,
                            std::vector<int> &k_indices,
                            std::vector<float> &k_sqr_distances) const;
    void nearestKSearch (const PointCloud& cloud, const std::vector<int>& indices,
                            int k, std::vector< std::vector<int> >& k_indices,
                            std::vector< std::vector<float> >& k_sqr_distances) const;

    int radiusSearch (const pcl::PointNormal &point, double radius,
                      std::vector<int> &k_indices, std::vector<float> &k_sqr_distances, unsigned int max_nn = 0) const;
    int radiusSearch (const PointCloud &cloud, int index, double radius,
                          std::vector<int> &k_indices, std::vector<float> &k_sqr_distances,
                          unsigned int max_nn = 0) const;
    int radiusSearch (int index, double radius, std::vector<int> &k_indices,
                          std::vector<float> &k_sqr_distances, unsigned int max_nn = 0) const;
    void radiusSearch (const PointCloud& cloud,
                          const std::vector<int>& indices,
                          double radius,
                          std::vector< std::vector<int> >& k_indices,
                          std::vector< std::vector<float> > &k_sqr_distances,
                          unsigned int max_nn = 0) const;

    double angleBetweenPoints(pcl::PointNormal p1, pcl::PointNormal p2) const;

protected:
    pcl::search::KdTree<pcl::PointNormal> tree;

};

#endif // KD_TREE_NORMAL_HPP
