/*
Code is nearly a copy from pcl  pcl::VoxelGridOcclusionEstimation< PointT >::rayBoxIntersection	in filters, just with a more usefull interface for this case.
https://tavianator.com/fast-branchless-raybounding-box-intersections-part-2-nans/
*/
#ifndef RAY_H
#define RAY_H

#include "vector"

class Ray
{
public:
        static bool rayBoxIntersection (const Eigen::Vector3d& origin, const Eigen::Vector3d& direction, std::vector<double> b_min_, std::vector<double> b_max_, Eigen::Vector2d& result)
	{
		double tmin, tmax, tymin, tymax, tzmin, tzmax;
		
		double div;
		div = 1/direction[0];//set to x divisor
		if (div >= 0)
		{
		tmin = (b_min_[0] - origin[0]) * div;
		tmax = (b_max_[0] - origin[0]) * div;
		}
		else
		{
		tmin = (b_max_[0] - origin[0]) * div;
		tmax = (b_min_[0] - origin[0]) * div;
		}

		div = 1/direction[1];//set to y divisor
		if (div >= 0)
		{
		tymin = (b_min_[1] - origin[1]) * div;
		tymax = (b_max_[1] - origin[1]) * div; 
		}
		else
		{
		tymin = (b_max_[1] - origin[1]) * div;
		tymax = (b_min_[1] - origin[1]) * div;
		}

		if ((tmin > tymax) || (tymin > tmax))
		{
		return false;
		}

		if (tymin > tmin)
		tmin = tymin;
		if (tymax < tmax)
		tmax = tymax;

		div = 1/direction[2];//set to z divisor
		if (div >= 0)
		{
		tzmin = (b_min_[2] - origin[2]) * div;
		tzmax = (b_max_[2] - origin[2]) * div;
		}
		else
		{
		tzmin = (b_max_[2] - origin[2]) * div;
		tzmax = (b_min_[2] - origin[2]) * div;
		}

		if ((tmin > tzmax) || (tzmin > tmax))
		{
		return false;       
		}

		if (tzmin > tmin)
		tmin = tzmin;
		if (tzmax < tmax)
		tmax = tzmax;

		result[0] = tmin;
		result[1] = tmax;

		return true;
	}
};

#endif // RAY_H
