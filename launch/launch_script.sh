#!/bin/bash
#Make sure the catkin_ws is sourced
source ../../devel/setup.sh
#Wait a bit (let rest of the nodes start before we start RobWorkStudio)
xterm -e roscore &
sleep 2s
#This comes before the other because it blocks, might result in timing issues but so far none is seen
xterm -e roslaunch system.launch --wait & #All other nodes
#Start robworkstudioand open the Scene (also locates RobWorkStudio.ini, and starts the plug in defined there)
xterm -e ${RWS_ROOT}/bin/debug/RobWorkStudio "../Workcells/CameraAndKinect/CameraKinectTest.wc.xml"
